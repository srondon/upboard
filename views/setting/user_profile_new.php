<section class="content-header">
          <h1>
            New User Profile
            <!--small>Control panel</small-->
          </h1>
          <!--ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Calendar</li>
          </ol-->
</section>
<section class="content">

  <div class="row">
    <div class="col-md-8">
    <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title"></h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form role="form" id="form-profile">
                    <div class="box-body">
                      <div class='row'>
                            <div class="col-xs-4">
                                <div class="form-group">
                                  <label>Description</label>
                                  <input type="text" class="form-control" id="description" ng-model="profile.description" placeholder="Enter description">
                                </div>
                                <div class="form-group">
                                  <div class="checkbox">
                                    <label>
                                      <input ng-model="profile.create_new" type="checkbox">
                                      Add
                                    </label>
                                  </div>

                                  <div class="checkbox">
                                    <label>
                                      <input ng-model="profile.modify" type="checkbox">
                                      Update
                                    </label>
                                  </div>

                                  <div class="checkbox">
                                    <label>
                                      <input ng-model="profile.remove" type="checkbox">
                                      Remove
                                    </label>
                                  </div>
                                </div>
                                <!--div class="form-group">
                                  <label>Menu Options</label>
                                  <select multiple size="10" class="form-control" ng-model="profile.menu_options" ng-options="option.menu group by option.parent_menu for option in menuoptions">
                                  </select>
                                </div> 
                                <div class="form-group " style="text-align:center"> 
                                  <button ng-click="addPermissions()" class="btn btn-success">Add Permissions</button>
                                </div-->                               
                            </div>
                            <div class="col-xs-8">
                              <div class="form-group">
                                
                                  <table class="table table-bordered">
                                    <tr>
                                      <td colspan="2">
                                        <label>Menu Options Access</label>           
                                      </td>
                                      <td style="width: 10%">
                                          <span style="cursor:pointer" ng-click='changeAccessAll()' class="label label-{{allow_all==1?'success':'danger'}}">{{allow_all==1?'&nbsp;&nbsp;&nbsp;All&nbsp;&nbsp;&nbsp;':'None'}}</span>
                                      </td>
                                    </tr>
                                    <tr ng-repeat="profile_access in profile.profileDetails">
                                      <td style="width: 45%">
                                          {{profile_access.parent_menu}}
                                      </td>
                                      <td style="width: 45%">
                                          {{profile_access.menu}}
                                      </td>
                                      <td style="width: 10%">
                                          <span style="cursor:pointer" ng-click='changeAccess($index)' class="label label-{{profile_access.allow==1?'success':'danger'}}">{{profile_access.allow==1?'Yes':'No'}}</span>
                                      </td>
                                    </tr>                              
                                  </table>
                              </div>
                            </div>
                          </div>
                    
                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button ng-click="submitForm(0)" class="btn btn-primary">Submit and Continue</button>
                    <button ng-click="submitForm(1)" class="btn btn-primary">Submit and Exit</button>
                    <button ng-click="exit()" style="float:right" class="btn btn-danger">Exit</button>
                  </div>
                </form>
              </div>        
    
  </div>
  </div>
</section>