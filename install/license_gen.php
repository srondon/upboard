<?php 
    include "../includes/clases/CONF.php";
    session_start();

    if(isset($_POST['group_name'])){
      $MAIN_DIR = "../";
      require_once("../includes/clases/controladores/AppController.php");
      $controlador = new AppController($_FILES,$_POST);//,$session

      $request['accion'] = "Action/Setting/Generate_License";
      $request['group_name'] = $_POST['group_name'];
     // $request['output'] = 'json';
      $controlador->setRequest('',$request);
      $resultDealers = $controlador->ejecutar();
    }

    $now = time(); // Checking the time now when home page starts.

    if(!isset($_SESSION['name'])){
      header("Location:../login.php?license");
    }
    if ($now >= $_SESSION['session_timeout']) {
      session_destroy();
      header("Location:../login.php?license");
    }
    
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo APP_NAME;?> | New License</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="../dist/css/skins/_all-skins.min.css">
    <link rel="stylesheet" href="../dist/css/plugins.css">
    <link rel="stylesheet" href="../dist/css/style.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script data-require="jquery@*" data-semver="2.1.1" src="../includes/js/jquery.min.js"></script>
    <script data-require="angular.js@1.3.0" data-semver="1.3.0" ng:autobind src="../includes/js/angular.js"></script>
  </head>
  <!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
  <body class="hold-transition skin-blue layout-top-nav">
    <div class="wrapper" ng-controller="wizard">

      <header class="main-header">
        <nav class="navbar navbar-static-top">
          <div class="container">
            <div class="navbar-header">
              <a href="" class="navbar-brand"><b><?php echo APP_NAME;?></a>
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                <i class="fa fa-bars"></i>
              </button>
            </div>

            
            <!-- Navbar Right Menu -->
              <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                  
                </ul>
              </div><!-- /.navbar-custom-menu -->
          </div><!-- /.container-fluid -->
        </nav>
      </header>
      <!-- Full Width Column -->
      <div class="content-wrapper">
        <div class="container">
          <!-- Content Header (Page header) -->
          <?php if($_SESSION['user_id'] == 1){?>
          <section class="content-header">
            <h1>
              New License
              <small></small>
            </h1>
          </section>
          
          <!-- Main content -->
          <section class="content">
            <div class="box box-default">
              <div class="box-body">
                  <?php if(!isset($_POST['group_name'])){?>
                  <form class="form-horizontal" method="post" action="license_gen.php">
                    <div class="box-body">
                      <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Group Name</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" id="inputEmail3" name="group_name" placeholder="Enter Group Name">
                        </div>
                      </div>
                    </div><!-- /.box-body -->
                    <div class="box-footer">
                      <input type="submit" class="btn btn-info pull-right" name="generate" value="Generate"/>
                    </div><!-- /.box-footer -->
                  </form>
                  <?php }else{?>
                    <h1>
                      Your new license is: 
                      <small><?php echo $resultDealers['license']?></small><br>
                      <a href="../install/" class="btn btn-primary btn-block btn-flat">Go Intall</a>
                    </h1>
                  <?php }?>
              </div><!-- /.box-body -->
            </div><!-- /.box -->
          </section><!-- /.content -->
        </div><!-- /.container -->
      </div><!-- /.content-wrapper -->
      <?php }else{?>
            <section class="content-header">
            <h1>
              Access Denied
              <small></small>
            </h1>
          </section>
      <?php }?>
      <footer class="main-footer">
        <div class="container">
          <div class="pull-right hidden-xs">
            <strong>Copyright @ 2011 Dealer Geek All Rights Reserved.
        <p style="color:#F0F0F0">Copyright 2014-2015 Almsaeed Studio. All rights reserved.</p>
        </div><!-- /.container -->
      </footer>
    </div><!-- ./wrapper -->

    <!-- jQuery 2.1.4 -->
    <script src="../plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="../bootstrap/js/bootstrap.min.js"></script>
    <!-- SlimScroll -->
    <script src="../plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="../plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="../dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="../dist/js/demo.js"></script>
  </body>
</html>

