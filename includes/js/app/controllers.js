angular.module('upboard-app')
.controller('generalSettings', function ($scope, $http,$routeParams,$location,FileUploader) {
    
    $scope.buttonDisabled = true;

    $http.get("includes/clases/controladores/AppController.php",{params:{
            accion:'Action/Setting/Get_General_Setting',
            output:'json'
    }})
    .then(function(response) {
            $scope.general = response.data.matches[0];
            $scope.buttonDisabled = false;
    });


    $scope.submitForm = function() {
        formData = $scope.general;

            
        $http.post("includes/clases/controladores/AppController.php?accion=Action/Setting/Save_General_Setting&output=json", JSON.stringify(formData))
        .success(function(response){
                /*if(response.reload){
                    alert(response.message);
                    location.href = "/upboard/login.php";
                }*/
                alert(response.message);
        });
    };
    
});

angular.module('upboard-app')
.controller('dashboard', function ($scope,$http,$routeParams,$location,$interval,$timeout,$location,FileUploader) {
    $scope.selling = [];
    $scope.offs = [];
    $scope.matchesBoard = [];
    $scope.next = '';
    /*appRequest('get','Dashboard/Get_Data',{params:{
        accion:'Action/Dashboard/Get_Data',
        output:'json'
    }},function(response) {
            angular.copy(response.data.matchesSelling, $scope.selling);
            angular.copy(response.data.matchesOffs, $scope.offs);
    }); */
    $scope.load = function() {

        $http.get("includes/clases/controladores/AppController.php",{params:{
            accion:'Action/Dashboard/Get_Data',
            output:'json'
        }})
        .then(function(response) {
                angular.copy(response.data.matchesSelling, $scope.selling);
                angular.copy(response.data.matchesOffs, $scope.offs);
                angular.copy(response.data.matchesBoard, $scope.matchesBoard);
                $scope.next = response.data.next;
        });
    }

    $scope.load();
    var callLoad = function(){
        $scope.load();
    }
    $interval(callLoad, 60000);
});

angular.module('upboard-app')
.controller('salesboard', function ($scope,$http,$routeParams,$location,$interval,$timeout,$filter,FileUploader) {
    $scope.salesmenMaster = [];

    $scope.salesmen = [];
    $scope.statusArray = [];
    $scope.messages = [];
    $scope.salesmenDisabled = false;
    $scope.availableBoardSalesmen = [];
    $scope.actionBoardSalesmen = [];

    $scope.next_up_name = "";
    $scope.disableAddSalesmen = true;

    $scope.create_new = false;
    $scope.modify = false;
    $scope.remove = false;
    $scope.greeter = false;
    $scope.greeterRO = false;

    $scope.load = function() {
        document.getElementById("prossesiing-available-list").style.visibility = 'visible';
        document.getElementById("prossesiing-action-list").style.visibility = 'visible';
        $http.get("includes/clases/controladores/AppController.php",{params:{
            accion:'Action/Salesboard/Load_Upboard_State',
            output:'json'
        }})
        .then(function(response) {
                angular.copy(response.data.matchesSalesmen, $scope.salesmen);
                angular.copy(response.data.matchesSalesmen, $scope.salesmenMaster);
                angular.copy(response.data.matchesStatus, $scope.statusArray);
                angular.copy(response.data.matchesAvailableBoard, $scope.availableBoardSalesmen);
                angular.copy(response.data.matchesActionBoard, $scope.actionBoardSalesmen);
                $scope.upboard.salesman = response.data.matchesSalesmen[0];
                $scope.upboard.salesman = $scope.salesmen[0];
                if($scope.availableBoardSalesmen.length != 0)
                    $scope.next_up_name = $scope.availableBoardSalesmen[0].name;
                else
                    $scope.next_up_name = "";

                if(response.data.create_new == 1)
                    response.data.create_new = true;
                else
                    response.data.create_new = false;
                if(response.data.modify == 1)
                        response.data.modify = true;
                else
                    response.data.modify = false;
                if(response.data.remove == 1)
                        response.data.remove = true;
                else
                    response.data.remove = false;
                if(response.data.greeter == 1)
                        response.data.greeter = true;
                else
                    response.data.greeter = false;

                if(response.data.greeterRO == 1)
                        response.data.greeterRO = true;
                else
                    response.data.greeterRO = false;

                $scope.create_new = response.data.create_new;
                $scope.modify = response.data.modify;
                $scope.remove = response.data.remove;
                $scope.greeter = response.data.greeter;
                $scope.greeterRO = response.data.greeterRO;

                //console.log(response.data.create_new , response.data.modify,response.data.create_new && response.data.modify)
                $scope.disableAddSalesmen = (!response.data.create_new || !response.data.modify);
                $scope.disableChangeStatus = (!response.data.modify);
                $scope.isGreeter =  !response.data.greeter;
                $scope.isGreeterRO =  !response.data.greeterRO;

                //console.log($scope.isGreeterRO , response.data.greeterRO ,!response.data.greeterRO)

        });
        document.getElementById("prossesiing-available-list").style.visibility = 'hidden';
        document.getElementById("prossesiing-action-list").style.visibility = 'hidden';
    }
    $scope.load();

    var tick = function() {
        var time = Date.now().toString();
        $scope.clock = time;
        $scope.datetoshow = $filter('date')(Date.now(),'HH:mm:ss');
        //console.log(time,$scope.clock,$filter('date')(Date.now(),'HH:mm:ss'))
    }
    tick();
    $interval(tick, 1000);
    
    var callLoad = function(){
        $scope.load();
    }
    $interval(callLoad, 120000);

    $scope.onSelectSalesman = function(index){
        document.getElementById("prossesiing-available-list").style.visibility = 'visible';
        //$scope.availableBoardSalesmen.push($scope.upboard.salesman);
        //var index = $scope.salesmen.indexOf($scope.upboard.salesman);
        //$scope.salesmen.splice(index, 1);             
        //if(confirm("Are you sure?")){
            $http.post("includes/clases/controladores/AppController.php?accion=Action/Salesboard/Change_Saleman_Status&output=json&new_status_id=1", JSON.stringify($scope.upboard.salesman))
                .success(function(response){
                    //alert(response.message);   
                    $scope.load();     
            });
        //}
    }

    $scope.next = function(){
        $scope.changeStatus(0,5,'next');
    }

    $scope.reset = function(){
        if(confirm("Are you sure?")){
                

                $http.post("includes/clases/controladores/AppController.php?accion=Action/Salesboard/Reset&output=json",{})
                    .success(function(response){
                        //alert(response.message);
                    $scope.load();
                });
        }
        else{

        document.getElementById("prossesiing-available-list").style.visibility = 'hidden';
        document.getElementById("prossesiing-action-list").style.visibility = 'hidden';
        }    
    }

    $scope.changeStatus = function(index,status_id,action){
        document.getElementById("prossesiing-available-list").style.visibility = 'visible';
        document.getElementById("prossesiing-action-list").style.visibility = 'visible';
        var action2 = "";
        if(action == 'hot'){
            action = "back";
            action2 = "hot";
            if(confirm("Are you sure?")){
                
                var Saleman = (action == "back")?$scope.actionBoardSalesmen[index]:$scope.availableBoardSalesmen[index];
                Saleman.status_id = "9";
                Saleman.order_showed = 1;
                Saleman.keep_order = 1;                
                Saleman.keep_second_order = 1;                
                Saleman.board_order = 1;                
                Saleman.from_beback = 1;                

                $http.post("includes/clases/controladores/AppController.php?accion=Action/Salesboard/Change_Saleman_Status&output=json&new_status_id="+status_id+"&saleman_id="+Saleman.saleman_id+"&new_datetoshow="+$scope.datetoshow+"&action="+action+"&action2="+action2, JSON.stringify(Saleman))
                    .success(function(response){
                        //alert(response.message);
                        $scope.load();
                });
            }            
            else{   
                if(action != "back"){
                    $scope.load();
                }
                document.getElementById("prossesiing-available-list").style.visibility = 'hidden';
                document.getElementById("prossesiing-action-list").style.visibility = 'hidden';
            }
        }
        else if(status_id != 6){
            if(confirm("Are you sure?")){
                
                var Saleman = (action == "back")?$scope.actionBoardSalesmen[index]:$scope.availableBoardSalesmen[index];

                $http.post("includes/clases/controladores/AppController.php?accion=Action/Salesboard/Change_Saleman_Status&output=json&new_status_id="+status_id+"&saleman_id="+Saleman.saleman_id+"&new_datetoshow="+$scope.datetoshow+"&action="+action, JSON.stringify(Saleman))
                    .success(function(response){
                        //alert(response.message);
                        $scope.load();
                });
            }            
            else{   
                if(action != "back"){
                    $scope.load();
                }
                document.getElementById("prossesiing-available-list").style.visibility = 'hidden';
                document.getElementById("prossesiing-action-list").style.visibility = 'hidden';
            }      
        }
        else{
            var new_board_order = prompt("Please enter the new order", "")
            
            if(new_board_order == null || new_board_order == '' || new_board_order == 0){
                document.getElementById("prossesiing-available-list").style.visibility = 'hidden';
                document.getElementById("prossesiing-action-list").style.visibility = 'hidden';
                $scope.load();
                return;
            }
            
            var Saleman = (action == "back")?$scope.actionBoardSalesmen[index]:$scope.availableBoardSalesmen[index];

                $http.post("includes/clases/controladores/AppController.php?accion=Action/Salesboard/Change_Saleman_Status&output=json&new_status_id="+status_id+"&saleman_id="+Saleman.saleman_id+"&new_datetoshow="+$scope.datetoshow+"&action="+action+"&new_board_order="+new_board_order, JSON.stringify(Saleman))
                    .success(function(response){
                        //alert(response.message);
                    $scope.load();
                });
                        
                            
            document.getElementById("prossesiing-available-list").style.visibility = 'hidden';
            document.getElementById("prossesiing-action-list").style.visibility = 'hidden';
                
        }
        
    }

    $scope.exit = function(){
        $location.path('/statusSalesBoard');
    }

    $scope.sendMessage = function(type,index){
        
        if(!$scope.create_new && !$scope.modify && !$scope.remove && !$scope.greeter){
            alert("Unable to send notifications");
            return;
        }
        if(confirm(type+": Are you sure?")){
            
            var Saleman = $scope.availableBoardSalesmen[index];
            $http.post("includes/clases/controladores/AppController.php?accion=Action/Salesboard/Notify&output=json&saleman_id="+Saleman.saleman_id+"&notification_type="+type, JSON.stringify(Saleman))
                .success(function(response){
                    //alert(response.message);
                $scope.load();
            });
        }
    }
    
});

angular.module('upboard-app')
.controller('statusSalesBoard', function ($scope,$http,$routeParams,$location,FileUploader) {
    $scope.inicio = 1;
    $scope.fin = 30;
    $scope.maxSize = 10;
    $scope.statusList = [];
    $scope.predicate = 'description';
    $scope.reverse = false;
    $scope.dir = "asc";     
    $scope.currentPage = 1;
    $scope.disableAdd = false;
    $scope.disableUpd = false;
    $scope.disableDel = false;

    $http.get("includes/clases/controladores/AppController.php",{params:{
        accion:'Action/Setting/Get_Status_List',
        output:'json',
        pagina:$scope.currentPage,
        orden:$scope.predicate,
        dir:$scope.dir
    }})
    .then(function(response) {
            $scope.totalItems = response.data.totalCount;
            $scope.inicio  = response.data.inicio;
            $scope.fin  = response.data.fin;
            angular.copy(response.data.matches, $scope.statusList);
            //console.log("aqui",response.data.busquedaS);
            if(response.data.create_new == 1)
                    response.data.create_new = true;
                else
                    response.data.create_new = false;
                if(response.data.modify == 1)
                        response.data.modify = true;
                else
                    response.data.modify = false;
                if(response.data.remove == 1)
                        response.data.remove = true;
                else
                    response.data.remove = false;
                //console.log(response.data.create_new , response.data.modify,response.data.create_new && response.data.modify)
                $scope.disableAdd = (!response.data.create_new);
                $scope.disableUpd = (!response.data.modify);
                $scope.disableDel = (!response.data.remove);

    });
});

angular.module('upboard-app')
.controller('statusNew', function ($scope, $http,$routeParams,$location,FileUploader) {
    
    $scope.submitForm = function(typeSubmit) {
        formData = $scope.status;


        $http.post("includes/clases/controladores/AppController.php?accion=Action/Setting/Save_Status&output=json", JSON.stringify(formData))
        .success(function(response){
            if (typeSubmit==1) {
                $location.path('/statusSalesBoard/');
            };            
        });
    };

    $scope.exit = function(){
        $location.path('/StatusSalesBoard/');
    }
    
});

angular.module('upboard-app')
.controller('statusView', function ($scope,$http,$routeParams,$location,FileUploader) {
    
    $scope.status_id = $routeParams.status_id;
    $scope.disableAdd = false;
    $scope.disableUpd = false;
    $scope.disableDel = false;

    $http.get("includes/clases/controladores/AppController.php",{params:{
                    accion:'Action/Setting/Get_Status',
                    output:'json',
                    status_id:$scope.status_id
    }})
    .then(function(response) {
        if(response.data.matches[0].allow == 1)
            response.data.matches[0].allow = true;
        else
            response.data.matches[0].allow = false;
        
        if(response.data.matches[0].preserve_position == 1)
            response.data.matches[0].preserve_position = true;
        else
            response.data.matches[0].preserve_position = false;

        $scope.status = response.data.matches[0];
        if(response.data.create_new == 1)
                    response.data.create_new = true;
                else
                    response.data.create_new = false;
                if(response.data.modify == 1)
                        response.data.modify = true;
                else
                    response.data.modify = false;
                if(response.data.remove == 1)
                        response.data.remove = true;
                else
                    response.data.remove = false;
                //console.log(response.data.create_new , response.data.modify,response.data.create_new && response.data.modify)
                $scope.disableAdd = (!response.data.create_new);
                $scope.disableUpd = (!response.data.modify);
                $scope.disableDel = (!response.data.remove);
    });


    $scope.submitForm = function(typeSubmit) {
        if($scope.status.allow == true)
            $scope.status.allow = '1';
        else
            $scope.status.allow = '0';

        if($scope.status.preserve_position == true)
            $scope.status.preserve_position = '1';
        else
            $scope.status.preserve_position = '0';
        formData = $scope.status;
        //$http.defaults.headers.post = 'application/upload';

        $http.post("includes/clases/controladores/AppController.php?accion=Action/Setting/Save_Status&output=json&status_id="+$scope.status.status_id, JSON.stringify(formData))
        .success(function(response){
            alert(response.message);
            if(typeSubmit == 1){
                $scope.exit();
            }
        });
    };

    $scope.exit = function(){
        $location.path('/StatusSalesBoard/');
    }
});


angular.module('upboard-app')
.controller('Salesmen', function ($scope,$http,$routeParams,$location,FileUploader) {
    $scope.inicio = 1;
    $scope.fin = 30;
    $scope.maxSize = 10;
    $scope.salesmen = [];
    $scope.predicate = 'name';
    $scope.reverse = false;
    $scope.dir = "asc";     
    $scope.currentPage = 1;
    $scope.disableAdd = false;
    $scope.disableUpd = false;
    $scope.disableDel = false;

    $scope.load = function(){ 
        $http.get("includes/clases/controladores/AppController.php",{params:{
            accion:'Action/Salesman/Get_Salesman_List',
            output:'json',
            pagina:$scope.currentPage,
            orden:$scope.predicate,
            dir:$scope.dir
        }})
        .then(function(response) {
                $scope.totalItems = response.data.totalCount;
                $scope.inicio  = response.data.inicio;
                $scope.fin  = response.data.fin;
                angular.copy(response.data.matches, $scope.salesmen);
                //console.log("aqui",response.data.busquedaS);
                    if(response.data.create_new == 1)
                        response.data.create_new = true;
                    else
                        response.data.create_new = false;
                    if(response.data.modify == 1)
                            response.data.modify = true;
                    else
                        response.data.modify = false;
                    if(response.data.remove == 1)
                            response.data.remove = true;
                    else
                        response.data.remove = false;
                    //console.log(response.data.create_new , response.data.modify,response.data.create_new && response.data.modify)
                    $scope.disableAdd = (!response.data.create_new);
                    $scope.disableUpd = (!response.data.modify);
                    $scope.disableDel = (!response.data.remove);

        });
    }
    $scope.load();
    
    
    $scope.delete = function(salesmanId){
        if($scope.disableDel){
            alert("You are unable for this action");
            return;
        }
        if(confirm("Are you sure?")){
            $http.get("includes/clases/controladores/AppController.php",{params:{
                            accion:'Action/Salesman/Delete',
                            output:'json',
                            saleman_id:salesmanId
            }})
            .then(function(response) {
                alert(response.data.message);
                $scope.load();
            });    
        }        
    }
});

angular.module('upboard-app')
.controller('SalesmenNew', function ($scope, $http,$routeParams,$location,FileUploader) {
    $scope.carriers = [];
    $scope.emailpat = /^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$/;

    $http.get("includes/clases/controladores/AppController.php",{params:{
                    accion:'Action/Setting/Get_Carrier_List',
                    output:'json'
    }})
    .then(function(response) {
            angular.copy(response.data.matches, $scope.carriers);
    });

    $scope.submitForm = function(typeSubmit) {
        //console.log($scope.saleman.name)
        if( $scope.saleman.name == ''  || typeof $scope.saleman.name == 'undefined' || 
            $scope.saleman.email == '' || typeof $scope.saleman.email == 'undefined' || 
            $scope.saleman.phone == '' || typeof $scope.saleman.phone == 'undefined'){
            alert("You have to enter required fields");
            return;
        }
        formData = $scope.saleman;


        $http.post("includes/clases/controladores/AppController.php?accion=Action/Salesman/Save_Salesman&output=json", JSON.stringify(formData))
        .success(function(response){
            if (typeSubmit==1) {
                $location.path('/Salesperson/');
            };            
        });
    };

    $scope.exit = function(){
        $location.path('/Salesperson/');
    }

});

angular.module('upboard-app')
.controller('SalesmenView', function ($scope,$http,$routeParams,$location,FileUploader) {
    
    $scope.saleman_id = $routeParams.saleman_id;
    $scope.carriers = [];
    $scope.disableAdd = false;
    $scope.disableUpd = false;
    $scope.disableDel = false;
    $scope.emailpat = /^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$/;
    

    $http.get("includes/clases/controladores/AppController.php",{params:{
                    accion:'Action/Setting/Get_Carrier_List',
                    output:'json'
    }})
    .then(function(response) {
            angular.copy(response.data.matches, $scope.carriers);
        
    });

    $http.get("includes/clases/controladores/AppController.php",{params:{
                    accion:'Action/Salesman/Get_Salesman',
                    output:'json',
                    saleman_id:$scope.saleman_id
    }})
    .then(function(response) {
        if(response.data.matches[0].active == 1)
            response.data.matches[0].active = true;
        else
            response.data.matches[0].active = false;

        
        $scope.saleman = response.data.matches[0];
        $scope.saleman.notification_carrier_id = parseInt(response.data.matches[0].notification_carrier_id);
        
        //alert(response.data.matches[0].notification_carrier_id)
        //$scope.saleman.notification_carrier_id = response.data.matches[0].notification_carrier_id;
        //alert($scope.saleman.notification_carrier_id)
            if(response.data.create_new == 1)
                    response.data.create_new = true;
                else
                    response.data.create_new = false;
                if(response.data.modify == 1)
                        response.data.modify = true;
                else
                    response.data.modify = false;
                if(response.data.remove == 1)
                        response.data.remove = true;
                else
                    response.data.remove = false;

                
                //console.log(response.data.create_new , response.data.modify,response.data.create_new && response.data.modify)
                $scope.disableAdd = (!response.data.create_new);
                $scope.disableUpd = (!response.data.modify);
                $scope.disableDel = (!response.data.remove);
    });


    $scope.submitForm = function(typeSubmit) {
        if( $scope.saleman.name == ''  || typeof $scope.saleman.name == 'undefined' || 
            $scope.saleman.email == '' || typeof $scope.saleman.email == 'undefined' || 
            $scope.saleman.phone == '' || typeof $scope.saleman.phone == 'undefined'){
            alert("You have to enter required fields");
            return;
        }
        if($scope.saleman.active == true)
            $scope.saleman.active = '1';
        else
            $scope.saleman.active = '0';

        formData = $scope.saleman;
        
        $http.post("includes/clases/controladores/AppController.php?accion=Action/Salesman/Save_Salesman&output=json&saleman_id="+$scope.saleman_id, JSON.stringify(formData))
        .success(function(response){
            alert(response.message);
            if(typeSubmit == 1){
                $scope.exit();
            }
            document.getElementById('new-saleman-form').reset();
        });
    };

    $scope.exit = function(){
        $location.path('/Salesperson/');
    }
});


angular.module('upboard-app')
.controller('UserList', function ($scope,$http,$routeParams,$location,FileUploader) {
    $scope.inicio = 1;
    $scope.fin = 30;
    $scope.maxSize = 10;
    $scope.currentPage = 1;
    $scope.predicate = 'name';
    $scope.reverse = false;
    $scope.dir = "asc";     
    $scope.gettingData = false;

    $scope.users = [];
    $scope.disableAdd = false;
    $scope.disableUpd = false;
    $scope.disableDel = false;

    getData()


    function getData(){
    $scope.gettingData = true;
        $http.get("includes/clases/controladores/AppController.php",{params:{
            accion:'Action/User/Get_User_List',
            output:'json',
            pagina:$scope.currentPage,
            orden:$scope.predicate,
            dir:$scope.dir
        }})
        .then(function(response) {
                $scope.totalItems = response.data.totalCount3;
                $scope.inicio  = response.data.inicio;
                $scope.fin  = response.data.fin;
                angular.copy(response.data.matches, $scope.users)
                //console.log("aqui",response.data.busquedaS);
                if(response.data.create_new == 1)
                        response.data.create_new = true;
                    else
                        response.data.create_new = false;
                    if(response.data.modify == 1)
                            response.data.modify = true;
                    else
                        response.data.modify = false;
                    if(response.data.remove == 1)
                            response.data.remove = true;
                    else
                        response.data.remove = false;
                    //console.log(response.data.create_new , response.data.modify,response.data.create_new && response.data.modify)
                    $scope.disableAdd = (!response.data.create_new);
                    $scope.disableUpd = (!response.data.modify);
                    $scope.disableDel = (!response.data.remove);
                $scope.gettingData = false;

        });
    }

    $scope.pageChanged = function() {
        getData();
    };

    $scope.newUser = function(){
        $location.path('/User/New');
    }

    $scope.delete = function(userId){
        if($scope.disableDel){
            alert("You are unable for this action");
            return;
        }
        if(confirm("Are you sure?")){
            $http.get("includes/clases/controladores/AppController.php",{params:{
                            accion:'Action/User/Delete',
                            output:'json',
                            user_id:userId
            }})
            .then(function(response) {
                alert(response.data.message);
                 getData();
            });    
        }        
    }
});

angular.module('upboard-app')
.controller('UserView', function ($scope,$http,$routeParams,$location,FileUploader) {
    

    $scope.user = {
        name:'',
        username:'',
        profileDefaults:[],
        profileAccess:[],
        profileDealerAccess:[],        
        profileDetails:[],
        dealerAccessDetails:[],
        password:'',
        repeatpassword:'',
        phone:'',
        profile:[],
        dealerships:[],
        dealershipSelected:[]
    };
    $scope.menuoptions = [];
    $scope.profileDetails = [];
    $scope.dealerAccessDetails = [];
    $scope.dealerships = [];
    $scope.profiles = [];
    $scope.profile = [];
    $scope.profileSelected = [];
    $scope.disableAdd = false;
    $scope.disableUpd = false;
    $scope.disableDel = false;
    /*$http.get("includes/clases/controladores/AppController.php",{params:{
            accion:'Action/Setting/Get_Menu_Options',
            output:'json'
    }})
    .then(function(response) {
            angular.copy(response.data.matches, $scope.menuoptions);
    });*/
    $scope.carriers = [];

    $http.get("includes/clases/controladores/AppController.php",{params:{
                    accion:'Action/Setting/Get_Carrier_List',
                    output:'json'
    }})
    .then(function(response) {
            angular.copy(response.data.matches, $scope.carriers);
    });

    $http.get("includes/clases/controladores/AppController.php",{params:{
            accion:'Action/Setting/Get_Dealer_List',
            output:'json'
    }})
    .then(function(response) {
            angular.copy(response.data.matches, $scope.dealerships);
            //$scope.user.dealerships = $scope.dealerships[0];
    });

    $http.get("includes/clases/controladores/AppController.php",{params:{
            accion:'Action/User/Get_Profile_List',
            output:'json'
    }})
    .then(function(response) {
            angular.copy(response.data.matches, $scope.profiles);
            //angular.copy(response.data.matches, $scope.user.profileDefaults);
            ////angular.copy(response.data.matchesProfilesDetails, $scope.user.profileAccess);
    });

    if($routeParams.user_id){
                $scope.user_id = $routeParams.user_id;
                $http.get("includes/clases/controladores/AppController.php",{params:{
                    accion:'Action/User/Get_User',
                    output:'json',
                    user_id:$scope.user_id
                }})
                .then(function(response) {
                        $scope.user = response.data.matches[0];
                        angular.copy(response.data.matchProfile, $scope.profileSelected);   
                        angular.copy(response.data.matchDealerAccess, $scope.user.matchesDealerAccessDetails);   
                        angular.copy(response.data.matchDealerAccess, $scope.dealerAccessDetails);   
                        angular.copy(response.data.matchesMenuAccess, $scope.user.profileAccess);   
                        angular.copy(response.data.matchesMenuAccess, $scope.profileDetails);   
                        //$scope.user.profile = $scope.profileSelected[0];
                        //$scope.user.profile = response.data.matchProfile;
                        //angular.copy(response.data.matchesDealerAccessDetails, $scope.user.dealerAccessDetails);

                        /*for (var i = 0; i < response.data.matchDealerAccess.length ;i++) {
                            $scope.user.dealershipSelected.push(response.data.matchDealerAccess[i].dealership_id);
                        }*/

                        if(response.data.create_new == 1)
                            response.data.create_new = true;
                        else
                            response.data.create_new = false;
                        if(response.data.modify == 1)
                                response.data.modify = true;
                        else
                            response.data.modify = false;
                        if(response.data.remove == 1)
                                response.data.remove = true;
                        else
                            response.data.remove = false;
                        //console.log(response.data.create_new , response.data.modify,response.data.create_new && response.data.modify)
                        $scope.disableAdd = (!response.data.create_new);
                        $scope.disableUpd = (!response.data.modify);
                        $scope.disableDel = (!response.data.remove);
                        //$scope.user.dealershipSelected.push() = response.data.matchDealerAccess;
                        //console.log($scope.user.dealershipSelected,$scope.dealerships)
                });                
    }  

    $scope.selectProfile = function(index){
        //console.log($filter('filter')($scope.profileAccess, {profile_id: 1}),$scope.profile.profiles)
        $scope.profileDetails = $filter('filter')($scope.user.profileAccess, {profile_id: $scope.user.profile.profile_id});
    }

    $scope.selectDealer = function(index){
        var count = $scope.user.dealerships.length;

        for (var i = 0; i < count; i++) {
            $scope.user.dealershipSelected.push($scope.user.dealerships[i]);
        }
    }


    $scope.changeDealerAccess = function(index){
        
        var count = $scope.dealerAccessDetails.length;


        $scope.dealerAccessDetails[index].allow = ($scope.dealerAccessDetails[index].allow == '1')?'0':'1'; 
        $scope.user.dealerAccessDetails = [];
        for (var i = 0; i < count; i++) {
            $scope.user.dealerAccessDetails.push($scope.dealerAccessDetails[i]);
        }
    }

    $scope.changeAccess = function(index){
        
        //var changeLine = $scope.profileDetails[index].profile_menu_access_id;
        var count = $scope.profileDetails.length;

        //console.log(index,changeLine,$scope.profileDetails[index])

        $scope.profileDetails[index].allow = ($scope.profileDetails[index].allow == '1')?'0':'1'; 
        $scope.user.profileDetails = [];
        for (var i = 0; i < count; i++) {
            $scope.user.profileDetails.push($scope.profileDetails[index]);
        }
    }

    $scope.submitForm = function(typeSubmit) {
        $scope.user.dealerAccessDetails = $scope.dealerAccessDetails;
        $scope.user.profileDetails = $scope.profileDetails;

        formData = $scope.user;
        //console.log($scope.user)
        //return;
        //$http.defaults.headers.post = 'application/upload';

        $http.post("includes/clases/controladores/AppController.php?accion=Action/User/Save&output=json", JSON.stringify(formData))
        .success(function(response){
            alert(response.message);
            
            $location.path('/User');  
        });
    };

    $scope.exit = function(){
        $location.path('/User');
    }
});

angular.module('upboard-app')
.controller('UserNew', function ($scope, $http,$routeParams,$location,$filter,FileUploader) {
    
    $scope.user = {
        name:'',
        username:'',
        profileDefaults:[],
        profileAccess:[],
        profileDealerAccess:[],        
        profileDetails:[],
        dealerAccess:[],
        password:'',
        repeatpassword:'',
        phone:'',
        dealerships:[],
        dealershipSelected:[]
    };
    $scope.menuoptions = [];
    $scope.profileDetails = [];
    $scope.dealerships = [];
    $scope.dealerAccess=[]  ;
    $scope.carriers = [];

    $http.get("includes/clases/controladores/AppController.php",{params:{
                    accion:'Action/Setting/Get_Carrier_List',
                    output:'json'
    }})
    .then(function(response) {
            angular.copy(response.data.matches, $scope.carriers);
    });

    $http.get("includes/clases/controladores/AppController.php",{params:{
            accion:'Action/Setting/Get_Menu_Options',
            output:'json'
    }})
    .then(function(response) {
            angular.copy(response.data.matches, $scope.menuoptions);
    });

    $http.get("includes/clases/controladores/AppController.php",{params:{
            accion:'Action/Setting/Get_Dealer_List',
            output:'json'
    }})
    .then(function(response) {
            angular.copy(response.data.matches, $scope.dealerships);
            $scope.user.dealerships = $scope.dealerships[0];
    });

    $http.get("includes/clases/controladores/AppController.php",{params:{
            accion:'Action/User/Get_Profile_List',
            output:'json'
    }})
    .then(function(response) {
            angular.copy(response.data.matches, $scope.user.profileDefaults);
            angular.copy(response.data.matchesProfilesDetails, $scope.user.profileAccess);
    });

    $scope.selectProfile = function(index){
        //console.log($filter('filter')($scope.profileAccess, {profile_id: 1}),$scope.profile.profiles)
        $scope.profileDetails = $filter('filter')($scope.user.profileAccess, {profile_id: $scope.user.profile.profile_id});
    }

    $scope.selectDealer = function(index){
        var count = $scope.user.dealerships.length;

        for (var i = 0; i < count; i++) {
            $scope.user.dealershipSelected.push($scope.user.dealerships[i]);
        }
    }

    $scope.changeDealerAccess = function(index){
        
        //var changeLine = $scope.profileDetails[index].profile_menu_access_id;
        var count = $scope.dealerships.length;

        //console.log(index,changeLine,$scope.profileDetails[index])

        $scope.dealerships[index].allow = ($scope.dealerships[index].allow == '1')?'0':'1'; 
        $scope.user.dealerships = [];
        for (var i = 0; i < count; i++) {
            $scope.user.dealerships.push($scope.dealerships[i]);
        }
        /*
        $scope.auxProfiles = $scope.profileAccess;
        $scope.profileAccess = [];*/
        //angular.copy($scope.profileDetails, $scope.profileAccess);
        //$scope.profileDetails = $filter('filter')($scope.profileAccess, {profile_id: $scope.profile.profiles.profile_id});
    }

    $scope.changeAccess = function(index){
        
        var changeLine = $scope.profileDetails[index].profile_menu_access_id;
        var count = $scope.user.profileAccess.length;

        //console.log(index,changeLine,$scope.profileDetails[index])

        $scope.profileDetails[index].allow = ($scope.profileDetails[index].allow == '1')?'0':'1'; 

        for (var i = 0; i < count; i++) {
            //console.log($scope.profileAccess[i].profile_menu_access_id , changeLine)
            if($scope.user.profileAccess[i].profile_menu_access_id == changeLine){
                //console.log($scope.profileAccess[i].allow)
                $scope.user.profileAccess[i].allow = $scope.profileDetails[index].allow;
                //console.log($scope.profileAccess[i].allow)
                break;
            }
        }
        /*
        $scope.auxProfiles = $scope.profileAccess;
        $scope.profileAccess = [];*/
        //angular.copy($scope.profileDetails, $scope.profileAccess);
        //$scope.profileDetails = $filter('filter')($scope.profileAccess, {profile_id: $scope.profile.profiles.profile_id});
    }


    $scope.validatePassword = function(){
        if($scope.user.password != $scope.user.repeatpassword){
            $scope.user.password = $scope.user.repeatpassword = '';
            alert('Password do not match');
        }
    }



    $scope.submitForm = function(typeSubmit) {
        formData = $scope.user;
        //$http.defaults.headers.post = 'application/upload';

        $http.post("includes/clases/controladores/AppController.php?accion=Action/User/Save&output=json", JSON.stringify(formData))
        .success(function(response){
            alert(response.message);
            
            $location.path('/User');        
            //$location.path('/');

            /*var canvasData = $scope.foto_hidden;
            var ajax2 = new XMLHttpRequest();
            ajax2.open("POST",'includes/clases/controladores/AppController.php?accion=Action/User/Save&output=json&op=addImage&user_id='+user_id,false);
            ajax2.setRequestHeader('Content-Type', 'application/upload');
            //var dataURL = canvas.toDataURL('image/png');  
            ajax2.send(canvasData);*/


        });
    };

    $scope.exit = function(){
        $location.path('/User');
    }
    
});


angular.module('upboard-app')
.controller('ProfileList', function ($scope,$http,$routeParams,$location,FileUploader) {
    $scope.inicio = 1;
    $scope.fin = 30;
    $scope.maxSize = 10;
    $scope.profiles = [];
    $scope.predicate = 'description';
    $scope.reverse = false;
    $scope.dir = "asc";     
    $scope.currentPage = 1;
    $scope.disableAdd = false;
    $scope.disableUpd = false;
    $scope.disableDel = false;
    
    $http.get("includes/clases/controladores/AppController.php",{params:{
        accion:'Action/User/Get_Profile_List',
        output:'json',
        pagina:$scope.currentPage,
        orden:$scope.predicate,
        dir:$scope.dir
    }})
    .then(function(response) {
            $scope.totalItems = response.data.totalCount;
            $scope.inicio  = response.data.inicio;
            $scope.fin  = response.data.fin;
            angular.copy(response.data.matches, $scope.profiles);
            //console.log("aqui",response.data.busquedaS);
            if(response.data.create_new == 1)
                            response.data.create_new = true;
                        else
                            response.data.create_new = false;
                        if(response.data.modify == 1)
                                response.data.modify = true;
                        else
                            response.data.modify = false;
                        if(response.data.remove == 1)
                                response.data.remove = true;
                        else
                            response.data.remove = false;
                        //console.log(response.data.create_new , response.data.modify,response.data.create_new && response.data.modify)
                        $scope.disableAdd = (!response.data.create_new);
                        $scope.disableUpd = (!response.data.modify);
                        $scope.disableDel = (!response.data.remove);

    });
});

angular.module('upboard-app')
.controller('ProfileNew', function ($scope, $http,$routeParams,$location,FileUploader) {
    $scope.menuoptions = [];
    $scope.allow_all = 0;
    
    $scope.profile = {
        description:'',
        profileDetails:[]
    }
    


    $http.get("includes/clases/controladores/AppController.php",{params:{
            accion:'Action/Setting/Get_Menu_Options',
            output:'json'
    }})
    .then(function(response) {
            angular.copy(response.data.matches, $scope.profile.profileDetails);
            angular.copy(response.data.matches, $scope.menuoptions);
    });

    $scope.addPermissions = function(){
        //console.log($scope.menuoptions)
        //console.log($scope.profileAccess)
        //console.log($scope.profileDefaults)
        ///console.log($scope.profileDetails)

        //$scope.profileDefaults.push({description:$scope.profile.description,profile_id:'NP'+$scope.newProfileId});

        for (var i = 0; i < $scope.menuoptions.length; i++) {
            var allowMenu = 0;
            for (var j = 0; j < $scope.profile.menu_options.length; j++) {
                if($scope.menuoptions[i].menu_id == $scope.profile.menu_options[j].menu_id){
                    allowMenu = 1;
                    break;
                }
            }
            $scope.profile.profileDetails.push({
                    allow:allowMenu,
                    menu:$scope.menuoptions[i].menu,
                    menu_id:$scope.menuoptions[i].menu_id,
                    parent_menu:$scope.menuoptions[i].parent_menu,
                    parent_menu_id:$scope.menuoptions[i].parent_menu_id
            }); 
        }
        /*for (var i = 0; i < $scope.profileDealerGroup.length; i++) {
            var allowDealer = 0;
            for (var j = 0; j < $scope.profile.dealer_access.length; j++) {
                if($scope.profileDealerGroup[i].dealership_id == $scope.profile.dealer_access[j].dealership_id){
                    allowDealer = 1;
                    break;
                }
            }
            $scope.install.profileDealerAccess.push({
                    allow:allowDealer,
                    dealership_id:$scope.profileDealerGroup[i].dealership_id,
                    profile_id:'NP'+$scope.newProfileId,
            }); 
        }*/
        $scope.newProfileId++;        
    }

    $scope.changeAccessAll = function(){
        $scope.allow_all = ($scope.allow_all == '1')?'0':'1'; 

        var count = $scope.profile.profileDetails.length;
        for (var i = 0; i < $scope.profile.profileDetails.length; i++) {
           $scope.profile.profileDetails[i].allow = ($scope.allow_all == '1')?'1':'0';  
        }
    }

    $scope.changeAccess = function(index){
        
        var changeLine = $scope.profile.profileDetails[index].profile_menu_access_id;
        

        //console.log(index,changeLine,$scope.profileDetails[index])

        $scope.profile.profileDetails[index].allow = ($scope.profile.profileDetails[index].allow == '1')?'0':'1'; 
        /*
        for (var i = 0; i < count; i++) {
            //console.log($scope.profileAccess[i].profile_menu_access_id , changeLine)
            if($scope.install.profileAccess[i].profile_menu_access_id == changeLine){
                //console.log($scope.profileAccess[i].allow)
                $scope.install.profileAccess[i].allow = $scope.profileDetails[index].allow;
                //console.log($scope.profileAccess[i].allow)
                break;
            }
        }*/
        /*
        $scope.auxProfiles = $scope.profileAccess;
        $scope.profileAccess = [];*/
        //angular.copy($scope.profileDetails, $scope.profileAccess);
        //$scope.profileDetails = $filter('filter')($scope.profileAccess, {profile_id: $scope.profile.profiles.profile_id});
    }

    $scope.submitForm = function(typeSubmit) {
        if($scope.profile.create_new == true)
            $scope.profile.create_new = '1';
        else
            $scope.profile.create_new = '0';

        if($scope.profile.modify == true)
            $scope.profile.modify = '1';
        else
            $scope.profile.modify = '0';

        if($scope.profile.remove == true)
            $scope.profile.remove = '1';
        else
            $scope.profile.remove = '0';

        formData = $scope.profile;

        $http.post("includes/clases/controladores/AppController.php?accion=Action/User/Save_Profile&output=json", JSON.stringify(formData))
        .success(function(response){
            alert(response.message);
            if (typeSubmit==1) {
                $location.path('/UserProfile');
            };            
            document.getElementById("form-profile").reset();
            $scope.profile.profileDetails = [];
            angular.copy( $scope.menuoptions,$scope.profile.profileDetails);
            document.getElementById("description").focus();
        });
    };

    $scope.exit = function(){
        $location.path('/UserProfile');
    }
    
});

angular.module('upboard-app')
.controller('ProfileView', function ($scope,$http,$routeParams,$location,FileUploader) {
    $scope.menuoptions = [];
    $scope.allow_all = 0;
    
    $scope.profile = {
        description:'',
        profileDetails:[]
    }
    $scope.disableAdd = false;
    $scope.disableUpd = false;
    $scope.disableDel = false;

    $scope.profile_id = $routeParams.profile_id;
    $http.get("includes/clases/controladores/AppController.php",{params:{
                    accion:'Action/User/Get_Profile',
                    output:'json',
                    profile_id:$scope.profile_id
    }})
    .then(function(response) {
        if(response.data.matches[0].create_new == 1)
                response.data.matches[0].create_new = true;
        if(response.data.matches[0].modify == 1)
                response.data.matches[0].modify = true;
        if(response.data.matches[0].remove == 1)
                response.data.matches[0].remove = true;

        $scope.profile.description = response.data.matches[0].description;
        $scope.profile.create_new = response.data.matches[0].create_new;
        $scope.profile.modify = response.data.matches[0].modify;
        $scope.profile.remove = response.data.matches[0].remove;
        ///$scope.profile = response.data.matches[0];
        angular.copy(response.data.matchesAccess, $scope.profile.profileDetails);
        //console.log(response.data.matches[0])
        if(response.data.create_new == 1)
                            response.data.create_new = true;
                        else
                            response.data.create_new = false;
                        if(response.data.modify == 1)
                                response.data.modify = true;
                        else
                            response.data.modify = false;
                        if(response.data.remove == 1)
                                response.data.remove = true;
                        else
                            response.data.remove = false;
                        //console.log(response.data.create_new , response.data.modify,response.data.create_new && response.data.modify)
                        $scope.disableAdd = (!response.data.create_new);
                        $scope.disableUpd = (!response.data.modify);
                        $scope.disableDel = (!response.data.remove);
    });

    $scope.changeAccessAll = function(){
        $scope.allow_all = ($scope.allow_all == '1')?'0':'1'; 

        var count = $scope.profile.profileDetails.length;
        for (var i = 0; i < $scope.profile.profileDetails.length; i++) {
           $scope.profile.profileDetails[i].allow = ($scope.allow_all == '1')?'1':'0';  
        }
    }
    
    $scope.changeAccess = function(index){
        
        var changeLine = $scope.profile.profileDetails[index].profile_menu_access_id;
        var count = $scope.profile.profileDetails.length;

        $scope.profile.profileDetails[index].allow = ($scope.profile.profileDetails[index].allow == '1')?'0':'1'; 
    }

    $scope.submitForm = function(typeSubmit) {
        if($scope.profile.create_new == true)
            $scope.profile.create_new = '1';
        else
            $scope.profile.create_new = '0';

        if($scope.profile.modify == true)
            $scope.profile.modify = '1';
        else
            $scope.profile.modify = '0';

        if($scope.profile.remove == true)
            $scope.profile.remove = '1';
        else
            $scope.profile.remove = '0';


        formData = $scope.profile;
        //$http.defaults.headers.post = 'application/upload';

        $http.post("includes/clases/controladores/AppController.php?accion=Action/User/Save_Profile&output=json&profile_id="+$scope.profile_id, JSON.stringify(formData))
        .success(function(response){
            alert(response.message);
            if(typeSubmit == 1){
                $scope.exit();
            }
        });
    };

    $scope.exit = function(){
        $location.path('/UserProfile/');
    }
});



angular.module('upboard-app')
.controller('MyProfile', function ($scope,$http,$routeParams,$location,FileUploader) {
    

    

    if($routeParams.user_id){
                $scope.user_id = $routeParams.user_id;
                $http.get("includes/clases/controladores/AppController.php",{params:{
                    accion:'Action/User/Get_User',
                    output:'json',
                    user_id:$scope.user_id
                }})
                .then(function(response) {
                        $scope.user = response.data.matches[0]; 
                        $scope.user.profile = response.data.matchProfile[0]; 
                });                
    }  

    $scope.submitForm = function(typeSubmit) {
        
        $http.post("includes/clases/controladores/AppController.php?accion=Action/User/Save_My_Profile&output=json&user_id="+$scope.user_id, JSON.stringify($scope.user))
        .success(function(response){
            alert(response.message);
            
            $location.path('/');  
        });
    };

    $scope.exit = function(){
        $location.path('/');
    }
});

angular.module('upboard-app')
.controller('inbox', function ($scope,$http,$routeParams,$location,FileUploader) {
    $scope.messages = [];
     
    $scope.load = function() {
        $http.get("includes/clases/controladores/AppController.php",{params:{
            accion:'Action/User/Get_User_Messages',
            output:'json'
        }})
        .then(function(response) {
                angular.copy(response.data.matches, $scope.messages);
        });
    }
    $scope.load();
});

angular.module('upboard-app').filter('time', function($filter){
 return function(input){
      if(input == null){ return ""; } 
     
      var _date = $filter('date')(new Date(input), 'HH:mm');
     
      return _date.toUpperCase();

     };
});