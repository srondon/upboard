<?php

//require_once(dirname(__FILE__)."/../includes/clases/libs/barcode/barcode.php");
// get the HTML
ob_start();
$request['receipt'] = $_GET['receipt'];
include(dirname(__FILE__).'/receipt.php');
$content = ob_get_clean();

// convert to PDF
require_once(dirname(__FILE__).'/../../includes/html2pdf/html2pdf.class.php');
try
{
	$width_in_inches = 4;
    $height_in_inches = 2.3;
    $width_in_mm = $width_in_inches * 25.4; 
	$height_in_mm = $height_in_inches * 25.4;
    $html2pdf = new HTML2PDF('P', array($width_in_mm,$height_in_mm), 'en', true, 'UTF-8', array(0, 0, 0, 0));
    
    //$html2pdf = new HTML2PDF('P', 'letter', 'en', true, 'UTF-8');
    $html2pdf->writeHTML($content);
    $html2pdf->pdf->SetDisplayMode('real');
    $html2pdf->Output('Fuel Fill Receipt.pdf');//,'D'

}
catch(HTML2PDF_exception $e) {
    echo $e;
    exit;
}
