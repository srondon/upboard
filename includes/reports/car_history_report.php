<?php

//require_once(dirname(__FILE__)."/../includes/clases/libs/barcode/barcode.php");
// get the HTML
ob_start();
$request['car'] = $_GET['car'];
include(dirname(__FILE__).'/car_history.php');
$content = ob_get_clean();

// convert to PDF
require_once(dirname(__FILE__).'/../../includes/html2pdf/html2pdf.class.php');
try
{
    $html2pdf = new HTML2PDF('P', 'letter', 'en', true, 'UTF-8');
    $html2pdf->writeHTML($content);
    $html2pdf->Output('Car History Report.pdf');//,'D'

}
catch(HTML2PDF_exception $e) {
    echo $e;
    exit;
}
