<section class="content-header">
          <h1>
            Sales Person
            <!--small>Control panel</small-->
          </h1>
          <!--ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Calendar</li>
          </ol-->
</section>
<section class="content">

  <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <a ng-disabled="disableAdd" href='#/Salesperson/New' class="btn btn-info">New Sale Person</a>
                  <!--h3 class="box-title">Hover Data Table</h3-->
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="example2" class="table table-bordered table-hover">
                    <thead>
                      <tr>
                        <th>Name</th>
                        <th>DMS ID</th>
                        <th>Dealership</th>
                        <th>Active</th>
                        <th width="20%"></th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr ng-repeat="saleman in salesmen">
                        <td>{{saleman.name}}</td>
                        <td>{{saleman.adpid}}</td>
                        <td>{{saleman.dealership}}</td>
                        <td>
                          <span class="label label-{{saleman.active==1?'success':'danger'}}">{{saleman.active==1?'Yes':'No'}}</span>
                        </td>
                        <td>
                            <div class="btn-group">
                              <button type="button" class="btn btn-info">Action</button>
                              <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                                <span class="caret"></span>
                                <span class="sr-only">Toggle Dropdown</span>
                              </button>
                              <ul class="dropdown-menu" role="menu">
                                <li><a href="#/Salesperson/View/{{saleman.saleman_id}}/">View</a></li>
                                <li><a ng-click="delete(saleman.saleman_id)">Delete</a></li>
                                <!--li class="divider"></li>
                                <li><a href="#">Separated link</a></li-->
                              </ul>
                            </div>
                        </td>
                      </tr>
                    </tbody>
                    <!--tfoot>
                      <tr>
                        <th>Rendering engine</th>
                        <th>Browser</th>
                      </tr>
                    </tfoot-->
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
      </section>