<?php
set_time_limit(0);
error_reporting(E_ALL);
ini_set('display_errors', 1);
if (isset($MAIN_DIR)) {
	$MAIN_DIR = $MAIN_DIR;//.'/';
}
else{
	$MAIN_DIR = '../../../';
}
require_once $MAIN_DIR.'includes/clases/ActionSalesBoard.php';
require_once $MAIN_DIR.'includes/clases/ActionSalesman.php';
require_once $MAIN_DIR.'includes/clases/ActionUser.php';
require_once $MAIN_DIR.'includes/clases/ActionDashboard.php';
require_once $MAIN_DIR.'includes/clases/ActionSetting.php';

if(!isset($_SESSION)){session_start();} 


class AppController {
	
	private $request = "";
	private $filesUploaded = "";
	private $accion = "";
	private $clase = "";
	private $operacion = "";
	private $obj = "";
	private $splitRequest = "";
    private $session = null;


        /*
	 * 
	 * 
	 */
    function __construct($filesUploaded,$request){//,Session &$session
                //$this->session = $session;
		$this->request   = $request;
		$this->filesUploaded   = $filesUploaded;
		if(!isset($this->request['file_get_contents']) && isset($request['accion'])){
			$this->splitRequest = explode("/",$this->request['accion']);
			 
			$this->clase     =  $this->splitRequest[0].$this->splitRequest[1];
			$this->accion    =  $this->splitRequest[2];
			//$this->accion    =  $request['accion'];
            if(isset($request['operacion']))
            	$this->operacion =  $request['operacion'];
            if(isset($request['start']))
            	$this->start     =  $request['start'];
            if(isset($request['limit']))
                $this->limit     =  $request['limit'];
        }
	}
	
	function validarOperacion(){
		//@TODO  validar si $request['operacion'] es una operacion valida o permitida
		
		return true;
	}
	
	
	function sistemaBloqueado(){
		//@TODO validar si el sistema ha sido bloqueado
		return false;
	}
	
		
	function sessionValida(){
		if($this->accion == "Get_Login")
			return true;

		/*$now = time(); // Checking the time now when home page starts.

        if ($now > $_SESSION['session_timeout']) {
            session_destroy();
            return false;
        }*/
        
        return true;
	}
	
	function setRequest($filesUploaded,$request){
		$this->request = $request;
		$this->filesUploaded   = $filesUploaded;
		if(!isset($this->request['file_get_contents'])){
			$this->splitRequest = explode("/",$this->request['accion']);
			 
			$this->clase     =  $this->splitRequest[0].$this->splitRequest[1];
			$this->accion    =  $this->splitRequest[2];
			//$this->accion    =  $request['accion'];
            if(isset($request['operacion']))
            	$this->operacion =  $request['operacion'];
            if(isset($request['start']))
            	$this->start     =  $request['start'];
            if(isset($request['limit']))
                $this->limit     =  $request['limit'];
        }
	}

	function ejecutar(){
            
            if($this->validarOperacion() && !$this->sistemaBloqueado() && $this->sessionValida()){
                
                //
                if(isset($this->request['file_get_contents'])){
                	$resultado = file_get_contents($this->request['ruta']);
                }
                else{
                	$this->obj = new $this->clase();
                	$resultado = call_user_func_array(array($this->obj, strtolower($this->accion)), array("_request" => $this->request,"_files" => $this->filesUploaded));
				}
				
                if (isset($this->request['output']) && $this->request['output']=="json"){
                    $this->outPutJson($resultado);
				}
                elseif (isset($this->request['output']) && $this->request['output']=="excel"){
                    header("Content-type: application/vnd.ms-excel; charset=utf-8");
                    header("Content-type: application/x-msexcel; charset=utf-8");
                    header("Content-Disposition: attachment; filename=".$resultado['file_name'].".xls");
                    header("<meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' />");
                    echo $resultado['report'];
				}
                elseif (isset($this->request['output']) && $this->request['output']=="xml"){
                    header('Content-Type: application/xml; charset=utf-8');
                    echo $resultado;
				}
                elseif (isset($this->request['output']) && $this->request['output']=="txt" && isset($this->request['file_get_contents'])){
                    //header("Content-type: application/octet-stream");
                    header("Content-Disposition: attachment; filename=".$this->request['archivo']);
                    header("Content-type: application/force-download");
                    echo $resultado;
				}
                elseif (isset($this->request['output']) && $this->request['output']=="txt"){
                    echo $resultado;
				}
                elseif (!isset($this->request['output'])){
                    return $resultado;//$this->outPutJson($resultado);
				}
				else{
                    //$this->outPutJson($resultado);
                    return $resultado;
				}
            }
            elseif( !$this->sessionValida() ){
                echo json_encode(array('success' => true,'message' => 'Session has expired.','reload' => true));
            }
            
	}
	
	function outPutJson($respuesta){
		header('Content-Type: application/json');
		echo json_encode($respuesta);
	}	
}

/*$session = new Session();
if(isset($_SESSION['session_erp_user'])){
    $session = $_SESSION['session_erp_user'];
}*/
if(isset($_REQUEST['accion'])){
	$BDController = new BDController();
	$BDController->conectar();


	foreach ($_REQUEST as $key => $value) {
		$request[$key] = $BDController->real_escape_string($_REQUEST[$key]);
	}
	$request2 = json_decode(file_get_contents('php://input'), true,512);
	if($request2){
		foreach ($request2 as $key => $value) {
			//echo $request2[$key]." -- ";
			if(!is_array($request2[$key])){
				$request[$key] = $BDController->real_escape_string($request2[$key]);
			}
			else{
				$request[$key] = $request2[$key];
			}
			
			//echo $request[$key]." -- ";
		}	
	}
	
	//print_r($request);

	$obj = new AppController($_FILES,$request);//,$session
	$obj->ejecutar();
}
/*foreach ($_FILES['archivos']['error'] as $key => $error) {
    if ($error == UPLOAD_ERR_OK) {
        $carga_file = $_FILES['archivos'];
        $nombre_archivo_tmp = $_FILES['archivos']['tmp_name'][$key];
        $nombre_archivo = $_FILES['archivos']['name'][$key];
        echo $nombre_archivo_tmp. "<br>".$nombre_archivo_tmp;
        
    }
}*/
?>