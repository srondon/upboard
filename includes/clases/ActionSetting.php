<?php

if(!isset($_SESSION)){session_start();} 
require_once $MAIN_DIR.'includes/clases/modelos/ModelSetting.php';
//if(!isset($_SESSION)){session_start();} 

class ActionSetting {
	
	private $session = "";
	
	public function get_general_setting($request){
		$sessionModelo = new ModelSetting();
		
		$response = $sessionModelo->getGeneralSetting($request);
		
		return $response;
	}
	public function save_general_setting($request){
		$sessionModelo = new ModelSetting();
		
		$response = $sessionModelo->saveGeneralSetting($request);
		
		return $response;
	}

	public function get_status_list($request){
		$sessionModelo = new ModelSetting();
		
		$response = $sessionModelo->getStatusList($request);
		
		return $response;
	}
	public function get_status($request){
		$sessionModelo = new ModelSetting();
		
		$response = $sessionModelo->getStatus($request);
		
		return $response;
	}
	public function save_status($request){
		$sessionModelo = new ModelSetting();
		
		$response = $sessionModelo->saveStatus($request);
		
		return $response;
	}

	public function get_carrier_list($request){
		$sessionModelo = new ModelSetting();
		
		$response = $sessionModelo->getCarrierList($request);
		
		return $response;
	}

	public function check_session($request){
		$now = time(); // Checking the time now when home page starts.
		$valid = true;
		$_SESSION['session_log_id'] = $_SESSION['session_log_id'];
        if ($now >= $_SESSION['session_timeout']) {
            //session_destroy();
            $valid = false;
        }		
		if($_SESSION['session_timeout_limit'] == -1) {
            //session_destroy();
            $_SESSION['session_timeout'] = time() + 60000;
            $valid = true;
        }
		return  Array('success' => true,'valid' => $valid,'session_log_id' => $_SESSION['session_log_id'], 
			'timeout' => $_SESSION['session_timeout'],
			'time' => time(),
			'timeout_limit' => $_SESSION['session_timeout_limit'],
			'timeout_remaining' => ($_SESSION['session_timeout'] - time()));
	}

	public function extend_session($request){
		$now = time(); // Checking the time now when home page starts.
		$_SESSION['session_timeout'] = $now + $_SESSION['session_timeout_limit'];
        
		return  Array('success' => true,'valid' => true);
	}

	public function change_dealership($request){
		$sessionModelo = new ModelSetting();
		
		$response = $sessionModelo->changeDealership($request);
		
		return $response;
	}










	





	public function get_car_type_list($request){
		$sessionModelo = new ModelSetting();
		
		$response = $sessionModelo->getCarTypeList($request);
		
		return $response;
	}
	public function get_car_type($request){
		$sessionModelo = new ModelSetting();
		
		$response = $sessionModelo->getCarType($request);
		
		return $response;
	}
	public function save_car_type($request){
		$sessionModelo = new ModelSetting();
		
		$response = $sessionModelo->saveCarType($request);
		
		return $response;
	}

	public function get_car_model_list($request){
		$sessionModel = new ModelSetting();
		
		$response = $sessionModel->getCarModelList($request);
		
		return $response;
	}

	public function get_car_model($request){
		$sessionModel = new ModelSetting();
		
		$response = $sessionModel->getCarModel($request);
		
		return $response;
	}

	public function save_car_model($request){
		$sessionModel = new ModelSetting();
		
		$response = $sessionModel->saveCarModel($request);
		
		return $response;
	}
	public function get_menu($request){
		$sessionModelo = new ModelSetting();
		
		$response = $sessionModelo->getMenu($request);
		
		return $response;
	}
	public function get_dealer_list($request){
		$sessionModelo = new ModelSetting();
		
		$response = $sessionModelo->getDealerList($request);
		
		return $response;
	}

	public function get_menu_options($request){
		$sessionModelo = new ModelSetting();
		
		$response = $sessionModelo->getMenuOptions($request);
		
		return $response;
	}

	public function get_default_profiles($request){
		$sessionModel = new ModelSetting();
		
		$response = $sessionModel->getDefaultProfiles($request);
		
		return $response;
	}

	public function generate_license($request){
		$sessionModel = new ModelSetting();
		
		$response = $sessionModel->generateLicense($request);
		
		return $response;
	}

	public function validate_license($request){
		$sessionModel = new ModelSetting();
		
		$response = $sessionModel->validateLicense($request);
		
		return $response;
	}

	public function install_dealership($request){
		$sessionModel = new ModelSetting();
		
		$response = $sessionModel->installDealership($request);
		
		return $response;
	}

	public function get_exception_vehicle_list($request){
		$sessionModel = new ModelSetting();
		
		$response = $sessionModel->getExceptionVehicleList($request);
		
		return $response;
	}

	public function get_exception_vehicle($request){
		$sessionModel = new ModelSetting();
		
		$response = $sessionModel->getExceptionVehicle($request);
		
		return $response;
	}

	public function save_exception_vehicle($request){
		$sessionModel = new ModelSetting();
		
		$response = $sessionModel->saveExceptionVehicle($request);
		
		return $response;
	}
}
/*
8220
8217
8227
8225
8222
8226
8233
8221
8223
8224
8230
8229
*/
?>
