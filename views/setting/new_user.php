<section class="content-header">
          <h1>
            New User
            <!--small>Control panel</small-->
          </h1>
          <!--ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Calendar</li>
          </ol-->
</section>
<section class="content" id='new-user-mod'>

  <div class="row" >
    <div class="col-md-10">
    <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title"></h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form role="form">
                    <div class="box-body">
                    <div class="row">
                      <div class="col-xs-3">
                        <div class="form-group">
                          <label for="exampleInputEmail1">Name</label>
                          <input type="text" class="form-control" ng-model="user.name" id="name" placeholder="Enter name">
                        </div>

                        <div class="form-group">
                          <label for="exampleInputEmail1">e-mail</label>
                          <input type="email" class="form-control" ng-model="user.username" id="username" placeholder="Enter email">
                        </div>

                        <div class="form-group">
                          <label for="exampleInputEmail1">Password</label>
                          <input type="password" class="form-control" ng-model="user.password" id="password" placeholder="Enter password">
                        </div>

                        <div class="form-group">
                          <label for="exampleInputEmail1">Repeat Password</label>
                          <input type="password" class="form-control" ng-model="user.repeatpassword" ng-blur='validatePassword()' id="repeatpassword" placeholder="Enter password again"> 
                        </div>

                        <div class="form-group">
                          <label for="exampleInputEmail1">Phone</label>
                          <input type="text" class="form-control" ng-model="user.phone" id="phone" placeholder="Enter phone">
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Carrier</label>
                            <select ng-init="user.notification_carrier_id=1" ng-model="user.notification_carrier_id" id="notification_carrier_id" class="form-control">
                              <option ng-repeat="carrier in carriers" value="{{carrier.notification_carrier_id}}">
                                  {{carrier.name}}
                              </option>
                            </select>
                          </div>
                        

                      </div>
                      <div class="col-xs-2">
                              <div class="form-group">
                                <label>Profiles</label>
                                <select ng-change="selectProfile()" size="10" class="form-control" ng-model="user.profile" ng-options="profileDefault.description for profileDefault in user.profileDefaults">
                                </select>
                              </div>
                              <!--div class="form-group">
                                <label>Dealerships Access</label>
                                <select ng-change="selectDealer()" size="10" multiple class="form-control" ng-model="user.dealerships" ng-options="dealership.company_name for dealership in dealerships">
                                </select>
                              </div-->
                              <div class="form-group">
                                <label>Dealerships Access</label>                             
                                  <table class="table table-bordered">
                                    <tr ng-repeat="dealer_access in dealerships">
                                      <td style="width: 90%">
                                          {{dealer_access.company_name}}
                                      </td>
                                      <td style="width: 10%">
                                          <span style="cursor:pointer" ng-click='changeDealerAccess($index)' class="label label-{{dealer_access.allow==1?'success':'danger'}}">{{dealer_access.allow==1?'Yes':'No'}}</span>
                                      </td>
                                    </tr>                              
                                  </table>
                              </div>
                      </div>
                      <div class="col-xs-4">
                              <div class="form-group">
                                <label>Menu Options Access</label>                                
                                  <table class="table table-bordered">
                                    <tr ng-repeat="profile_access in profileDetails">
                                      <td style="width: 45%">
                                          {{profile_access.parent_menu}}
                                      </td>
                                      <td style="width: 45%">
                                          {{profile_access.menu}}
                                      </td>
                                      <td style="width: 10%">
                                          <span style="cursor:pointer" ng-click='changeAccess($index)' class="label label-{{profile_access.allow==1?'success':'danger'}}">{{profile_access.allow==1?'Yes':'No'}}</span>
                                      </td>
                                    </tr>                              
                                  </table>
                              </div>
                            </div>  
                    </div>
                    <div class='row'>
                            
                            
                            
                          </div>
                    <div class="row">
                      <div class="col-xs-4">
                          <div class="form-group">
                              
                          </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-xs-3" style="text-align:left">
                        
                      </div>
                    </div>
                    
                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <!--button ng-click="submitForm(0)" class="btn btn-primary">Submit and Continue</button-->
                    <button ng-click="submitForm(1)" class="btn btn-primary">Submit and Exit</button>
                    <button ng-click="submitForm(2)" class="btn btn-primary">Submit and Exit</button>
                    <button ng-click="exit()" style="float:right" class="btn btn-danger">Exit</button>
                  </div>
                </form>
              </div>        
    
  </div>
  </div>

<script type="text/javascript">

  
</script>



</section>
   <script>
   
  </script>