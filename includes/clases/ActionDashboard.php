<?php

require_once $MAIN_DIR.'includes/clases/modelos/ModelDashboard.php';
//if(!isset($_SESSION)){session_start();} 

class ActionDashboard {
	
	private $session = "";
	
	public function get_data($request){
		$sessionModelo = new ModelDashboard();
		
		$response = $sessionModelo->getData($request);
		
		return $response;
	}

	public function save_notification($request){
		$sessionModelo = new ModelDashboard();
		
		$response = $sessionModelo->saveNotification($request);
		
		return $response;	
	}
}

?>