<section class="content-header">
          <h1>
            View Sales Person
            <!--small>Control panel</small-->
          </h1>
          <!--ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Calendar</li>
          </ol-->
</section>
<section class="content">

  <div class="row">
    <div class="col-md-8">
    <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title"></h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form name="salemanform" id="new-saleman-form"  role="form">
                    <div class="box-body">
                    <div class="row">
                      <div class="col-xs-4">
                        <div class="form-group">
                          <label for="exampleInputEmail1">Name</label>&nbsp;<span style="color: red;font-weight: bolder;">*</span>
                          <input type="text" class="form-control" ng-model="saleman.name" id="name" placeholder="Enter name">
                        </div>

                        
                      </div>  
                      
                      <div class="col-xs-3">
                          <div class="form-group">
                            <label for="exampleInputEmail1">DMS ID</label>
                            <input type="text" class="form-control" ng-model="saleman.adpid" id="adpid">
                          </div>
                      </div>

                      <div class="col-xs-4">
                          <div class="form-group">
                            <label for="exampleInputEmail1">email</label>&nbsp;<span style="color: red;font-weight: bolder;">*</span>
                          <span class="error" ng-show="salemanform.email.$error.pattern">Invalid email!</span>
                            <!--input type="hidden" ng-model="status.status_id" id="status_id"-->
                            <input type="email" class="form-control" ng-model="saleman.email"  ng-pattern="emailpat" name="email" id="email" placeholder="Enter email" required>
                          </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-xs-4">
                        <div class="form-group">
                          <label for="exampleInputEmail1">Phone</label>&nbsp;<span style="color: red;font-weight: bolder;">*</span>
                          <input type="text" class="form-control" ng-model="saleman.phone" id="phone" placeholder="Enter phone">
                        </div>

                        
                      </div>  
                      
                      <div class="col-xs-3">
                          <div class="form-group">
                            <label for="exampleInputEmail1">Carrier</label>
                            <select ng-model="saleman.notification_carrier_id" id="notification_carrier_id" class="form-control">
                              <option ng-repeat="carrier in carriers" value="{{carrier.notification_carrier_id}}">
                                  {{carrier.name}}
                              </option>
                            </select>
                          </div>
                      </div>

                      <div class="col-xs-4">
                        <div class="form-group">
                          <label for="exampleInputEmail1">Active</label>&nbsp;<span style="color: red;font-weight: bolder;"></span>
                          <input type="checkbox" ng-model="saleman.active" id="active" placeholder="Enter active">
                        </div>

                        
                      </div>  
                    </div>
                    
                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button ng-disabled="disableUpd" ng-click="submitForm(1)" class="btn btn-primary">Submit and Exit</button>
                    <button ng-click="exit()" style="float:right" class="btn btn-danger">Exit</button>
                  </div>
                </form>
              </div>        
    
  </div>
  </div>
</section>