<?php 
    session_start();  

    $MAIN_DIR = "";
    require_once("includes/clases/controladores/AppController.php");
    $controlador = new AppController($_FILES,$_REQUEST);//,$session
    $request['accion'] = "Action/Setting/Get_General_Setting";
    $controlador->setRequest('',$request);
    $result = $controlador->ejecutar();

    /*$_SESSION['name'] = 'Nombre de TEST';
      $_SESSION['username'] = 'admin';
      $_SESSION['user_type'] = 'Admin';
      $_SESSION['foto'] = ($foto!="")?$foto:'no-avatar.jpg';
      $_SESSION['user_id'] = 1;
    */

    $_SESSION['PHP_TIMEZONE'] = $result['matches'][0]['time_zone'];
    date_default_timezone_set($_SESSION['PHP_TIMEZONE']);
    $now = new DateTime();
    $mins = $now->getOffset() / 60;
    $sgn = ($mins < 0 ? -1 : 1);
    $mins = abs($mins);
    $hrs = floor($mins / 60);
    $mins -= $hrs * 60;
    $offset = sprintf('%+d:%02d', $hrs*$sgn, $mins);

    $_SESSION['MYSQL_TIMEZONE'] = $offset;

    $now = time(); // Checking the time now when home page starts.

    if($now >= $_SESSION['session_timeout'] && $_SESSION['session_timeout_limit'] != -1) {
      session_destroy();
      header("Location:login.php");
    }
    else if($_SESSION['session_timeout_limit'] == -1){
        $_SESSION['session_timeout'] = time() + 60000;
    }
    
    if(!isset($_SESSION['name'])){
      header("Location:login.php");
    }

    //$controlador = new AppController($_FILES,$_REQUEST);//,$session
    $request['accion'] = "Action/User/Get_User_Messages";
    $request['message_read'] = 0;
    $controlador->setRequest('',$request);
    $resultMessages = $controlador->ejecutar();

    $modalMessagesWarning = -1;

    if($_SESSION['firts_load'] == 1 && $resultMessages['totalCount'] != 0){
      $_SESSION['firts_load'] = 0;
      $modalMessagesWarning = 1;

    }

    $request['accion'] = "Action/User/Get_User_List";
    $request['user_id_messages'] = $_SESSION['user_id'];
    $controlador->setRequest('',$request);
    $resultUsers = $controlador->ejecutar();

    $request['accion'] = "Action/Setting/Get_Dealer_List";
    $request['user_id'] = $_SESSION['user_id'];
    $controlador->setRequest('',$request);
    $resultDealers = $controlador->ejecutar();

    $request['accion'] = "Action/Setting/Get_Menu";
    $controlador->setRequest('',$request);
    $resultMenu = $controlador->ejecutar();


    function isMobile(){
      $uaFull = strtolower($_SERVER['HTTP_USER_AGENT']);
      $uaStart = substr($uaFull, 0, 4);

      $uaPhone = [ // use `= array(` if PHP<5.4
          '(android|bb\d+|meego).+mobile',
          'avantgo',
          'bada\/',
          'blackberry',
          'blazer',
          'compal',
          'elaine',
          'fennec',
          'hiptop',
          'iemobile',
          'ip(hone|od)',
          'iris',
          'kindle',
          'lge ',
          'maemo',
          'midp',
          'mmp',
          'mobile.+firefox',
          'netfront',
          'opera m(ob|in)i',
          'palm( os)?',
          'phone',
          'p(ixi|re)\/',
          'plucker',
          'pocket',
          'psp',
          'series(4|6)0',
          'symbian',
          'treo',
          'up\.(browser|link)',
          'vodafone',
          'wap',
          'windows ce',
          'xda',
          'xiino',
          'iPad'
      ]; // use `);` if PHP<5.4

      $uaMobile = [ // use `= array(` if PHP<5.4
          '1207', 
          '6310', 
          '6590', 
          '3gso', 
          '4thp', 
          '50[1-6]i', 
          '770s', 
          '802s', 
          'a wa', 
          'abac|ac(er|oo|s\-)', 
          'ai(ko|rn)', 
          'al(av|ca|co)', 
          'amoi', 
          'an(ex|ny|yw)', 
          'aptu', 
          'ar(ch|go)', 
          'as(te|us)', 
          'attw', 
          'au(di|\-m|r |s )', 
          'avan', 
          'be(ck|ll|nq)', 
          'bi(lb|rd)', 
          'bl(ac|az)', 
          'br(e|v)w', 
          'bumb', 
          'bw\-(n|u)', 
          'c55\/', 
          'capi', 
          'ccwa', 
          'cdm\-', 
          'cell', 
          'chtm', 
          'cldc', 
          'cmd\-', 
          'co(mp|nd)', 
          'craw', 
          'da(it|ll|ng)', 
          'dbte', 
          'dc\-s', 
          'devi', 
          'dica', 
          'dmob', 
          'do(c|p)o', 
          'ds(12|\-d)', 
          'el(49|ai)', 
          'em(l2|ul)', 
          'er(ic|k0)', 
          'esl8', 
          'ez([4-7]0|os|wa|ze)', 
          'fetc', 
          'fly(\-|_)', 
          'g1 u', 
          'g560', 
          'gene', 
          'gf\-5', 
          'g\-mo', 
          'go(\.w|od)', 
          'gr(ad|un)', 
          'haie', 
          'hcit', 
          'hd\-(m|p|t)', 
          'hei\-', 
          'hi(pt|ta)', 
          'hp( i|ip)', 
          'hs\-c', 
          'ht(c(\-| |_|a|g|p|s|t)|tp)', 
          'hu(aw|tc)', 
          'i\-(20|go|ma)', 
          'i230', 
          'iac( |\-|\/)', 
          'ibro', 
          'idea', 
          'ig01', 
          'ikom', 
          'im1k', 
          'inno', 
          'ipaq', 
          'iris', 
          'ja(t|v)a', 
          'jbro', 
          'jemu', 
          'jigs', 
          'kddi', 
          'keji', 
          'kgt( |\/)', 
          'klon', 
          'kpt ', 
          'kwc\-', 
          'kyo(c|k)', 
          'le(no|xi)', 
          'lg( g|\/(k|l|u)|50|54|\-[a-w])', 
          'libw', 
          'lynx', 
          'm1\-w', 
          'm3ga', 
          'm50\/', 
          'ma(te|ui|xo)', 
          'mc(01|21|ca)', 
          'm\-cr', 
          'me(rc|ri)', 
          'mi(o8|oa|ts)', 
          'mmef', 
          'mo(01|02|bi|de|do|t(\-| |o|v)|zz)', 
          'mt(50|p1|v )', 
          'mwbp', 
          'mywa', 
          'n10[0-2]', 
          'n20[2-3]', 
          'n30(0|2)', 
          'n50(0|2|5)', 
          'n7(0(0|1)|10)', 
          'ne((c|m)\-|on|tf|wf|wg|wt)', 
          'nok(6|i)', 
          'nzph', 
          'o2im', 
          'op(ti|wv)', 
          'oran', 
          'owg1', 
          'p800', 
          'pan(a|d|t)', 
          'pdxg', 
          'pg(13|\-([1-8]|c))', 
          'phil', 
          'pire', 
          'pl(ay|uc)', 
          'pn\-2', 
          'po(ck|rt|se)', 
          'prox', 
          'psio', 
          'pt\-g', 
          'qa\-a', 
          'qc(07|12|21|32|60|\-[2-7]|i\-)', 
          'qtek', 
          'r380', 
          'r600', 
          'raks', 
          'rim9', 
          'ro(ve|zo)', 
          's55\/', 
          'sa(ge|ma|mm|ms|ny|va)', 
          'sc(01|h\-|oo|p\-)', 
          'sdk\/', 
          'se(c(\-|0|1)|47|mc|nd|ri)', 
          'sgh\-', 
          'shar', 
          'sie(\-|m)', 
          'sk\-0', 
          'sl(45|id)', 
          'sm(al|ar|b3|it|t5)', 
          'so(ft|ny)', 
          'sp(01|h\-|v\-|v )', 
          'sy(01|mb)', 
          't2(18|50)', 
          't6(00|10|18)', 
          'ta(gt|lk)', 
          'tcl\-', 
          'tdg\-', 
          'tel(i|m)', 
          'tim\-', 
          't\-mo', 
          'to(pl|sh)', 
          'ts(70|m\-|m3|m5)', 
          'tx\-9', 
          'up(\.b|g1|si)', 
          'utst', 
          'v400', 
          'v750', 
          'veri', 
          'vi(rg|te)', 
          'vk(40|5[0-3]|\-v)', 
          'vm40', 
          'voda', 
          'vulc', 
          'vx(52|53|60|61|70|80|81|83|85|98)', 
          'w3c(\-| )', 
          'webc', 
          'whit', 
          'wi(g |nc|nw)', 
          'wmlb', 
          'wonu', 
          'x700', 
          'yas\-', 
          'your', 
          'zeto', 
          'zte\-',
          'iPad'
      ]; // use `);` if PHP<5.4

      $isPhone = preg_match('/' . implode($uaPhone, '|') . '/i', $uaFull);
      $isMobile = preg_match('/' . implode($uaMobile, '|') . '/i', $uaStart);

      if($isPhone || $isMobile) {
          return true;
      } 
      return false;
    }

?>
<!--DOCTYPE html-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
  <head>
    <meta charset="utf-8">
    <!--link rel="shortcut icon" type="image/ico" href="favion.ico"-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="" name="description"/>
    <meta content="" name="author"/>

    <link rel="icon" href="includes/images/server/favicon.ico" type="image/x-icon"/>
    <link rel="shortcut icon" href="includes/images/server/favicon.ico"/>
    <link rel="icon" type="image/png" href="includes/images/server/favicon.png"/>
    
    <!--link rel="apple-touch-icon" sizes="57x57" href="apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="  x72" href="apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="favicon-16x16.png">
    <link rel="manifest" href="manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff"-->

    <title><?php echo APP_NAME." | ".$result['matches'][0]['short_name']?></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <!--link rel="stylesheet" href="bootstrap/css/bootstrap.min.css"-->
    <link rel="stylesheet" href="includes/js/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

    <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker-bs3.css">
    <link rel="stylesheet" href="plugins/iCheck/all.css">
    <link rel="stylesheet" href="plugins/fullcalendar/fullcalendar.min.css">
    <link rel="stylesheet" href="plugins/fullcalendar/fullcalendar.print.css" media="print">
    <link rel="stylesheet" href="plugins/timepicker/bootstrap-timepicker.min.css">
    <link rel="stylesheet" href="plugins/select2/select2.min.css">

    <link rel="stylesheet" href="plugins/morris/morris.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

       <!--
        ANGULAR
    -->
    <script type="text/javascript">
        global GLOBAL_DASHBOARD = "<?php echo $_SESSION['dashboard'];?>";
        global CURRENT_DATE = "<?php echo date("m/d/Y")?>";
        global CAN_ADD = parseInt("<?php echo date("m/d/Y")?>");
        global CAN_UPDATE = parseInt("<?php echo date("m/d/Y")?>");
        global CAN_REMOVE = parseInt("<?php echo date("m/d/Y")?>");
    </script>



    <script data-require="jquery@*" data-semver="2.1.1" src="includes/js/jquery.min.js"></script>
    <script data-require="angular.js@1.3.0" data-semver="1.3.0" ng:autobind src="includes/js/angular.js"></script>
    <script src="includes/js/angular-sanitize.js"></script>
    <script src="includes/js/ui-bootstrap-tpls.js"></script>
    <script src="includes/js/angular-confirm.js"></script>
    <!--script src="includes/js/angular.min.js"></script-->
    <script type="text/javascript" src="includes/js/angular-resource.min.js"></script>
    <script data-require="ng-route@1.2.0" data-semver="1.2.0" src="includes/js/angular-route.js"></script>
    <script src="includes/js/ui-bootstrap-tpls-0.12.0.js"></script>

    <script src="includes/js/angular-animate.js"></script>
    <script src="includes/js/angular-file-upload/dist/angular-file-upload.min.js"></script>
    <script src="includes/js/app/app.js"></script>
    <script src="includes/js/checklist-model/checklist-model.js"></script>
    <script src="includes/js/app/controllers.js"></script>
    <script src="includes/js/ngDialog.js"></script>
    <script src="includes/js/angular-multi-check.js"></script>

    <link href="includes/js/xeditable.css" rel="stylesheet">
    <script src="includes/js/xeditable.js"></script>
    <!--script type="text/javascript" src="includes/js/angular-mocks.js"></script-->

    <style>
      .opacity_table {
        background-color: gray;
        filter: alpha(opacity=75);
        -moz-opacity: 0.75;
        opacity: 0.75;
        /*background: rgba(255,255,255,0.5);*/
      }
      .alert-color{
        background-color: "#ED8C9C"
      }
    </style>
  </head>
  <body class="hold-transition skin-blue sidebar-mini"  data-ng-app="upboard-app">
        
      <div class="example-modal">
            <div  id="modal-mail" class="modal modal-primary">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    
                    <h3 class="box-title"><i class="fa fa-envelope"></i>&nbsp;Send Message</h3>
                    <!--h4 class="modal-title">Modal Primary</h4-->
                  </div>
                  <div class="modal-body">


                    <div class="box box-info">
                      <div class="box-header">
                        <!--i class="fa fa-envelope"></i>
                        <h3 class="box-title">Quick Email</h3>
                        <div class="pull-right box-tools">
                          <button class="btn btn-info btn-sm" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                        </div-->
                      </div>
                      <div class="box-body">
                        <form action="" method="post">
                          <div class="form-group">
                            <label>Multiple</label>
                            <select id="recipients" class="form-control select2" multiple="multiple" data-placeholder="Send To: " style="width: 100%;">
                              <?php for ($iUsers=0; $iUsers < $resultUsers['totalCount2']; $iUsers++) { ?>
                              <option value="<?php echo $resultUsers['matches'][$iUsers]['user_id']?>"><?php echo $resultUsers['matches'][$iUsers]['name']?></option>
                              <?php }?>
                            </select>
                          </div>
                          <!--div class="form-group">
                            <input type="email" class="form-control" name="emailto" placeholder="Send to:">
                          </div-->
                          <div class="form-group">
                            <input id="subject" type="text" class="form-control" name="subject" placeholder="Subject">
                          </div>
                          <div>
                            <textarea id="message" class="textarea" placeholder="Message" style="width: 100%; height: 125px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                          </div>
                        </form>
                      </div>
                      <div class="box-footer clearfix">
                        <img src="includes/images/spinner.gif" width="40" height="40" style="visibility:hidden"  id="loading_image_mail" />
                        <button class="pull-right btn btn-default" id="sendMessage">Send <i class="fa fa-arrow-circle-right"></i></button>
                      </div>
                    </div>




                  </div>
                  <!--div class="modal-footer">
                    <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-outline">Save changes</button>
                  </div-->
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->

            <div  id="modal-read-mail" class="modal modal-primary">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    
                    <h3 class="box-title"><i class="fa fa-envelope"></i>&nbsp;Read Message</h3>
                    <!--h4 class="modal-title">Modal Primary</h4-->
                  </div>
                  <div class="modal-body">


                    <div class="box box-info">
                      <div class="box-header">
                        <!--i class="fa fa-envelope"></i>
                        <h3 class="box-title">Quick Email</h3>
                        <div class="pull-right box-tools">
                          <button class="btn btn-info btn-sm" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                        </div-->
                      </div>
                      <div class="box-body">
                        <form action="" method="post">
                          <div class="form-group">
                            <input readOnly="true" id="from" type="email" class="form-control">
                          </div>
                          <div class="form-group">
                            <input readOnly="true" id="subjectRead" type="text" class="form-control">
                          </div>
                          <div>
                            <textarea id="messageRead" readOnly="true" class="textarea" style="width: 100%; height: 125px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                          </div>
                        </form>
                      </div>
                      <div class="box-footer clearfix">
                        <button class="pull-right btn btn-default" data-dismiss="modal">Exit <i class="fa fa-arrow-circle-right"></i></button>
                      </div>
                    </div>




                  </div>
                  <!--div class="modal-footer">
                    <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-outline">Save changes</button>
                  </div-->
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->

            <div  id="modal-messages-warning" class="modal modal-primary">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h5 class="box-title"><i class="fa fa-envelope"></i>&nbsp;New Message</h5>
                  </div>
                  <div class="modal-body">


                    <div class="box box-info">
                      <div class="box-header">
                        You have <?php echo $resultMessages['totalCount']?> new messsages
                      </div>
                    </div>




                  </div>
                  <!--div class="modal-footer">
                    <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-outline">Save changes</button>
                  </div-->
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
          </div><!-- /.example-modal -->


    <div class="wrapper">

      <header class="main-header">
        <!-- Logo -->
        <a href="" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b><?php echo APP_NAME_SHORT_1?></b><?php echo APP_NAME_SHORT_2?></span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b><?php echo APP_NAME_1?></b><?php echo APP_NAME_2?><?php //echo $_SESSION['MYSQL_TIMEZONE']; ?></span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button" id="navsidebar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <div class="navbar-custom-menu">

            <ul class="nav navbar-nav">
              <li class="dropdown messages-menu">
              <img src="includes/images/loader.gif" width="40" height="40" style="display:none"  id="loading_image_change_dealership">
                <a class="dropdown-toggle" style="height:50px;">
                <select onchange="changeDealership(this)" class="form-control select2A" style="<?php echo ((isMobile())?'':'width: 250px;margin-top:15px')?>">
                  <?php for ($iDealer=0; $iDealer < $resultDealers['totalCount']; $iDealer++) { ?>
                    <option value="<?php echo $resultDealers['matches'][$iDealer]['dealership_id'] ?>" <?php if($resultDealers['matches'][$iDealer]['dealership_id'] == $_SESSION['dealership_id']){echo "selected='selected'";} ?>>
                      <?php echo $resultDealers['matches'][$iDealer][((isMobile())?'short_name':'company_name')] ?>
                    </option>
                  <?php }?>
                </select>
                </a>
              </li>
              <li class="dropdown messages-menu">
                <a data-target="#modal-mail" data-toggle="modal"  class="dropdown-toggle" tittle="Send Message">
                  <i class="fa fa-send-o"></i>
                </a>
              </li>
              <li class="dropdown messages-menu">
                <a href="" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-envelope-o"></i>
                  <span class="label label-success" id="messages-count-label"><?php echo $resultMessages['totalCount'] ?></span>
                </a>
                <ul class="dropdown-menu">
                  <li class="header">You have <span id='messages-count-header'><?php echo $resultMessages['totalCount'] ?></span> messages</li>
                  <li>
                    <!-- inner menu: contains the actual data -->
                    <ul class="menu">
                      <?php for ($iMsg=0; $iMsg < $resultMessages['totalCount']; $iMsg++) { ?>
                      <li><!-- start message '<?php echo $resultMessages['matches'][$iMsg]['name'] ?>','<?php echo $resultMessages['matches'][$iMsg]['subject'] ?>','<?php echo htmlentities($resultMessages['matches'][$iMsg]['message']) ?>', -->
                        <a data-target="#modal-read-mail" data-toggle="modal" onClick="setReadMessage('<?php echo $resultMessages['matches'][$iMsg]['notification_id'] ?>')">
                          <div class="pull-left">
                            <?php 
                                    $imageMsg = $resultMessages['matches'][$iMsg]['picture'];
                                    $fileImgMsg = "includes/images/avatars/".$imageMsg;
                                    if($imageMsg == '' || !file_exists($fileImgMsg) || !is_readable($fileImgMsg) || !fopen($fileImgMsg,"r")){
                                      $fileImgMsg = "includes/images/avatars/no-avatar.jpg";
                                    }

                            ?>
                            <img src="<?php echo $fileImgMsg?>" width='160' height='160' class="img-circle" alt="User Image">
                          </div>
                          <h4>
                            <?php echo substr($resultMessages['matches'][$iMsg]['subject'],0,12) ?>
                            <small><i class="fa fa-clock-o"></i> <?php echo $resultMessages['matches'][$iMsg]['date2']." - ".$resultMessages['matches'][$iMsg]['time2'] ?></small>
                          </h4>
                          <p><?php echo substr($resultMessages['matches'][$iMsg]['message'], 0,25)."..." ?></p>
                        </a>
                      </li><!-- end message -->
                      <?php }?>
                    </ul>
                  </li>
                  <li class="footer"><a href="#/Inbox/">Go to Inbox</a></li>
                </ul>
              </li>
              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a href="" class="dropdown-toggle" data-toggle="dropdown">
                  <!--img src="includes/images/avatars/no-avatar.jpg"" class="user-image"-->
                  <span class="hidden-xs"><?php echo $_SESSION['name']?></span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image dist/img/user2-160x160 -->
                  <li class="user-header">
                  <img src="includes/images/avatars/<?php echo $_SESSION['photo']?>" class="img-circle" alt="User Image">
                    <p>
                      <?php echo $_SESSION['profile']." | ".$_SESSION['name']?>
                      <!--small>Member since Nov. 2012</small-->
                    </p>
                  </li>
                  <!-- Menu Body -->
                  <!--li class="user-body">
                    <div class="col-xs-4 text-center">
                      <a href="#">Followers</a>
                    </div>
                    <div class="col-xs-4 text-center">
                      <a href="#">Sales</a>
                    </div>
                    <div class="col-xs-4 text-center">
                      <a href="#">Friends</a>
                    </div>
                  </li-->
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="#/MyProfile/View/<?php echo $_SESSION['user_id']?>" class="btn btn-default btn-flat">Profile</a>
                    </div>
                    <div class="pull-right">
                      <a href="login.php" class="btn btn-default btn-flat">Sign out</a>
                    </div>

                  </li>        
                </ul>
              </li>
            </ul>
          </div>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <!--div class="user-panel">
            <div class="pull-left image">
            </div>
            <div class="pull-left info">
              <p> <?php echo $_SESSION['name']?></p>
            </div>
          </div-->
          <!-- search form -->
          
          <!-- /.search form -->
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <!--li class="header">MAIN NAVIGATION</li-->
            <li class="active"><a href="#/"><i class="fa fa-dashboard active"></i> <span>Dashboard</span></a></li>

            <?php 
                  $parentMenu = "";
                  for ($iMenu=0; $iMenu < $resultMenu['totalCount']; $iMenu++) { 
                    if($parentMenu != $resultMenu['matches'][$iMenu]['parent_menu']){
                      $parentMenu = $resultMenu['matches'][$iMenu]['parent_menu'];?>
                      <?php if($iMenu != 0){?>
                          </ul>
                          </li>
                      <?php }?>
                       <?php //echo (($iMenu==0)?'active':'')?>
                      <li class="treeview" >
                        <a href="">
                          <i class="fa <?php echo $resultMenu['matches'][$iMenu]['parent_icon']?>"></i>
                          <span><?php echo $resultMenu['matches'][$iMenu]['parent_menu']?></span>
                          <i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu">
                    <?php }
                    ?>
                        <li>
                          <!-- <?php if($resultMenu['matches'][$iMenu]['hide_menu']==1){?> class="sidebar-toggle" data-toggle="offcanvas" role="button"<?php }?> -->
                          <a href="<?php echo $resultMenu['matches'][$iMenu]['target']?>" >
                            <i class="fa <?php echo $resultMenu['matches'][$iMenu]['icon']?>"></i> 
                              <?php echo $resultMenu['matches'][$iMenu]['menu']?>
                          </a>
                        </li>
                      
            <?php }?>  
                  </ul>
                    </li>
              <li><a href="login.php"><i class="fa fa-plug"></i> <span>Sign Out</span></a></li>
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content"  data-ng-view="">
          <!-- Small boxes (Stat box) -->
          

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      

      <footer class="main-footer">
        <!--div class="pull-right hidden-xs">
          <b>Version</b> 2.3.0
        </div-->
        <strong>Copyright @ 2011 Dealer Geek All Rights Reserved.
        <p style="color:#F0F0F0">Copyright 2014-2015 Almsaeed Studio. All rights reserved.</p>
      </footer>

      <!-- Control Sidebar -->
      <!--aside class="control-sidebar control-sidebar-dark">
      </aside-->
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <!--div class="control-sidebar-bg"></div-->
    </div><!-- ./wrapper -->

    <!-- jQuery 2.1.4 -->
    <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
    <script src="plugins/select2/select2.full.min.js"></script>
    <!-- Slimscroll -->
    <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    <script src="plugins/morris/morris.min.js"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.min.js"></script>
    <script src="plugins/input-mask/jquery.inputmask.js"></script>
    <script src="plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
    <script src="plugins/input-mask/jquery.inputmask.extensions.js"></script>
    <script src="plugins/daterangepicker/daterangepicker.js"></script>
    <script src="plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
    <script src="plugins/timepicker/bootstrap-timepicker.min.js"></script>
    <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- iCheck 1.0.1 -->
    <script src="plugins/iCheck/icheck.min.js"></script>
    <script src="plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="dist/js/demo.js"></script>
    <!-- fullCalendar 2.2.5 -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
    <script src="plugins/fullcalendar/fullcalendar.js"></script>
    <!-- Page specific script -->
    <script>

    // Put event listeners into place
    /*window.addEventListener("DOMContentLoaded", function() {
      // Grab elements, create settings, etc.
      var canvas = document.getElementById("canvas"),
        context = canvas.getContext("2d"),
        video = document.getElementById("video"),
        videoObj = { "video": true },
        errBack = function(error) {
          console.log("Video capture error: ", error.code); 
        };

      // Put video listeners into place
      if(navigator.getUserMedia) { // Standard
        navigator.getUserMedia(videoObj, function(stream) {
          video.src = stream;
          video.play();
        }, errBack);
      } else if(navigator.webkitGetUserMedia) { // WebKit-prefixed
        navigator.webkitGetUserMedia(videoObj, function(stream){
          video.src = window.webkitURL.createObjectURL(stream);
          video.play();
        }, errBack);
      } else if(navigator.mozGetUserMedia) { // WebKit-prefixed
        navigator.mozGetUserMedia(videoObj, function(stream){
          video.src = window.URL.createObjectURL(stream);
          video.play();
        }, errBack);
      }

      
    }, false);*/

  // Trigger photo take dentro de la funcion de arriba  
      /*document.getElementById("snap").addEventListener("click", function() {
        context.drawImage(video, 0, 0, 640, 480);
        
      });*/
      
      function setImageUser(dataURL){
        var scope = angular.element(document.getElementById('new-user-mod')).scope();
        scope.$apply(function(){
           scope.user.foto_hidden = dataURL;
        });
      }

      $(".select2").select2();
      <!--
      if (screen.width > 768) {//800
        $(".select2A").select2();
      }
      //-->

      
      <?php 
        if($modalMessagesWarning == 1){?>
          $('#modal-messages-warning').modal('show');
      <?php  }?>

      $( "#sendMessage" ).click(function() {
          if($("#recipients").val() == "" || $("#recipients").val() == "Sent To:" || $("#recipients").val() == null){
            alert("No recipients.");
            return;
          }
          if($("#subject").val() == ""){
            alert("No subject.");
            return;
          }
          if($("#message").val() == ""){
            alert("No message.");
            return;
          }
          document.getElementById('loading_image_mail').style.visibility = 'visible';
          $.ajax({
            method: "POST",
            url: "includes/clases/controladores/AppController.php",
            data: { 
              accion: "Action/User/Send_Message", 
              recipients:  $("#recipients").val().toString(),
              subject: $("#subject").val(), 
              message: $("#message").val(),
              output:'json'
            }
          })
          .done(function( msg ) {
              document.getElementById('loading_image_mail').style.visibility = 'hidden';
              $("#recipients").val("");
              $("#subject").val("");
              $("#message").val("");
              alert( msg.message );
              $('#modal-mail').modal('hide');
          });
      });

      function setReadMessage(notification_id){
        markRead(notification_id);
      }

      function reloadMessages(){
        $.ajax({
            method: "POST",
            url: "includes/clases/controladores/AppController.php",
            datatype: 'json',
            data: { 
              accion: "Action/User/Get_User_Messages",
              message_read:0,
              output:'json'
            }
          })
          .done(function( msg ) {
              //console.log(msg,msg.totalCount)
              $("#messages-count-label").html(msg.totalCount);
              $("#messages-count-header").html(msg.totalCount);
          });
      }

      function markRead(notification_id){
        $.ajax({
            method: "POST",
            url: "includes/clases/controladores/AppController.php",
            data: { 
              accion: "Action/User/Set_Message_As_Read", 
              notification_id: notification_id,
              output:'json'
            }
          })
          .done(function( msg ) {
              $("#from").val("From: "+msg.from);
              $("#subjectRead").val("Subject: "+msg.subject);
              $("#messageRead").val(msg.messageBody);
              reloadMessages();
          });
      }

      function changeDealership(selectDealer){
        document.getElementById("loading_image_change_dealership").style.display = "block";
        $.ajax({
            method: "POST",
            url: "includes/clases/controladores/AppController.php",
            data: { 
              accion: "Action/Setting/Change_Dealership", 
              dealership_id: selectDealer.value,
              output:'json'
            }
          })
          .done(function( msg ) {
              document.getElementById("loading_image_change_dealership").style.display = "none";
              location.reload();
        });
        
      }

      function goController(target,mobileTarget){

      }

      function checkSession(){
        $.ajax({
            method: "POST",
            url: "includes/clases/controladores/AppController.php",
            data: { 
              accion: "Action/Setting/Check_Session",
              output:'json'
            }
          })
          .done(function( msg ) {
            //console.log(msg);    
            if(!msg.valid){
              if(confirm("Session time is out. Do you want extend it?")){

                $.ajax({
                    method: "POST",
                    url: "includes/clases/controladores/AppController.php",
                    data: { 
                      accion: "Action/Setting/Extend_Session",
                      output:'json'
                    }
                  })
                  .done(function( msg ) {
                });

              }
              else{
                location.reload();  
              }              
            }
          });
      }
      setInterval('checkSession()',30000);
      //$.timer(30000, function(){
      //   ;
      //})
  </script>
  </body>
</html>
