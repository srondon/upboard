<?php

//require_once(dirname(__FILE__)."/../includes/clases/libs/barcode/barcode.php");
// get the HTML
ob_start();
$request['year'] = substr($_POST['month'], 3, 6);
$request['month'] = (string)((int)$_POST['month']);
$month = '';
    switch ($request['month']) {
        case '1':
            $month = 'January';
            break;
        case '2':
            $month = 'February';
            break;
        case '3':
            $month = 'March';
            break;
        case '4':
            $month = 'April';
            break;
        case '5':
            $month = 'May';
            break;
        case '6':
            $month = 'June';
            break;
        case '7':
            $month = 'July';
            break;
        case '8':
            $month = 'August';
            break;
        case '9':
            $month = 'September';
            break;
        case '10':
            $month = 'October';
            break;
        case '11':
            $month = 'November';
            break;
        default:
            $month = 'December';
            break;
    }
include(dirname(__FILE__).'/eom.php');
$content = ob_get_clean();

// convert to PDF
require_once(dirname(__FILE__).'/../../includes/html2pdf/html2pdf.class.php');
try
{
    //$html2pdf = new HTML2PDF('L', 'letter', 'en');
    
    //$width_in_inches = 4;
    //$height_in_inches = 2.3;
    //$width_in_mm = $width_in_inches * 25.4; 
	//$height_in_mm = $height_in_inches * 25.4;
    //$html2pdf = new HTML2PDF('L', array($width_in_mm,$height_in_mm), 'en', true, 'UTF-8', array(0, 0, 0, 0));
    $html2pdf = new HTML2PDF('P', 'letter', 'en', true, 'UTF-8');
    $html2pdf->writeHTML($content);
    $html2pdf->pdf->SetDisplayMode('real');
    //$html2pdf->writeHTML($content, !isset($_POST['month']));
    //$html2pdf->SetDisplayMode('real');
    $html2pdf->Output('EOM Report '.$month.' '.$request['year'].'.pdf');

}
catch(HTML2PDF_exception $e) {
    echo $e;
    exit;
}
