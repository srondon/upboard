
angular.module('upboard-app', ['checklist-model','ngRoute','ngResource','ui.bootstrap','ngDialog','angular-confirm','angularFileUpload','ngSanitize','xeditable','angular-multi-check'])
//,'ngFileUpload'
.config(['$locationProvider','$routeProvider', function ( $locationProvider, $routeProvider) {
    //$locationProvider.html5Mode(true);
        /*$locationProvider.html5Mode({
          enabled: true,
          requireBase: false
        });*/
    $routeProvider //add resolve function is pending.
        .when('/', {
            templateUrl:'views/dashboards/dashboard.php',//templateUrl: 'views/dashboards/dashboard'+GLOBAL_DASHBOARD+'.php',
            controller: 'dashboard'//controller: 'dashboard',

        })
        .when('/generalSettings/',{
            templateUrl:'views/setting/general_settings.php',
            controller: 'generalSettings',
        })
        .when('/Salesboard/',{
            templateUrl:'views/salesboard/salesboard.php',
            controller: 'salesboard',
        })
        .when('/Salesperson/',{
            templateUrl:'views/salesboard/salesmen_list.php',
            controller: 'Salesmen',
        })
        .when('/Salesperson/New',{
            templateUrl:'views/salesboard/salesmen_new.php',
            controller: 'SalesmenNew',
        })
        .when('/Salesperson/View/:saleman_id/',{
            templateUrl:'views/salesboard/salesmen_view.php',
            controller: 'SalesmenView',
        })
        .when('/StatusSalesBoard/',{
            templateUrl:'views/setting/status_list.php',
            controller: 'statusSalesBoard',
        })
        .when('/StatusSalesBoard/New',{
            templateUrl:'views/setting/status_new.php',
            controller: 'statusNew',
        })
        .when('/StatusSalesBoard/View/:status_id/',{
            templateUrl:'views/setting/status_view.php',
            controller: 'statusView',
        })
        .when('/User/',{
            templateUrl:'views/setting/user_list.php',
            controller: 'UserList',
        })
        .when('/User/New',{
            templateUrl:'views/setting/new_user.php',
            controller: 'UserNew',
        })
        .when('/User/View/:user_id/',{
            templateUrl:'views/setting/view_user.php',
            controller: 'UserView',
        })
        .when('/UserProfile/',{
            templateUrl:'views/setting/user_profile_list.php',
            controller: 'ProfileList',
        })
        .when('/UserProfile/New',{
            templateUrl:'views/setting/user_profile_new.php',
            controller: 'ProfileNew',
        })
        .when('/UserProfile/View/:profile_id/',{
            templateUrl:'views/setting/user_profile_view.php',
            controller: 'ProfileView',
        })
        .when('/Inbox/',{
            templateUrl:'views/setting/inbox.php',
            controller: 'inbox',
        })
        .when('/MyProfile/View/:user_id/',{
            templateUrl:'views/setting/my_profile.php',
            controller: 'MyProfile',
        })
        .otherwise({
            redirectTo: '/'
        });
}]);
/*
angular.module('upboard-app').constant('CAN_ADD', CAN_ADD );
angular.module('upboard-app').constant('CAN_UPDATE', CAN_UPDATE );
angular.module('upboard-app').constant('CAN_REMOVE', CAN_REMOVE );*/
upboardApp = angular.module('upboard-app');

/*upboardApp.service('appRequest', function(type,action,params,ResponseFunction) {
    // setting some default value
       this.userId = 0;
       var requestResponse = {};

       if(type == "get"){//action = Class/Method
            $http.get("includes/clases/controladores/AppController.php",paramsObject)
            .then(ResponseFunction);
       }
       else{//action = Class/Method
            $http.post("includes/clases/controladores/AppController.php?accion=Action/"+action+"&output=json", JSON.stringify(params))
            .success(ResponseFunction);
       }
});*/
