<?php

require_once $MAIN_DIR.'includes/clases/modelos/ModelSalesman.php';
//if(!isset($_SESSION)){session_start();} 

class ActionSalesman {
	
	private $session = "";
	
	public function get_salesman_list($request){
		$sessionModel = new ModelSalesman();
		
		$response = $sessionModel->getSalesmanList($request);
		
		return $response;
	}

	public function get_salesman($request){
		$sessionModel = new ModelSalesman();
		
		$response = $sessionModel->getSalesman($request);
		
		return $response;
	}

	public function save_salesman($request){
		$sessionModel = new ModelSalesman();
		
		$response = $sessionModel->saveSalesman($request);
		
		return $response;
	}

	public function delete($request){
		$sessionModel = new ModelSalesman();
		
		$response = $sessionModel->delete($request);
		
		return $response;
	}
}

?>