<?php
if(!isset($_SESSION)){session_start();} 
require_once $MAIN_DIR.'includes/clases/BDController.php';
require_once $MAIN_DIR.'includes/clases/Notifier.php';
//if(!isset($_SESSION)){session_start();} 
class ModelSalesBoard {
	
	private $bd = null;
	
	public function loadUpboardState($request){
		$this->BDController = new BDController();
		try{
			$this->BDController->conectar();
			
			$query = '';
			$andDealership = "";
			if(isset($_SESSION['dealership_id']) && $_SESSION['dealership_id'] != ''){
				$andDealership = " and g.dealership_id = '".$_SESSION['dealership_id']."'";//"'".$_SESSION['dealership_id']."'";
				$andDealership2 = " and s.dealership_id = '".$_SESSION['dealership_id']."'";//"'".$_SESSION['dealership_id']."'";
			}
			$showActives = "";
			//if((isset($request['typeahead']) || isset($request['isBox']))){
			$showActives = " and g.active = 1 ";
			//}
			$query = "SELECT * 
                    FROM saleman  g                      
                    WHERE  g.saleman_id not in (select s.saleman_id from salesboard s where 1 ".$andDealership2.") ".$andDealership." ".$showActives." 
                    ORDER BY name";
			
			$this->BDController->setQuery($query);
			$result = $this->BDController->ejecutaInstruccion();
			$matchesSalesmen = Array();
			$itemBox['name'] = "----Put In----";
			$itemBox['dpid'] = "";
			$itemBox['carrier_id'] = "";
			$itemBox['dealership_id'] = "";
			$itemBox['email'] = "";
			$itemBox['phone'] = "";
			$itemBox['saleman_id'] = 0;
			$i=0;
			while ($item = $this->BDController->fetch($result)){
				if($i == 0){
					$matchesSalesmen[] = $itemBox;					
					$i++;
				}
				$matchesSalesmen[] = $item;
			}

			$query = "SELECT d.*,sd.preserve_position  
                    FROM status d  
                    inner join status_dealership sd on sd.status_id = d.status_id and sd.dealership_id = '".$_SESSION['dealership_id']."' and sd.allow = 1 
                    WHERE  1  
                    ORDER BY status_id";
			
			$this->BDController->setQuery($query);
			$result = $this->BDController->ejecutaInstruccion();
			$matchesStatus = Array();
			while ($item = $this->BDController->fetch($result)){
				$matchesStatus[] = $item;
			}

			$matchesAvailableBoard = Array();
			$matchesActionBoard = Array();

			$query = "SELECT g.*,s.name ,st.name status,s.email   
                    FROM salesboard  g 
                    inner join saleman s on s.saleman_id = g.saleman_id 
                    inner join status st on st.status_id = g.status_id 
                    WHERE  g.status_id = 1 ".$andDealership."
                    ORDER BY g.board_order ";
			
			$this->BDController->setQuery($query);
			$result = $this->BDController->ejecutaInstruccion();
			while ($item = $this->BDController->fetch($result)){
				$matchesAvailableBoard[] = $item;
			}

			$query = "SELECT g.*,s.name ,st.name status,s.email, 
					(case when isnull(g.keep_order)!=1 then g.keep_order else 
					(case when g.board_order > 0 then g.board_order else '' end) end)
					 order_showed 
                    FROM salesboard  g 
                    inner join saleman s on s.saleman_id = g.saleman_id 
                    inner join status st on st.status_id = g.status_id 
                    WHERE  g.status_id != 1 ".$andDealership."
                    ORDER BY (case when g.status_id=10 then 0 else 1 end),g.datetoshow ";
			
			$this->BDController->setQuery($query);
			$result = $this->BDController->ejecutaInstruccion();
			while ($item = $this->BDController->fetch($result)){
				if($item['status_id'] == 1){
					$matchesAvailableBoard[] = $item;
				}
				else{
					$matchesActionBoard[] = $item;	
				}
			}

			$query = "SELECT coalesce(p.create_new,0) create_new,coalesce(p.modify,0) modify,coalesce(p.remove,0) remove,
					coalesce(p.greeter,0) greeter,coalesce(p.greeter_readonly,0) greeterRO  
                    FROM user u    
                    left join profile p on p.profile_id = u.profile_id 
					WHERE  u.user_id = '".$_SESSION['user_id']."'";
			$this->BDController->setQuery($query);
			$result = $this->BDController->ejecutaInstruccion();
			$num = $this->BDController->numero_filas($result);
			$item = $this->BDController->fetch($result);//'create_new' => $item['create_new'],'modify' => $item['modify'],'remove' => $item['remove']

			$this->BDController->desconectar();;
			return  Array('success' => true,'message' => 'Success','matchesSalesmen' => $matchesSalesmen,'matchesActionBoard' => $matchesActionBoard,'matchesAvailableBoard' => $matchesAvailableBoard,'matchesStatus' => $matchesStatus,
			'create_new' => $item['create_new'],'modify' => $item['modify'],'remove' => $item['remove'],'greeter' => $item['greeter'],'greeterRO' => $item['greeterRO']);//
		} 
		catch(Exception $e ) {
			$this->BDController->rollback();
			$this->BDController->desconectar();
			$arrayError = Array('Error No:' => $e->getCode(),'Error Message:' => $e->getMessage(),'Stack Trace:' => nl2br($e->getTraceAsString()));
			//$arrayError = 'Error No: ' . $e->getCode().' Error Message: ' . $e->getMessage().' Stack Trace: ' . nl2br($e->getTraceAsString());
			return  Array('success' => false,'message' => "Error during the operation",'error' => $arrayError);
		}
		
	}
	//Made before start working on preserve_position Status field
	public function changeSalemanStatus_FUNCTION_BACKUP($request){
		$this->BDController = new BDController();
		try{
			$this->BDController->conectar();
			$this->BDController->autocommit(FALSE);
			$saveQuery = '';
			$requestOriginal = $request;
			
			$actionInBoard = "";

			if($request['new_status_id'] == '1' && !isset($request['action'])){
				$actionInBoard = "In";
				$query = "SELECT max(g.board_order) board_order
                    FROM salesboard  g 
                    WHERE  g.dealership_id = '".$_SESSION['dealership_id']."' and g.status_id = 1";
			
				$this->BDController->setQuery($query);
				$result = $this->BDController->ejecutaInstruccion();
				$item = $this->BDController->fetch($result);

				$item['board_order'] = ($item['board_order']=='')?0:$item['board_order'];

				$saveQuery = "insert salesboard (board_order,saleman_id,dealership_id,status_id,control_date) values ('".($item['board_order']+1)."','".$request['saleman_id']."',
				'".$_SESSION['dealership_id']."',1,concat(curdate(),' ',curtime()))";

				$this->BDController->setQuery($saveQuery);
				$this->BDController->ejecutaInstruccion();
			}
			elseif($request['new_status_id'] == '7'){
				$actionInBoard = "Out";
				$saveQuery = "delete from salesboard where saleman_id = '".$request['saleman_id']."' and dealership_id = '".$_SESSION['dealership_id']."'";
				$this->BDController->setQuery($saveQuery);
				$this->BDController->ejecutaInstruccion();

				$saveQuery = "update salesboard set board_order = (board_order-1) where dealership_id = '".$_SESSION['dealership_id']."' and board_order >= '".$request['board_order']."'";
				$this->BDController->setQuery($saveQuery);
				$this->BDController->ejecutaInstruccion();
			}
			elseif($request['new_status_id'] == '6'){
				$actionInBoard = "Change Order";
				
				if($request['new_board_order'] < $request['board_order']){
					$saveQuery = "update salesboard set board_order = (board_order+1) where dealership_id = '".$_SESSION['dealership_id']."' and board_order >= '".$request['new_board_order']."' 
				                       and board_order <= '".$request['board_order']."'";	
				}
				elseif($request['new_board_order'] > $request['board_order']){
					$saveQuery = "update salesboard set board_order = (board_order-1) where dealership_id = '".$_SESSION['dealership_id']."' and board_order > '".$request['board_order']."' 
				                       and board_order <= '".$request['new_board_order']."'";	
				}
				$this->BDController->setQuery($saveQuery);
				$this->BDController->ejecutaInstruccion();
				

				$saveQuery = "update salesboard set board_order = '".$request['new_board_order']."' where saleman_id = '".$request['saleman_id']."' and dealership_id = '".$_SESSION['dealership_id']."'";
				$this->BDController->setQuery($saveQuery);
				$this->BDController->ejecutaInstruccion();
				


			}
			else{
				$actionInBoard = "Change Status";
				$datetoshow = "";
				if(isset($request['new_datetoshow'])){
					$datetoshow = ", datetoshow='".$request['new_datetoshow']."'";
				}
				$board_order = "";

				
				//Look if there is any Be-Back
				if($request['new_status_id'] != 1 && $request['new_status_id'] != 6 && $request['new_status_id'] != 7 && 
					$request['new_status_id'] != 10 && $request['new_status_id'] != 9){
					
					$query = "SELECT coalesce(min(keep_order),0) min,coalesce(max(keep_order),0)max FROM salesboard WHERE dealership_id='".$_SESSION['dealership_id']."' and isnull(keep_order)!=1";
					$this->BDController->setQuery($query);
					$result = $this->BDController->ejecutaInstruccion();
					$item = $this->BDController->fetch($result);

					if($request['board_order'] < $item['min']){

						/*$saveQuery = "update salesboard set keep_order = (keep_order-1),from_beback = (from_beback-1),keep_second_order=(keep_second_order-1)  
								 where isnull(keep_order) != 1 and keep_order > 3  and dealership_id = '".$_SESSION['dealership_id']."' ";*/
						$saveQuery = "update salesboard s 
						 inner join dealership d on d.dealership_id = s.dealership_id 
						  set s.keep_order = (s.keep_order-1),s.from_beback = (s.from_beback-1),s.keep_second_order=(s.keep_second_order-1)  
								 where isnull(s.keep_order) != 1 and s.keep_order > d.beback_min and s.dealership_id = '".$_SESSION['dealership_id']."' ";
						//echo $saveQuery;
						$this->BDController->setQuery($saveQuery);
						$this->BDController->ejecutaInstruccion();

						/*$saveQuery = "update salesboard set from_beback = (from_beback-1),keep_second_order=(keep_second_order-1) 
								 where isnull(keep_order) = 1 and keep_second_order > 3  and dealership_id = '".$_SESSION['dealership_id']."' ";*/
						$saveQuery = "update salesboard s 
						 inner join dealership d on d.dealership_id = s.dealership_id 
						  set s.from_beback = (s.from_beback-1),s.keep_second_order=(s.keep_second_order-1) 
								 where isnull(s.keep_order) = 1 and s.keep_second_order > d.beback_min and s.dealership_id = '".$_SESSION['dealership_id']."' ";
						$this->BDController->setQuery($saveQuery);
						$this->BDController->ejecutaInstruccion();
					}
				}


				
				$beback = "";
				$keep_second_order = "";
				$control_date = "";


				if($request['new_status_id'] == 10){
					$request['new_datetoshow'] = "";
					//$request['board_order'] = -1;
					$board_order = -1;

					$saveQuery = "update salesboard set board_order = (board_order-1) where dealership_id = '".$_SESSION['dealership_id']."' and board_order >= '".$request['board_order']."'  and status_id=1";
					$this->BDController->setQuery($saveQuery);
					$this->BDController->ejecutaInstruccion();
				}
				elseif($request['action'] == 'next'){
					$board_order = 0;
					$saveQuery = "update salesboard set board_order = (board_order-1)
					 where dealership_id = '".$_SESSION['dealership_id']."' and board_order > 0 and status_id=1";
					 //echo $saveQuery;
					$this->BDController->setQuery($saveQuery);
					$this->BDController->ejecutaInstruccion();
				}
				elseif($request['new_status_id'] == 9){
					//begin second order set 
					$query = "SELECT coalesce(max(keep_second_order),-1) keep_second_order FROM salesboard 
					  WHERE dealership_id='".$_SESSION['dealership_id']."' and isnull(keep_order)!=1 and keep_order='".$request['board_order']."'";
					$this->BDController->setQuery($query);
					$result = $this->BDController->ejecutaInstruccion();
					$item = $this->BDController->fetch($result);

					
					if($item['keep_second_order'] == -1){
						$keep_second_order = ", keep_second_order = '".$request['board_order']."'";//
					}
					else{
						$keep_second_order = ", keep_second_order = '".($item['keep_second_order']+1)."' ";	
					}
					//End second order set
					$beback = ",keep_order='".$request['board_order']."',from_beback='".$request['board_order']."'";
					//board_order >=
					$saveQuery = "update salesboard set board_order = (board_order-1) where dealership_id = '".$_SESSION['dealership_id']."' and board_order > '".$request['board_order']."'  and status_id=1";
					$this->BDController->setQuery($saveQuery);
					$this->BDController->ejecutaInstruccion();
				}
				elseif(($request['action'] == 'back' && $request['status_id'] != 9)){
					// ||
					//($request['action'] == 'back' && $request['status_id'] != 5 && $request['order_showed'] < 3)
					$board_order = 0;
					$beback = ",keep_order=NULL";
					$control_date = ", control_date = concat(curdate(),' ',curtime())";
					$query = "SELECT max(g.board_order) board_order
	                    FROM salesboard  g 
	                    WHERE  g.dealership_id = '".$_SESSION['dealership_id']."' and g.status_id = 1";
				
					$this->BDController->setQuery($query);
					$result = $this->BDController->ejecutaInstruccion();
					$item = $this->BDController->fetch($result);
					$board_order = ($item['board_order']=='')?1:($item['board_order']+1);
				}
				elseif($request['action'] == 'back' && $request['status_id'] == 9){// && $request['order_showed'] >= 3
					//begin cheking order for second order
					/*$query = "SELECT board_order,coalesce(max(keep_second_order),0) keep_second_order
	                    FROM salesboard  g 
	                    WHERE  g.dealership_id = '".$_SESSION['dealership_id']."' and g.status_id = 1 and 
	                     board_order='".$request['order_showed']."'";
				
					$this->BDController->setQuery($query);
					$result = $this->BDController->ejecutaInstruccion();
					$itemSecOrder = $this->BDController->fetch($result);
					
					if($itemSecOrder['keep_second_order'] == 0)
						$request['order_showed'] = $request['order_showed'];
					else if($request['keep_second_order'] < $itemSecOrder['keep_second_order'])
						$request['order_showed'] = $request['order_showed'];
					elseif($request['keep_second_order'] > $itemSecOrder['keep_second_order']){

						$request['order_showed'] = $request['order_showed'] + ($request['keep_second_order']-1);
					}*/
					//$board_order = $request['order_showed'];
					$query = "SELECT board_order,keep_order,keep_second_order,from_beback 
	                    FROM salesboard  g 
	                    WHERE  g.dealership_id = '".$_SESSION['dealership_id']."' and g.status_id = 1 and 
	                     from_beback ='".$request['order_showed']."' 
	                      order by keep_second_order asc ";
					//echo $query;
					$this->BDController->setQuery($query);
					$result = $this->BDController->ejecutaInstruccion();
					$position = $request['order_showed'];
					while ($itemSecOrder = $this->BDController->fetch($result)){
						//echo $request['keep_second_order']." < ".$itemSecOrder['keep_second_order']; 
						if($request['keep_second_order'] < $itemSecOrder['keep_second_order']){
							$request['order_showed'] = $position;//$request['order_showed'];
							break;
						}
						else{//if($request['keep_second_order'] < $itemSecOrder['keep_second_order'])
							//$request['order_showed'] = $request['order_showed'] + 1;
							$position++;
							$request['order_showed'] = $position;
							//break;	
						}
					}
					
					//echo $board_order;

					//end cheking order for second order
					$board_order = $request['order_showed'];
					$beback = ",keep_order=NULL";
					//$keep_second_order = ", keep_second_order = 0";

					$query = "SELECT max(g.board_order) board_order
	                    FROM salesboard  g 
	                    WHERE  g.dealership_id = '".$_SESSION['dealership_id']."' and g.status_id = 1";
				
					$this->BDController->setQuery($query);
					$result = $this->BDController->ejecutaInstruccion();
					$itemOrder = $this->BDController->fetch($result);

					if($itemOrder['board_order'] != '' && ($request['order_showed'] <= $itemOrder['board_order'] && $request['order_showed'] >= 1) ){
						$saveQuery = "update salesboard set board_order = (board_order+1) where dealership_id = '".$_SESSION['dealership_id']."' and board_order >= '".$request['order_showed']."'  and status_id=1";
						$this->BDController->setQuery($saveQuery);
						$this->BDController->ejecutaInstruccion();
					}
					else{
						$board_order = ($itemOrder['board_order']=='')?1:($itemOrder['board_order']+1);
					}
				}
				else{
					///$board_order=0
					$saveQuery = "update salesboard set board_order = (board_order-1)
					 where dealership_id = '".$_SESSION['dealership_id']."' and board_order > '".$request['board_order']."' and status_id=1";
					 //echo $saveQuery;..
					$this->BDController->setQuery($saveQuery);
					$this->BDController->ejecutaInstruccion();
				}
				//@TODO
				// RESOLVER ORDEN CUANDO HAY VARISO BEBACKS. MANTENER EL ORDEN EN EL QUE LLEGAN A LA LISTA DE ACCION
				//PODRIA SER ORDENANDO POR LA FECHA DE DATETOSHOW

				//+1

				

				$saveQuery = "update salesboard set status_id = '".$request['new_status_id']."' ".$datetoshow.",board_order='".$board_order."' ".$beback." ".$keep_second_order."  ".$control_date." 
				 where saleman_id = '".$request['saleman_id']."' and dealership_id = '".$_SESSION['dealership_id']."'";
				 //echo $saveQuery;
				$this->BDController->setQuery($saveQuery);
				$this->BDController->ejecutaInstruccion();

				//After move back a BeBack
				//Check if there is no more beback quoe to reset keep_second_order and from_beback fields
				if($request['action'] == 'back' && $request['status_id'] == 9){
					$query = "SELECT board_order,keep_order,keep_second_order,from_beback 
	                    FROM salesboard  g 
	                    WHERE  g.dealership_id = '".$_SESSION['dealership_id']."' and g.status_id = 9 and 
	                     from_beback ='".$request['from_beback']."' 
	                      order by keep_second_order asc ";
					//echo $query;
					$this->BDController->setQuery($query);
					$result = $this->BDController->ejecutaInstruccion();
					$num = $this->BDController->numero_filas($result);
					if($num == 0){
						$saveQuery = "update salesboard set keep_second_order=0,from_beback=0 
						 where from_beback = '".$request['from_beback']."' and dealership_id = '".$_SESSION['dealership_id']."'";
						 //echo $saveQuery;
						$this->BDController->setQuery($saveQuery);
						$this->BDController->ejecutaInstruccion();						
					}
				}
			}


			$action = "Saleman ".$request['email']." ".$actionInBoard;
			$saveQuery = "insert salesboard_log (saleman_id,dealership_id,action,status_id) values ('".$request['saleman_id']."',
				'".$_SESSION['dealership_id']."','".$action."','".$request['new_status_id']."')";

			$this->BDController->setQuery($saveQuery);
			$this->BDController->ejecutaInstruccion();


			$this->BDController->commit();
			
			
			return  Array('success' => true,"message" => "Success");
		} 
		catch(Exception $e ) {
			$this->BDController->rollback();
			$this->BDController->desconectar();
			$arrayError = Array('Error No:' => $e->getCode(),'Error Message:' => $e->getMessage(),'Stack Trace:' => nl2br($e->getTraceAsString()));
			//$arrayError = 'Error No: ' . $e->getCode().' Error Message: ' . $e->getMessage().' Stack Trace: ' . nl2br($e->getTraceAsString());
			return  Array('success' => true,'message' => $e->getMessage(),'error' => $arrayError);
		}
		
	}
	
	public function changeSalemanStatus($request){
		$this->BDController = new BDController();
		try{
			$this->BDController->conectar();
			$this->BDController->autocommit(FALSE);
			$saveQuery = '';
			$requestOriginal = $request;
			
			$actionInBoard = "";

			//Get Status Info and Conf for the new status
			
			$query = "SELECT s.*,d.preserve_position,d.allow 
                    FROM status  s 
                    inner join status_dealership d on d.status_id = s.status_id and d.dealership_id = '".$_SESSION['dealership_id']."' 	
                    WHERE  s.status_id = '".$request['new_status_id']."'";
			
			$this->BDController->setQuery($query);
			$result = $this->BDController->ejecutaInstruccion();
			$item = $this->BDController->fetch($result);


			$NewStatusConf = Array();
			$NewStatusConf['status_id'] = $item['status_id'];
			$NewStatusConf['allow'] = $item['allow'];
			$NewStatusConf['preserve_position'] = $item['preserve_position'];

			if(isset($request['action']) && $request['action'] == 'back'){
				$query = "SELECT s.*,d.preserve_position,d.allow 
                    FROM status  s 
                    inner join status_dealership d on d.status_id = s.status_id and d.dealership_id = '".$_SESSION['dealership_id']."' 	
                    WHERE  s.status_id = '".$request['status_id']."'";
				
				$this->BDController->setQuery($query);
				$result = $this->BDController->ejecutaInstruccion();
				$item = $this->BDController->fetch($result);


				$BackFromStatusConf = Array();
				$BackFromStatusConf['status_id'] = $item['status_id'];
				$BackFromStatusConf['allow'] = $item['allow'];
				$BackFromStatusConf['preserve_position'] = $item['preserve_position'];
			}



			//This is when select a sales person from the droplist
			if($request['new_status_id'] == '1' && !isset($request['action'])){
				$actionInBoard = "In";
				$query = "SELECT max(g.board_order) board_order
                    FROM salesboard  g 
                    WHERE  g.dealership_id = '".$_SESSION['dealership_id']."' and g.status_id = 1";
			
				$this->BDController->setQuery($query);
				$result = $this->BDController->ejecutaInstruccion();
				$item = $this->BDController->fetch($result);

				$item['board_order'] = ($item['board_order']=='')?0:$item['board_order'];

				$saveQuery = "insert salesboard (board_order,saleman_id,dealership_id,status_id,control_date) values ('".($item['board_order']+1)."','".$request['saleman_id']."',
				'".$_SESSION['dealership_id']."',1,concat(curdate(),' ',curtime()))";

				$this->BDController->setQuery($saveQuery);
				$this->BDController->ejecutaInstruccion();
			}
			elseif($request['new_status_id'] == '7'){//This is for remove sales person from the first list
				$actionInBoard = "Out";
				$saveQuery = "delete from salesboard where saleman_id = '".$request['saleman_id']."' and dealership_id = '".$_SESSION['dealership_id']."'";
				$this->BDController->setQuery($saveQuery);
				$this->BDController->ejecutaInstruccion();

				$saveQuery = "update salesboard set board_order = (board_order-1) where dealership_id = '".$_SESSION['dealership_id']."' and board_order >= '".$request['board_order']."'";
				$this->BDController->setQuery($saveQuery);
				$this->BDController->ejecutaInstruccion();
			}
			elseif($request['new_status_id'] == '6'){//This is for change order of a sales person in the droplist
				$actionInBoard = "Change Order";
				
				if($request['new_board_order'] < $request['board_order']){
					$saveQuery = "update salesboard set board_order = (board_order+1) where dealership_id = '".$_SESSION['dealership_id']."' and board_order >= '".$request['new_board_order']."' 
				                       and board_order <= '".$request['board_order']."'";	
				}
				elseif($request['new_board_order'] > $request['board_order']){
					$saveQuery = "update salesboard set board_order = (board_order-1) where dealership_id = '".$_SESSION['dealership_id']."' and board_order > '".$request['board_order']."' 
				                       and board_order <= '".$request['new_board_order']."'";	
				}
				$this->BDController->setQuery($saveQuery);
				$this->BDController->ejecutaInstruccion();
				

				$saveQuery = "update salesboard set board_order = '".$request['new_board_order']."' where saleman_id = '".$request['saleman_id']."' and dealership_id = '".$_SESSION['dealership_id']."'";
				$this->BDController->setQuery($saveQuery);
				$this->BDController->ejecutaInstruccion();
				


			}
			else{ //The actual status change

				$actionInBoard = "Change Status";
				$datetoshow = "";
				if(isset($request['new_datetoshow'])){
					$datetoshow = ", datetoshow='".$request['new_datetoshow']."'";
				}
				$board_order = "";



				//Look if there is any Preserve-Position to bring them down when someone goes Selling
				if($request['new_status_id'] != 1 && $request['new_status_id'] != 6 && $request['new_status_id'] != 7 && 
					//------$request['new_status_id'] != 10 && $request['new_status_id'] != 9){
					$request['new_status_id'] != 10 && $NewStatusConf['preserve_position'] == 0){
					
					$query = "SELECT coalesce(min(keep_order),0) min,coalesce(max(keep_order),0)max FROM salesboard WHERE dealership_id = '".$_SESSION['dealership_id']."' and isnull(keep_order)!=1";
					$this->BDController->setQuery($query);
					$result = $this->BDController->ejecutaInstruccion();
					$item = $this->BDController->fetch($result);

					if($request['board_order'] < $item['min']){

						$saveQuery = "update salesboard s 
						 inner join dealership d on d.dealership_id = s.dealership_id 
						  set s.keep_order = (s.keep_order-1),s.from_beback = (s.from_beback-1),s.keep_second_order=(s.keep_second_order-1)  
								 where isnull(s.keep_order) != 1 and s.keep_order > d.beback_min and s.dealership_id = '".$_SESSION['dealership_id']."' ";
						$this->BDController->setQuery($saveQuery);
						$this->BDController->ejecutaInstruccion();

						$saveQuery = "update salesboard s 
						 inner join dealership d on d.dealership_id = s.dealership_id 
						  set s.from_beback = (s.from_beback-1),s.keep_second_order=(s.keep_second_order-1) 
								 where isnull(s.keep_order) = 1 and s.keep_second_order > d.beback_min and s.dealership_id = '".$_SESSION['dealership_id']."' ";
						$this->BDController->setQuery($saveQuery);
						$this->BDController->ejecutaInstruccion();
					}
				}

				//**C****************O****************P*************Y**

				//Look if there is any Preserve-Position to bring them up when someone goes HOT
				if(isset($request['action2']) && $request['action2'] == 'hot' && $request['new_status_id'] == 1){
					
					$query = "SELECT coalesce(min(keep_order),0) min,coalesce(max(keep_order),0)max FROM salesboard WHERE dealership_id = '".$_SESSION['dealership_id']."' and isnull(keep_order)!=1";
					$this->BDController->setQuery($query);
					$result = $this->BDController->ejecutaInstruccion();
					$item = $this->BDController->fetch($result);

					//if($request['board_order'] < $item['min']){

						$saveQuery = "update salesboard s 
						 inner join dealership d on d.dealership_id = s.dealership_id 
						  set s.keep_order = (s.keep_order+1),s.from_beback = (s.from_beback+1),s.keep_second_order=(s.keep_second_order+1)  
								 where isnull(s.keep_order) != 1 and s.dealership_id = '".$_SESSION['dealership_id']."' ";
						$this->BDController->setQuery($saveQuery);// and s.keep_order > d.beback_min *** and s.keep_order < s.beback_initial_pos
						$this->BDController->ejecutaInstruccion();

						$saveQuery = "update salesboard s 
						 inner join dealership d on d.dealership_id = s.dealership_id 
						  set s.from_beback = (s.from_beback+1),s.keep_second_order=(s.keep_second_order+1) 
								 where isnull(s.keep_order) = 1 and s.dealership_id = '".$_SESSION['dealership_id']."' ";
						$this->BDController->setQuery($saveQuery);// and s.keep_second_order > d.beback_min
						//$this->BDController->ejecutaInstruccion();
					//}
				}

				//**P*******A************S*************T***********E***
				
				$beback = "";
				$keep_second_order = "";
				$control_date = "";


				if($request['new_status_id'] == 10){ //Day OFF
					$request['new_datetoshow'] = "";
					//$request['board_order'] = -1;
					$board_order = -1;

					$saveQuery = "update salesboard set board_order = (board_order-1) where dealership_id = '".$_SESSION['dealership_id']."' and board_order >= '".$request['board_order']."'  and status_id=1";
					$this->BDController->setQuery($saveQuery);
					$this->BDController->ejecutaInstruccion();
				}
				elseif($request['action'] == 'next'){// Send to board as Selling status
					$board_order = 0;
					$saveQuery = "update salesboard set board_order = (board_order-1)
					 where dealership_id = '".$_SESSION['dealership_id']."' and board_order > 0 and status_id=1";
					 //echo $saveQuery;
					$this->BDController->setQuery($saveQuery);
					$this->BDController->ejecutaInstruccion();

					//$this->notifyPositions($request);
				}
				//-------elseif($request['new_status_id'] == 9){//Change to Beback
				elseif($NewStatusConf['preserve_position'] == 1){//Change to a preserve_position Status
					//begin second order set 
					$query = "SELECT coalesce(max(keep_second_order),-1) keep_second_order FROM salesboard 
					  WHERE dealership_id = '".$_SESSION['dealership_id']."' and isnull(keep_order)!=1 and keep_order='".$request['board_order']."'";
					  //-------WHERE dealership_id=1 and isnull(keep_order)!=1 and keep_order='".$request['board_order']."'";
					$this->BDController->setQuery($query);
					$result = $this->BDController->ejecutaInstruccion();
					$item = $this->BDController->fetch($result);

					
					if($item['keep_second_order'] == -1){
						$keep_second_order = ", keep_second_order = '".$request['board_order']."'";//
					}
					else{
						$keep_second_order = ", keep_second_order = '".($item['keep_second_order']+1)."' ";	
					}
					//End second order set
					//$beback = ",keep_order='".$request['board_order']."',from_beback='".$request['board_order']."'";
					$beback = ",keep_order='".$request['board_order']."',from_beback='".$request['board_order']."',beback_initial_pos='".$request['board_order']."' ";
					//board_order >=
					$saveQuery = "update salesboard set board_order = (board_order-1) where dealership_id = '".$_SESSION['dealership_id']."' and board_order > '".$request['board_order']."'  and status_id=1";
					$this->BDController->setQuery($saveQuery);
					$this->BDController->ejecutaInstruccion();
				}
				//-------elseif(($request['action'] == 'back' && $request['status_id'] != 9)){
				elseif(($request['action'] == 'back' && $BackFromStatusConf['preserve_position'] == 0)){//Back from a non-preserve-position Status 
					// ||
					//($request['action'] == 'back' && $request['status_id'] != 5 && $request['order_showed'] < 3)
					$board_order = 0;
					$beback = ",keep_order=NULL";
					$control_date = ", control_date = concat(curdate(),' ',curtime())";
					$query = "SELECT max(g.board_order) board_order
	                    FROM salesboard  g 
	                    WHERE  g.dealership_id = '".$_SESSION['dealership_id']."' and g.status_id = 1";
				
					$this->BDController->setQuery($query);
					$result = $this->BDController->ejecutaInstruccion();
					$item = $this->BDController->fetch($result);
					$board_order = ($item['board_order']=='')?1:($item['board_order']+1);




				}
				//--------elseif($request['action'] == 'back' && $request['status_id'] == 9){// && $request['order_showed'] >= 3
				elseif($request['action'] == 'back' && $BackFromStatusConf['preserve_position'] == 1){////Back from a preserve-position Status 
					$query = "SELECT board_order,keep_order,keep_second_order,from_beback 
	                    FROM salesboard  g 
	                    WHERE  g.dealership_id = '".$_SESSION['dealership_id']."' and g.status_id = 1 and 
	                     from_beback ='".$request['order_showed']."' 
	                      order by keep_second_order asc ";
					//echo $query;
					$this->BDController->setQuery($query);
					$result = $this->BDController->ejecutaInstruccion();
					$position = $request['order_showed'];
					while ($itemSecOrder = $this->BDController->fetch($result)){
						//echo $request['keep_second_order']." < ".$itemSecOrder['keep_second_order']; 
						if($request['keep_second_order'] < $itemSecOrder['keep_second_order']){
							$request['order_showed'] = $position;//$request['order_showed'];
							break;
						}
						else{//if($request['keep_second_order'] < $itemSecOrder['keep_second_order'])
							//$request['order_showed'] = $request['order_showed'] + 1;
							$position++;
							$request['order_showed'] = $position;
							//break;	
						}
					}
					
					//echo $board_order;

					//end cheking order for second order
					$board_order = $request['order_showed'];
					//$beback = ",keep_order=NULL";
					$beback = ",keep_order=NULL,beback_initial_pos=0";
					//$keep_second_order = ", keep_second_order = 0";

					$query = "SELECT max(g.board_order) board_order
	                    FROM salesboard  g 
	                    WHERE  g.dealership_id = '".$_SESSION['dealership_id']."' and g.status_id = 1";
				
					$this->BDController->setQuery($query);
					$result = $this->BDController->ejecutaInstruccion();
					$itemOrder = $this->BDController->fetch($result);

					if($itemOrder['board_order'] != '' && ($request['order_showed'] <= $itemOrder['board_order'] && $request['order_showed'] >= 1) ){
						$saveQuery = "update salesboard set board_order = (board_order+1) where dealership_id = '".$_SESSION['dealership_id']."' and board_order >= '".$request['order_showed']."'  and status_id=1";
						$this->BDController->setQuery($saveQuery);
						$this->BDController->ejecutaInstruccion();
					}
					else{
						$board_order = ($itemOrder['board_order']=='')?1:($itemOrder['board_order']+1);
					}
				}
				else{
					///$board_order=0
					$saveQuery = "update salesboard set board_order = (board_order-1)
					 where dealership_id = '".$_SESSION['dealership_id']."' and board_order > '".$request['board_order']."' and status_id=1";
					 //echo $saveQuery;..
					$this->BDController->setQuery($saveQuery);
					$this->BDController->ejecutaInstruccion();
				}
				//@TODO
				// RESOLVER ORDEN CUANDO HAY VARISO BEBACKS. MANTENER EL ORDEN EN EL QUE LLEGAN A LA LISTA DE ACCION
				//PODRIA SER ORDENANDO POR LA FECHA DE DATETOSHOW

				//+1

				

				$saveQuery = "update salesboard set status_id = '".$request['new_status_id']."' ".$datetoshow.",board_order='".$board_order."' ".$beback." ".$keep_second_order."  ".$control_date." 
				 where saleman_id = '".$request['saleman_id']."' and dealership_id = '".$_SESSION['dealership_id']."'";
				 //echo $saveQuery;
				$this->BDController->setQuery($saveQuery);
				$this->BDController->ejecutaInstruccion();

				//After move back a BeBack
				//Check if there is no more beback quoe to reset keep_second_order and from_beback fields
				//------if($request['action'] == 'back' && $request['status_id'] == 9){
				if($request['action'] == 'back' && $BackFromStatusConf['preserve_position'] == 1){
					/*-------$query = "SELECT board_order,keep_order,keep_second_order,from_beback 
	                  ----  FROM salesboard  g 
	                  ----  WHERE  g.dealership_id = '".$_SESSION['dealership_id']."' and g.status_id = 9 and 
	                  ----   from_beback ='".$request['from_beback']."' 
	                  ----    order by keep_second_order asc ";*/
	                $query = "SELECT board_order,keep_order,keep_second_order,from_beback 
	                    FROM salesboard  g 
	                    WHERE  g.dealership_id = '".$_SESSION['dealership_id']."' 
	                           and g.status_id in (select status_id from status_dealership where dealership_id = '".$_SESSION['dealership_id']."' and preserve_position = 1)  and 
	                     from_beback ='".$request['from_beback']."' 
	                      order by keep_second_order asc ";
					//echo $query;
					$this->BDController->setQuery($query);
					$result = $this->BDController->ejecutaInstruccion();
					$num = $this->BDController->numero_filas($result);
					if($num == 0){
						$saveQuery = "update salesboard set keep_second_order=0,from_beback=0 
						 where from_beback = '".$request['from_beback']."' and dealership_id = '".$_SESSION['dealership_id']."'";
						 //echo $saveQuery;
						$this->BDController->setQuery($saveQuery);
						$this->BDController->ejecutaInstruccion();						
					}
				}
			}


			$action = "Saleman ".$request['email']." ".$actionInBoard;
			$saveQuery = "insert salesboard_log (saleman_id,dealership_id,action,status_id) values ('".$request['saleman_id']."',
				'".$_SESSION['dealership_id']."','".$action."','".$request['new_status_id']."')";

			$this->BDController->setQuery($saveQuery);
			$this->BDController->ejecutaInstruccion();


			$this->BDController->commit();
			
			if(isset($request['action']) && $request['action'] == 'next'){// Send to board as Selling status
				$this->notifyPositions($request);
			}
			
			return  Array('success' => true,"message" => "Success");
		} 
		catch(Exception $e ) {
			$this->BDController->rollback();
			$this->BDController->desconectar();
			$arrayError = Array('Error No:' => $e->getCode(),'Error Message:' => $e->getMessage(),'Stack Trace:' => nl2br($e->getTraceAsString()));
			//$arrayError = 'Error No: ' . $e->getCode().' Error Message: ' . $e->getMessage().' Stack Trace: ' . nl2br($e->getTraceAsString());
			return  Array('success' => true,'message' => $e->getMessage(),'error' => $arrayError);
		}
		
	}

	public function reset($request){
		$this->BDController = new BDController();
		$Notifier = new Notifier();
		try{
			$this->BDController->conectar();
			$this->BDController->autocommit(FALSE);
			$saveQuery = '';
			$requestOriginal = $request;
			
			/*if(isset($_SESSION['dealership_id'])){
				$andDealership = " and dealership_id =1";// '".$_SESSION['dealership_id']."'";
			}*/

			$saveQuery = "delete from salesboard where dealership_id = '".$_SESSION['dealership_id']."'";// '".$_SESSION['dealership_id']."'";
				 //echo $saveQuery;
			$this->BDController->setQuery($saveQuery);
			$this->BDController->ejecutaInstruccion();
			

			$this->BDController->commit();
			
			return  Array('success' => true,"message" => "Success");
		} 
		catch(Exception $e ) {
			$this->BDController->rollback();
			$this->BDController->desconectar();
			$arrayError = Array('Error No:' => $e->getCode(),'Error Message:' => $e->getMessage(),'Stack Trace:' => nl2br($e->getTraceAsString()));
			//$arrayError = 'Error No: ' . $e->getCode().' Error Message: ' . $e->getMessage().' Stack Trace: ' . nl2br($e->getTraceAsString());
			return  Array('success' => true,'message' => $e->getMessage(),'error' => $arrayError);
		}
		
	}

	public function resetAll($request){
		$this->BDController = new BDController();
		$BDController2 = new BDController();
		try{
			$this->BDController->conectar();
			$this->BDController->autocommit(FALSE);
			$saveQuery = '';
			$requestOriginal = $request;
			
			$query = "SELECT * 
                    FROM dealership";
			
			$this->BDController->setQuery($query);
			$result = $this->BDController->ejecutaInstruccion();
			while ($item = $this->BDController->fetch($result)){
				
				try{
					date_default_timezone_set($item['time_zone']);
				    $now = new DateTime();
				    $mins = $now->getOffset() / 60;
				    $sgn = ($mins < 0 ? -1 : 1);
				    $mins = abs($mins);
				    $hrs = floor($mins / 60);
				    $mins -= $hrs * 60;
				    $offset = sprintf('%+d:%02d', $hrs*$sgn, $mins);
				    $_SESSION['MYSQL_TIMEZONE'] = $offset;

				    $BDController2->conectar();
					$BDController2->autocommit(FALSE);

					$query2 = "delete from salesboard where dealership_id = 1  ";//'".$item['dealership_id']."' and (curtime() between '04:00:00' and '04:59:59')
					$BDController2->setQuery($query2);
					$result = $BDController2->ejecutaInstruccion();


					$BDController2->desconectar();
				}	
				catch(Exception $e ) {
					$BDController2->rollback();
					$BDController2->desconectar();
					$arrayError = Array('Error No:' => $e->getCode(),'Error Message:' => $e->getMessage(),'Stack Trace:' => nl2br($e->getTraceAsString()));
					//$arrayError = 'Error No: ' . $e->getCode().' Error Message: ' . $e->getMessage().' Stack Trace: ' . nl2br($e->getTraceAsString());
					return  Array('success' => true,'message' => $e->getMessage(),'error' => $arrayError);
				}		
			}

			$saveQuery = " ".$andDealership;
				 //echo $saveQuery;
			$this->BDController->setQuery($saveQuery);
			$this->BDController->ejecutaInstruccion();
			

			$this->BDController->commit();
			
			return  Array('success' => true,"message" => "Success");
		} 
		catch(Exception $e ) {
			$this->BDController->rollback();
			$this->BDController->desconectar();
			$arrayError = Array('Error No:' => $e->getCode(),'Error Message:' => $e->getMessage(),'Stack Trace:' => nl2br($e->getTraceAsString()));
			//$arrayError = 'Error No: ' . $e->getCode().' Error Message: ' . $e->getMessage().' Stack Trace: ' . nl2br($e->getTraceAsString());
			return  Array('success' => true,'message' => $e->getMessage(),'error' => $arrayError);
		}
		
	}

	public function notify($request){
		$this->BDController = new BDController();
		$Notifier = new Notifier();
		try{
			$this->BDController->conectar();
			$saveQuery = '';
			$requestOriginal = $request;
			


			$query = "SELECT g.*,d.notification_message  
                    FROM saleman  g 
                    inner join dealership d on d.dealership_id = '".$_SESSION['dealership_id']."' 
                    WHERE  g.saleman_id = '".$request['saleman_id']."'";
			
			$this->BDController->setQuery($query);
			$result = $this->BDController->ejecutaInstruccion();
			$item = $this->BDController->fetch($result);

			if($request['notification_type'] == "MAIL"){
				$resultMessage = $Notifier->sendMail("HelpDesk",$item['email'],"Up - Board",$item['notification_message']);
			}
			if($request['notification_type'] == "SMS"){
				$resultMessage = $Notifier->sendSMS("HelpDesk",$item['phone'],"Up - Board",$item['notification_message'],$item['notification_carrier_id'],$this->BDController);
			}
			
			return  Array('success' => true,"message" => "Success",'resultMessage' => $resultMessage);
		} 
		catch(Exception $e ) {
			$this->BDController->rollback();
			$this->BDController->desconectar();
			$arrayError = Array('Error No:' => $e->getCode(),'Error Message:' => $e->getMessage(),'Stack Trace:' => nl2br($e->getTraceAsString()));
			//$arrayError = 'Error No: ' . $e->getCode().' Error Message: ' . $e->getMessage().' Stack Trace: ' . nl2br($e->getTraceAsString());
			return  Array('success' => true,'message' => $e->getMessage(),'error' => $arrayError);
		}
		
	}

	public function notifyPositions($request){ //TOP 3 positions
		$this->BDController = new BDController();
		
		try{
			$this->BDController->conectar();
			$saveQuery = '';
			$requestOriginal = $request;
			


			$query = "SELECT g.*,concat('You are now in position: ',s.board_order) notification_message    
                    FROM salesboard s 
                    inner join saleman  g on g.saleman_id = s.saleman_id 
                    inner join dealership d on s.dealership_id = d.dealership_id 
                    WHERE  d.dealership_id = '".$_SESSION['dealership_id']."' and s.board_order <= 3 and s.board_order >=  1 
                    order by board_order asc";
			
			$this->BDController->setQuery($query);
			$result = $this->BDController->ejecutaInstruccion();
			//$item = $this->BDController->fetch($result);
			/*$item1 = $this->BDController->fetch($result);
			$item2 = $this->BDController->fetch($result);
			$item3 = $this->BDController->fetch($result);

			$resultMessage = $Notifier->sendSMS("HelpDesk",$item1['phone'],"Up - Board","You are now in position:  ".$item1['board_order'].". Be ready.",$item1['notification_carrier_id'],$this->BDController);
			$resultMessage = $Notifier->sendSMS("HelpDesk",$item2['phone'],"Up - Board","You are now in position:  ".$item2['board_order'].". Be ready.",$item2['notification_carrier_id'],$this->BDController);
			$resultMessage = $Notifier->sendSMS("HelpDesk",$item3['phone'],"Up - Board","You are now in position:  ".$item3['board_order'].". Be ready.",$item3['notification_carrier_id'],$this->BDController);*/
			while ($item = $this->BDController->fetch($result)){
				$Notifier = new Notifier();
				$resultMessage = $Notifier->sendSMS("HelpDesk",$item['phone'],"Up - Board","You are now in position:  ".$item['board_order'].". Be ready.",$item['notification_carrier_id'],$this->BDController);
				//echo $item['board_order'];
				
				//$resultMessage = $Notifier->sendSMS("HelpDesk","7869302433","Up - Board",$item['notification_message'],"3",$this->BDController);
				var_dump($Notifier);
			}
			
			return  Array('success' => true,"message" => "Success",'resultMessage' => $resultMessage);
		} 
		catch(Exception $e ) {
			$this->BDController->rollback();
			$this->BDController->desconectar();
			$arrayError = Array('Error No:' => $e->getCode(),'Error Message:' => $e->getMessage(),'Stack Trace:' => nl2br($e->getTraceAsString()));
			//$arrayError = 'Error No: ' . $e->getCode().' Error Message: ' . $e->getMessage().' Stack Trace: ' . nl2br($e->getTraceAsString());
			return  Array('success' => true,'message' => $e->getMessage(),'error' => $arrayError);
		}
		
	}
}

?>
