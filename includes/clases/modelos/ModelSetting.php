<?php

if(!isset($_SESSION)){session_start();} 
require_once $MAIN_DIR.'includes/clases/BDController.php';
//if(!isset($_SESSION)){session_start();} 
class ModelSetting {
	
	private $bd = null;
	
	public function getGeneralSetting($request){
		$this->BDController = new BDController();
		try{
			$this->BDController->conectar();
			
			$query = '';
			$andDealership = "";
			if(isset($_SESSION['dealership_id']) && $_SESSION['dealership_id'] != ''){
				$andDealership = " and g.dealership_id = '".$_SESSION['dealership_id']."'";
			}

			$query = "SELECT * 
				FROM dealership g 
				 WHERE 1  ".$andDealership;
			$this->BDController->setQuery($query);
			$result = $this->BDController->ejecutaInstruccion();
			$matches = Array();
			while ($item = $this->BDController->fetch($result)){
				$matches[] = $item;
			}

			/*if(isset($_SESSION['user_id'])){
				$query = "SELECT coalesce(p.create_new,0) create_new,coalesce(p.modify,0) modify,coalesce(p.remove,0) remove 
	                    FROM user u    
	                    left join profile p on p.profile_id = u.profile_id 
						WHERE  u.user_id = '".$_SESSION['user_id']."'";
				$this->BDController->setQuery($query);
				$result = $this->BDController->ejecutaInstruccion();
				$num = $this->BDController->numero_filas($result);
				$item = $this->BDController->fetch($result);//'create_new' => $item['create_new'],'modify' => $item['modify'],'remove' => $item['remove']
			}*/

			$this->BDController->desconectar();;
			return  Array('success' => true,'message' => 'Success','matches' => $matches);//
		} 
		catch(Exception $e ) {
			$this->BDController->rollback();
			$this->BDController->desconectar();
			$arrayError = Array('Error No:' => $e->getCode(),'Error Message:' => $e->getMessage(),'Stack Trace:' => nl2br($e->getTraceAsString()));
			//$arrayError = 'Error No: ' . $e->getCode().' Error Message: ' . $e->getMessage().' Stack Trace: ' . nl2br($e->getTraceAsString());
			return  Array('success' => false,'message' => "Error during the operation",'error' => $arrayError);
		}
		
	}

	public function getDealerList($request){
		$this->BDController = new BDController();
		try{
			$this->BDController->conectar();
			
			$query = '';
			$andDealership = "";
			/*
			$query = "SELECT g.*  
				FROM dealership g 
				inner join user_dealership_access a on a.dealership_id = g.dealership_id 
				inner join user u on u.user_id = a.user_id 
				 WHERE a.user_id = '".$request['user_id']."' and a.allow = 1 
				 order by u.dealership_id,a.dealership_id ";*/
			$query = "SELECT 0 user_dealership_access_id,g.*,0 allow   
				FROM dealership g 
				 order by company_name ";
			if(isset($request['user_id'])){
				$query = "SELECT a.user_dealership_access_id,g.* ,0 allow   
				FROM dealership g 
				inner join user_dealership_access a on a.dealership_id = g.dealership_id 
				inner join user u on u.user_id = a.user_id 
				 WHERE a.user_id = '".$request['user_id']."' and a.allow = 1 
				 order by u.dealership_id,a.dealership_id ";
			}
			$this->BDController->setQuery($query);
			$result = $this->BDController->ejecutaInstruccion();
			$num = $this->BDController->numero_filas($result);
			$matches = Array();
			while ($item = $this->BDController->fetch($result)){
				$matches[] = $item;
			}

			$this->BDController->desconectar();;
			return  Array('success' => true,'message' => 'Success','matches' => $matches,'totalCount' => $num);//
		} 
		catch(Exception $e ) {
			$this->BDController->rollback();
			$this->BDController->desconectar();
			$arrayError = Array('Error No:' => $e->getCode(),'Error Message:' => $e->getMessage(),'Stack Trace:' => nl2br($e->getTraceAsString()));
			//$arrayError = 'Error No: ' . $e->getCode().' Error Message: ' . $e->getMessage().' Stack Trace: ' . nl2br($e->getTraceAsString());
			return  Array('success' => false,'message' => "Error during the operation",'error' => $arrayError);
		}
		
	}

	public function saveGeneralSetting($request){
		$this->BDController = new BDController();
		try{
			$this->BDController->conectar();
			$this->BDController->autocommit(FALSE);
			$saveQuery = '';
			$requestOriginal = $request;
			
			$saveQuery = "update dealership set company_name='".$request['company_name']."',
			address ='".$request['address']."',  
			time_zone='".$request['time_zone']."', 
			 short_name='".$request['short_name']."', 
			 logo='".$request['logo']."',      
			 beback_min='".$request['beback_min']."',
			 session_timeout='".$request['session_timeout']."',        
			 notification_message ='".$request['notification_message']."'         
			 where dealership_id = '".$_SESSION['dealership_id']."'";

				$this->BDController->setQuery($saveQuery);
				$this->BDController->ejecutaInstruccion();
			
				$this->BDController->commit();

			$_SESSION['session_timeout_limit'] = $request['session_timeout'];
			$_SESSION['session_timeout'] = time() + $request['session_timeout'];
			
			
			return  Array('success' => true,"message" => "Success");
		} 
		catch(Exception $e ) {
			$this->BDController->rollback();
			$this->BDController->desconectar();
			$arrayError = Array('Error No:' => $e->getCode(),'Error Message:' => $e->getMessage(),'Stack Trace:' => nl2br($e->getTraceAsString()));
			//$arrayError = 'Error No: ' . $e->getCode().' Error Message: ' . $e->getMessage().' Stack Trace: ' . nl2br($e->getTraceAsString());
			return  Array('success' => true,'message' => $e->getMessage(),'error' => $arrayError);
		}
		
	}

	public function getStatusList($request){
		$this->BDController = new BDController();
		try{
			$this->BDController->conectar();
			
			$query = '';
			$inicio = 0;
			$fin = 0;
			$num = 0;
			$limit = "";
			
			if (isset($request['orden']) && $request['orden'] != "") {
				$orden = $request['orden']." ".$request['dir'];
			}
			else{
				$orden = " name asc";
			}

			$andDealership = "";
			if(isset($_SESSION['dealership_id']) && $_SESSION['dealership_id'] != ''){
				$andDealership = " and d.dealership_id = '".$_SESSION['dealership_id']."'";
			}
			
			$tamanoPagina = 30;
			if (isset($request['pagina'])) {
				$inicio = ($request['pagina'] - 1 )  * $tamanoPagina;
				$query = "SELECT d.*     
                    FROM status d  
                    WHERE  1 ";
				
				$this->BDController->setQuery($query);
				$result = $this->BDController->ejecutaInstruccion();
				$num = $this->BDController->numero_filas($result);
				$limit = " limit ".$inicio.",".$tamanoPagina;
			}
			

			$query = "SELECT d.* ,de.allow,de.preserve_position  
                    FROM status d  
                    inner join status_dealership de on de.status_id = d.status_id and de.dealership_id = '".$_SESSION['dealership_id']."' 
                    WHERE  1  
                    ORDER BY ".$orden." ".$limit;
			
			$this->BDController->setQuery($query);
			$result = $this->BDController->ejecutaInstruccion();
			$matches = Array();
			while ($item = $this->BDController->fetch($result)){
				$matches[] = $item;
				//$matches2[] = $item;//array('nombre' => $item['nombre']);
			}

			$query = "SELECT coalesce(p.create_new,0) create_new,coalesce(p.modify,0) modify,coalesce(p.remove,0) remove 
                    FROM user u    
                    left join profile p on p.profile_id = u.profile_id 
					WHERE  u.user_id = '".$_SESSION['user_id']."'";
			$this->BDController->setQuery($query);
			$result = $this->BDController->ejecutaInstruccion();
			$num = $this->BDController->numero_filas($result);
			$item = $this->BDController->fetch($result);//'create_new' => $item['create_new'],'modify' => $item['modify'],'remove' => $item['remove']
			$this->BDController->desconectar();

			if(isset($request['isbox']))
				return  Array($matches);				

			return  Array('success' => true,'totalCount' => $num,'matches' => $matches,'inicio' => $inicio,'fin' => $fin,
				'create_new' => $item['create_new'],'modify' => $item['modify'],'remove' => $item['remove']);
		} 			
		catch(Exception $e ) {
			$this->BDController->rollback();
			$this->BDController->desconectar();
			$arrayError = Array('Error No:' => $e->getCode(),'Error Message:' => $e->getMessage(),'Stack Trace:' => nl2br($e->getTraceAsString()));
			//$arrayError = 'Error No: ' . $e->getCode().' Error Message: ' . $e->getMessage().' Stack Trace: ' . nl2br($e->getTraceAsString());
			return  Array('success' => false,'mensaje' => "Error durante la operacion",'error' => $arrayError);
		}
		
	}

	public function getStatus($request){
		$this->BDController = new BDController();
		try{
			$this->BDController->conectar();
			
			$query = '';
			$inicio = 0;
			$fin = 0;
			$num = 0;
			
			$query = "SELECT s.*,d.allow,d.preserve_position  
                    FROM status s 
                     inner join status_dealership d on d.status_id = s.status_id and d.dealership_id = '".$_SESSION['dealership_id']."' 
                    WHERE  s.status_id = '".$request['status_id']."'";
			
			$this->BDController->setQuery($query);
			$result = $this->BDController->ejecutaInstruccion();
			$matches = Array();
			while ($item = $this->BDController->fetch($result)){
				$matches[] = $item;
				//$matches2[] = $item;//array('nombre' => $item['nombre']);
			}

			$query = "SELECT coalesce(p.create_new,0) create_new,coalesce(p.modify,0) modify,coalesce(p.remove,0) remove 
                    FROM user u    
                    left join profile p on p.profile_id = u.profile_id 
					WHERE  u.user_id = '".$_SESSION['user_id']."'";
			$this->BDController->setQuery($query);
			$result = $this->BDController->ejecutaInstruccion();
			$num = $this->BDController->numero_filas($result);
			$item = $this->BDController->fetch($result);//'create_new' => $item['create_new'],'modify' => $item['modify'],'remove' => $item['remove']
			$this->BDController->desconectar();

			return  Array('success' => true,'totalCount' => $num,'matches' => $matches,'inicio' => $inicio,'fin' => $fin,
				'create_new' => $item['create_new'],'modify' => $item['modify'],'remove' => $item['remove']);
		} 			
		catch(Exception $e ) {
			$this->BDController->rollback();
			$this->BDController->desconectar();
			$arrayError = Array('Error No:' => $e->getCode(),'Error Message:' => $e->getMessage(),'Stack Trace:' => nl2br($e->getTraceAsString()));
			//$arrayError = 'Error No: ' . $e->getCode().' Error Message: ' . $e->getMessage().' Stack Trace: ' . nl2br($e->getTraceAsString());
			return  Array('success' => false,'mensaje' => "Error durante la operacion",'error' => $arrayError);
		}
		
	}

	public function saveStatus($request){
		$this->BDController = new BDController();
		try{
			$this->BDController->conectar();
			$this->BDController->autocommit(FALSE);
			$saveQuery = '';
			$requestOriginal = $request;
			
			if(!isset($request['status_id'])){
				$saveQuery = "INSERT INTO status (name,short_name,description) 
								VALUES ('".$request['name']."','".$request['short_name']."','".$request['description']."');";

				$this->BDController->setQuery($saveQuery);
				$this->BDController->ejecutaInstruccion();
				$lastId = $this->BDController->lastId();

				$saveQuery = "INSERT INTO status_dealership (status_id,dealership_id,allow,preserve_position) 
								VALUES ('".$lastId."','".$_SESSION['dealership_id']."',1,0);";

				$this->BDController->setQuery($saveQuery);
				$this->BDController->ejecutaInstruccion();
			}
			else{
				$saveQuery = "update status set description = '".$request['description']."', name = '".$request['name']."', short_name = '".$request['short_name']."'     
								where status_id = '".$request['status_id']."'";

				$this->BDController->setQuery($saveQuery);
				$this->BDController->ejecutaInstruccion();					
				$lastId = $request['status_id'];

				$saveQuery = "update status_dealership set allow = '".$request['allow']."',preserve_position = '".$request['preserve_position']."'      
								where status_id = '".$request['status_id']."' and dealership_id = '".$_SESSION['dealership_id']."' ";

				$this->BDController->setQuery($saveQuery);
				$this->BDController->ejecutaInstruccion();	
			}
				$this->BDController->commit();
			
			
			return  Array('success' => true,"message" => "Success","status_id" => $lastId);
		} 
		catch(Exception $e ) {
			$this->BDController->rollback();
			$this->BDController->desconectar();
			$arrayError = Array('Error No:' => $e->getCode(),'Error Message:' => $e->getMessage(),'Stack Trace:' => nl2br($e->getTraceAsString()));
			//$arrayError = 'Error No: ' . $e->getCode().' Error Message: ' . $e->getMessage().' Stack Trace: ' . nl2br($e->getTraceAsString());
			return  Array('success' => true,'message' => $e->getMessage(),'error' => $arrayError);
		}
		
	}

	public function getCarrierList($request){
		$this->BDController = new BDController();
		try{
			$this->BDController->conectar();
			
			$query = '';
			$andDealership = "";
			
			$query = "SELECT g.*  
				FROM notification_carrier g 
				 WHERE 1 
				 order by name ";
			$this->BDController->setQuery($query);
			$result = $this->BDController->ejecutaInstruccion();
			$num = $this->BDController->numero_filas($result);
			$matches = Array();
			while ($item = $this->BDController->fetch($result)){
				$matches[] = $item;
			}

			$this->BDController->desconectar();;
			return  Array('success' => true,'message' => 'Success','matches' => $matches,'totalCount' => $num);//
		} 
		catch(Exception $e ) {
			$this->BDController->rollback();
			$this->BDController->desconectar();
			$arrayError = Array('Error No:' => $e->getCode(),'Error Message:' => $e->getMessage(),'Stack Trace:' => nl2br($e->getTraceAsString()));
			//$arrayError = 'Error No: ' . $e->getCode().' Error Message: ' . $e->getMessage().' Stack Trace: ' . nl2br($e->getTraceAsString());
			return  Array('success' => false,'message' => "Error during the operation",'error' => $arrayError);
		}
		
	}

	public function changeDealership($request){
		$this->BDController = new BDController();
		try{

				$this->BDController->conectar();
				
				$_SESSION['dealership_id'] = $request['dealership_id'];
				
				$query = "SELECT g.*  
					FROM dealership g 
					 WHERE g.dealership_id = '".$request['dealership_id']."' ";
				$this->BDController->setQuery($query);
				$result = $this->BDController->ejecutaInstruccion();
				$item = $this->BDController->fetch($result);

				$_SESSION['session_timeout_limit'] = $item['session_timeout'];
				$_SESSION['PHP_TIMEZONE'] = $item['time_zone'];


			$this->BDController->desconectar();;
			return  Array('success' => true,'session_timeout_limit' => $_SESSION['session_timeout_limit']);
		} 			
		catch(Exception $e ) {
			return  Array('success' => false,'mensaje' => "Error during the process");
		}
		
	}

































	public function getCarTypeList($request){
		$this->BDController = new BDController();
		try{
			$this->BDController->conectar();
			
			$query = '';
			$inicio = 0;
			$fin = 0;
			$num = 0;
			
			if (isset($request['orden']) && $request['orden'] != "") {
				$orden = $request['orden']." ".$request['dir'];
			}
			else{
				$orden = " description asc";
			}

			$andDealership = "";
			if(isset($_SESSION['dealership_id']) && $_SESSION['dealership_id'] != ''){
				$andDealership = " and c.dealership_id = '".$_SESSION['dealership_id']."'";
			}			
			
			$tamanoPagina = 30;
			if (isset($request['pagina'])) {
				$inicio = ($request['pagina'] - 1 )  * $tamanoPagina;
				$query = "SELECT c.*     
                    FROM car_type c   
                    WHERE  1 ".$andDealership;
				
				$this->BDController->setQuery($query);
				$result = $this->BDController->ejecutaInstruccion();
				$num = $this->BDController->numero_filas($result);
				$limit = " limit ".$inicio.",".$tamanoPagina;
			}
			

			$query = "SELECT c.* 
                    FROM car_type c   
                    WHERE  1  ".$andDealership."
                    ORDER BY ".$orden." ".$limit;
			
			$this->BDController->setQuery($query);
			$result = $this->BDController->ejecutaInstruccion();
			$matches = Array();
			while ($item = $this->BDController->fetch($result)){
				$matches[] = $item;
				//$matches2[] = $item;//array('nombre' => $item['nombre']);
			}
			$this->BDController->desconectar();

			return  Array('success' => true,'totalCount' => $num,'matches' => $matches,'inicio' => $inicio,'fin' => $fin);
		} 			
		catch(Exception $e ) {
			$this->BDController->rollback();
			$this->BDController->desconectar();
			$arrayError = Array('Error No:' => $e->getCode(),'Error Message:' => $e->getMessage(),'Stack Trace:' => nl2br($e->getTraceAsString()));
			//$arrayError = 'Error No: ' . $e->getCode().' Error Message: ' . $e->getMessage().' Stack Trace: ' . nl2br($e->getTraceAsString());
			return  Array('success' => false,'mensaje' => "Error durante la operacion",'error' => $arrayError);
		}
		
	}

	public function getCarType($request){
		$this->BDController = new BDController();
		try{
			$this->BDController->conectar();
			
			$query = '';
			$inicio = 0;
			$fin = 0;
			$num = 0;
			
			$query = "SELECT * 
                    FROM car_type    
                    WHERE  car_type_id = '".$request['car_type_id']."'";
			
			$this->BDController->setQuery($query);
			$result = $this->BDController->ejecutaInstruccion();
			$matches = Array();
			while ($item = $this->BDController->fetch($result)){
				$matches[] = $item;
				//$matches2[] = $item;//array('nombre' => $item['nombre']);
			}
			$this->BDController->desconectar();

			return  Array('success' => true,'totalCount' => $num,'matches' => $matches,'inicio' => $inicio,'fin' => $fin);
		} 			
		catch(Exception $e ) {
			$this->BDController->rollback();
			$this->BDController->desconectar();
			$arrayError = Array('Error No:' => $e->getCode(),'Error Message:' => $e->getMessage(),'Stack Trace:' => nl2br($e->getTraceAsString()));
			//$arrayError = 'Error No: ' . $e->getCode().' Error Message: ' . $e->getMessage().' Stack Trace: ' . nl2br($e->getTraceAsString());
			return  Array('success' => false,'mensaje' => "Error durante la operacion",'error' => $arrayError);
		}
		
	}

	public function saveCarType($request){
		$this->BDController = new BDController();
		try{
			$this->BDController->conectar();
			$this->BDController->autocommit(FALSE);
			$saveQuery = '';
			$requestOriginal = $request;
			
			if(!isset($request['car_type_id'])){
				$saveQuery = "INSERT INTO car_type (dealership_id,description) 
								VALUES ('".$_SESSION['dealership_id']."','".$request['description']."');";

				$this->BDController->setQuery($saveQuery);
				$this->BDController->ejecutaInstruccion();
				$lastId = $this->BDController->lastId();
			}
			else{
				$saveQuery = "update car_type set description = '".$request['description']."'   
								where car_type_id = '".$request['car_type_id']."'";

				$this->BDController->setQuery($saveQuery);
				$this->BDController->ejecutaInstruccion();	
				$lastId = $request['car_type_id'];
			}
				$this->BDController->commit();
			
			
			return  Array('success' => true,"message" => "Success","car_type_id" => $lastId);
		} 
		catch(Exception $e ) {
			$this->BDController->rollback();
			$this->BDController->desconectar();
			$arrayError = Array('Error No:' => $e->getCode(),'Error Message:' => $e->getMessage(),'Stack Trace:' => nl2br($e->getTraceAsString()));
			//$arrayError = 'Error No: ' . $e->getCode().' Error Message: ' . $e->getMessage().' Stack Trace: ' . nl2br($e->getTraceAsString());
			return  Array('success' => true,'message' => $e->getMessage(),'error' => $arrayError);
		}
		
	}

	public function getMenuOptions($request){
		$this->BDController = new BDController();
		try{
			$this->BDController->conectar();
			
			$query = '';
			
			$query = "SELECT m.parent_menu_id,m.menu_id,p.menu_order, p.description parent_menu, m.description menu,
			 			'0' allow 
						FROM menu m
						INNER JOIN menu p ON p.menu_id = m.parent_menu_id
						WHERE 1 
						ORDER BY p.menu_order,m.menu_order ";
			
			$this->BDController->setQuery($query);
			$result = $this->BDController->ejecutaInstruccion();
			$matches = Array();
			while ($item = $this->BDController->fetch($result)){
				$matches[] = $item;
				//$matches2[] = $item;//array('nombre' => $item['nombre']);
			}
			$this->BDController->desconectar();

			return  Array('success' => true,'matches' => $matches);
		} 			
		catch(Exception $e ) {
			$this->BDController->rollback();
			$this->BDController->desconectar();
			$arrayError = Array('Error No:' => $e->getCode(),'Error Message:' => $e->getMessage(),'Stack Trace:' => nl2br($e->getTraceAsString()));
			//$arrayError = 'Error No: ' . $e->getCode().' Error Message: ' . $e->getMessage().' Stack Trace: ' . nl2br($e->getTraceAsString());
			return  Array('success' => false,'mensaje' => "Error durante la operacion",'error' => $arrayError);
		}
		
	}

	public function getDefaultProfiles($request){
		$this->BDController = new BDController();
		try{
			$this->BDController->conectar();
			
			$query = '';
			
			$query = "SELECT * 
						from profile_default
						WHERE 1 
						ORDER BY profile_id ";
			
			$this->BDController->setQuery($query);
			$result = $this->BDController->ejecutaInstruccion();
			$matchesProfiles = Array();
			while ($item = $this->BDController->fetch($result)){
				$matchesProfiles[] = $item;
				//$matches2[] = $item;//array('nombre' => $item['nombre']);
			}

			$query = "SELECT a.profile_menu_access_id,a.profile_id,a.allow, p.description parent_menu,m.description menu, 
						m.parent_menu_id,m.menu_id  
						from profile_menu_access_default a 
						inner join menu m on m.menu_id = a.menu_id 
						inner join menu p on p.menu_id = m.parent_menu_id 
						WHERE 1 
						ORDER BY a.profile_id,p.menu_order,m.menu_order  ";
			
			$this->BDController->setQuery($query);
			$result = $this->BDController->ejecutaInstruccion();
			$matchesProfilesDetails = Array();
			while ($item = $this->BDController->fetch($result)){
				$matchesProfilesDetails[] = $item;
				//$matches2[] = $item;//array('nombre' => $item['nombre']);
			}

			$this->BDController->desconectar();

			return  Array('success' => true,'matchesProfiles' => $matchesProfiles,'matchesProfilesDetails' => $matchesProfilesDetails);
		} 			
		catch(Exception $e ) {
			$this->BDController->rollback();
			$this->BDController->desconectar();
			$arrayError = Array('Error No:' => $e->getCode(),'Error Message:' => $e->getMessage(),'Stack Trace:' => nl2br($e->getTraceAsString()));
			//$arrayError = 'Error No: ' . $e->getCode().' Error Message: ' . $e->getMessage().' Stack Trace: ' . nl2br($e->getTraceAsString());
			return  Array('success' => false,'mensaje' => "Error durante la operacion",'error' => $arrayError);
		}
		
	}

	public function getMenu($request){
		$this->BDController = new BDController();
		try{
			$this->BDController->conectar();
			
			$query = '';
			
			$query = "SELECT m.parent_menu_id,m.menu_id,p.menu_order, p.description parent_menu, m.description menu,
						m.target ,m.icon,p.icon parent_icon ,m.hide_menu 
						FROM menu m
						INNER JOIN menu p ON p.menu_id = m.parent_menu_id 
						inner join user_menu_access a on a.user_id = a.user_id and a.menu_id = m.menu_id 
						WHERE a.user_id = '".$_SESSION['user_id']."' and a.allow = 1 and  m.active = 1 
						ORDER BY p.menu_order,m.menu_order ";
			
			$this->BDController->setQuery($query);
			$result = $this->BDController->ejecutaInstruccion();
			$matches = Array();
			$i = 0;
			while ($item = $this->BDController->fetch($result)){
				$matches[] = $item;
				$i++;
				//$matches2[] = $item;//array('nombre' => $item['nombre']);
			}
			$this->BDController->desconectar();

			return  Array('success' => true,'totalCount' => $i,'matches' => $matches);
		} 			
		catch(Exception $e ) {
			$this->BDController->rollback();
			$this->BDController->desconectar();
			$arrayError = Array('Error No:' => $e->getCode(),'Error Message:' => $e->getMessage(),'Stack Trace:' => nl2br($e->getTraceAsString()));
			//$arrayError = 'Error No: ' . $e->getCode().' Error Message: ' . $e->getMessage().' Stack Trace: ' . nl2br($e->getTraceAsString());
			return  Array('success' => false,'mensaje' => "Error durante la operacion",'error' => $arrayError);
		}
		
	}

	public function generateLicense($request){
		$this->BDController = new BDController();
		try{
			$this->BDController->conectar();
			$this->BDController->autocommit(FALSE);
			$saveQuery = '';
			$requestOriginal = $request;
			
			$date = date_create();
			
			$keyGen = "DG".$request['group_name'].date_timestamp_get($date);

			$license = md5($keyGen);
			$saveQuery = "INSERT INTO dealership_group (dealership_group_name,dealership_group_license,create_date) 
								VALUES ('".$request['group_name']."','".$license."',curdate());";

			$this->BDController->setQuery($saveQuery);
			$this->BDController->ejecutaInstruccion();
			
			$this->BDController->commit();
			
			
			return  Array('success' => true,"message" => "Success","license" => $license);
		} 
		catch(Exception $e ) {
			$this->BDController->rollback();
			$this->BDController->desconectar();
			$arrayError = Array('Error No:' => $e->getCode(),'Error Message:' => $e->getMessage(),'Stack Trace:' => nl2br($e->getTraceAsString()));
			//$arrayError = 'Error No: ' . $e->getCode().' Error Message: ' . $e->getMessage().' Stack Trace: ' . nl2br($e->getTraceAsString());
			return  Array('success' => true,'message' => $e->getMessage(),'error' => $arrayError);
		}
		
	}

	public function validateLicense($request){
		$this->BDController = new BDController();
		try{
			$this->BDController->conectar();
			$this->BDController->autocommit(FALSE);
			
			$query = '';
			
			$query = "SELECT c.*,d.dealership_id,d.company_name,d.short_name,d.address,d.time_zone  
                    FROM dealership_group c 
                    left join dealership d on d.dealership_group_id = c.dealership_group_id   
                    WHERE  dealership_group_license = '".$request['license']."'";
			
			$this->BDController->setQuery($query);
			$result = $this->BDController->ejecutaInstruccion();
			$matches = Array();
			$i=0;
			while ($item = $this->BDController->fetch($result)){
				$matches[] = $item;
				$i++;
			}
			$j=0;
			$adminsOnGroup = Array();
			if($j != 0){
				$query = "SELECT  u.user_id,u.name,u.username,concat(u.name,' - ',u.username) description 
                    FROM dealership_group c 
                    inner join dealership d on d.dealership_group_id = c.dealership_group_id   
                    inner join user u on u.dealership_id = d.dealership_id 
                    WHERE  c.dealership_group_license = '".$request['license']."' and u.username like '%@admin'";
			
				$this->BDController->setQuery($query);
				$result = $this->BDController->ejecutaInstruccion();
				$j=0;
				while ($item = $this->BDController->fetch($result)){
					$adminsOnGroup[] = $item;
					$j++;
				}
			}
			$adminsOnGroup2 = Array();
				$query = "SELECT  u.user_id,u.name,u.username,concat(u.name,' - ',u.username) description 
                    FROM  user u 
                    WHERE  u.profile_id in (1,2)";
			
				$this->BDController->setQuery($query);
				$result = $this->BDController->ejecutaInstruccion();
				$j=0;
				while ($item = $this->BDController->fetch($result)){
					$adminsOnGroup2[] = $item;
					$j++;
				}
			
			

			$this->BDController->commit();
			$this->BDController->desconectar();

			return  Array('success' => true,"message" => "Success","totalCount" => $i,"dealershipGroup" => $matches,"adminsOnGroup" => $adminsOnGroup,"adminsOnGroup2" => $adminsOnGroup2);
		} 			
		catch(Exception $e ) {
			$this->BDController->rollback();
			$this->BDController->desconectar();
			$arrayError = Array('Error No:' => $e->getCode(),'Error Message:' => $e->getMessage(),'Stack Trace:' => nl2br($e->getTraceAsString()));
			//$arrayError = 'Error No: ' . $e->getCode().' Error Message: ' . $e->getMessage().' Stack Trace: ' . nl2br($e->getTraceAsString());
			return  Array('success' => false,'message' => "Error durante la operacion",'error' => $arrayError);
		}
		
	}

	public function getAdminsOnGroup($request){
		$this->BDController = new BDController();
		try{
			$this->BDController->conectar();
			$this->BDController->autocommit(FALSE);
			
			$query = '';
			
			$query = "SELECT c.*,d.dealership_id,d.company_name,d.short_name,d.address,d.time_zone  
                    FROM dealership_group c 
                    left join dealership d on d.dealership_group_id = c.dealership_group_id   
                    WHERE  dealership_group_license = '".$request['license']."'";
			
			$this->BDController->setQuery($query);
			$result = $this->BDController->ejecutaInstruccion();
			$matches = Array();
			$i=0;
			while ($item = $this->BDController->fetch($result)){
				$matches[] = $item;
				$i++;
			}

			$this->BDController->commit();
			$this->BDController->desconectar();

			return  Array('success' => true,"message" => "Success","totalCount" => $i,"dealershipGroup" => $matches);
		} 			
		catch(Exception $e ) {
			$this->BDController->rollback();
			$this->BDController->desconectar();
			$arrayError = Array('Error No:' => $e->getCode(),'Error Message:' => $e->getMessage(),'Stack Trace:' => nl2br($e->getTraceAsString()));
			//$arrayError = 'Error No: ' . $e->getCode().' Error Message: ' . $e->getMessage().' Stack Trace: ' . nl2br($e->getTraceAsString());
			return  Array('success' => false,'message' => "Error durante la operacion",'error' => $arrayError);
		}
	}

	public function installDealership($request){
		session_destroy();
		$this->BDController = new BDController();
		try{
			$this->BDController->conectar();
			$this->BDController->autocommit(FALSE);
			

			$query = "SELECT c.* 
                    FROM dealership_group c   
                    WHERE  dealership_group_license = '".$request['license']."'";
			
			$this->BDController->setQuery($query);
			$result = $this->BDController->ejecutaInstruccion();
			$item = $this->BDController->fetch($result);


			//INSERT GENERAL SETTINGS
			$saveQuery = "insert into dealership (dealership_group_id,company_name,address,time_zone,short_name) values ('".$item['dealership_group_id']."','".$request['company_name']."',
			 '".$request['address']."',  
			'".$request['time_zone']."', 
			 '".$request['short_name']."')";

			$this->BDController->setQuery($saveQuery);
			$this->BDController->ejecutaInstruccion();
			$DealershipId = $this->BDController->lastId();

			foreach ($request['profileDefaults'] as $key => $profileDefault) {
				$saveQuery = "INSERT INTO profile (dealership_id,description) 
								VALUES ('".$DealershipId."','".$profileDefault['description']."');";

				$this->BDController->setQuery($saveQuery);
				$this->BDController->ejecutaInstruccion();
				$profileID = $this->BDController->lastId();

				$parent_menu_id = -1;
				foreach ($request['profileAccess'] as $key => $profileAccesMenu) {
					if($profileDefault['profile_id'] == $profileAccesMenu['profile_id']){
						if($profileAccesMenu['parent_menu_id'] != $parent_menu_id){
							$parent_menu_id = $profileAccesMenu['parent_menu_id'];

							$saveQuery = "INSERT INTO profile_menu_access (profile_id,menu_id,allow) 
										VALUES ('".$profileID."','".$profileAccesMenu['parent_menu_id']."',1);";
							$this->BDController->setQuery($saveQuery);
							$this->BDController->ejecutaInstruccion();
						}
						$saveQuery = "INSERT INTO profile_menu_access (profile_id,menu_id,allow) 
										VALUES ('".$profileID."','".$profileAccesMenu['menu_id']."','".$profileAccesMenu['allow']."');";
						$this->BDController->setQuery($saveQuery);
						$this->BDController->ejecutaInstruccion();
					}
				}
			}

			$saveQuery = "INSERT INTO user_dealership_access (user_id,dealership_id,allow) 
										VALUES ('1','".$DealershipId."',1);";
			$this->BDController->setQuery($saveQuery);
			$this->BDController->ejecutaInstruccion();

			if($request['useAdminOnGroup'] == 0){
				$saveQuery = "INSERT INTO user (dealership_id,code,user_type_id,profile_id,name,username,password,email) 
										VALUES ('".$DealershipId."','',1,1,'Admin','".$request['user_prefix']."@admin','".md5($request['adminPassword'])."','".$request['adminEmail']."');";
				$this->BDController->setQuery($saveQuery);
				$this->BDController->ejecutaInstruccion();
				$adminUserID = $this->BDController->lastId();

				$saveQuery = "INSERT INTO user_dealership_access (user_id,dealership_id,allow) 
										VALUES ('".$adminUserID."','".$DealershipId."',1);";
				$this->BDController->setQuery($saveQuery);
				$this->BDController->ejecutaInstruccion();

				$saveQuery = "INSERT INTO user_menu_access (user_id,menu_id,allow) 
										 (select '".$adminUserID."',menu_id,allow from profile_menu_access_default where profile_id = 1);";
				$this->BDController->setQuery($saveQuery);
				$this->BDController->ejecutaInstruccion();
			}
			else{
				$saveQuery = "INSERT INTO user_dealership_access (user_id,dealership_id,allow) 
										VALUES ('".$request['useAdminOnGroup']."','".$DealershipId."',1);";
				$this->BDController->setQuery($saveQuery);
				$this->BDController->ejecutaInstruccion();
			}
			
			
			$this->BDController->commit();
			$this->BDController->desconectar();

			return  Array('success' => true,"message" => "Success");
		} 			
		catch(Exception $e ) {
			$this->BDController->rollback();
			$this->BDController->desconectar();
			$arrayError = Array('Error No:' => $e->getCode(),'Error Message:' => $e->getMessage(),'Stack Trace:' => nl2br($e->getTraceAsString()));
			//$arrayError = 'Error No: ' . $e->getCode().' Error Message: ' . $e->getMessage().' Stack Trace: ' . nl2br($e->getTraceAsString());
			return  Array('success' => false,'message' => "Error during the process",'error' => $arrayError);
		}
		
	}

	public function getCarModelList($request){
		$this->BDController = new BDController();
		try{
			$this->BDController->conectar();
			
			$query = '';
			$inicio = 0;
			$fin = 0;
			$num = 0;

			$andDealership = "";
			if(isset($_SESSION['dealership_id']) && $_SESSION['dealership_id'] != ''){
				$andDealership = " and c.dealership_id = '".$_SESSION['dealership_id']."'";
			}
			$query = "SELECT * 
                    FROM car_model c     
                    where 1 ".$andDealership;
			
			$this->BDController->setQuery($query);
			$result = $this->BDController->ejecutaInstruccion();
			$matches = Array();
			if(isset($request['box'])){
				$item = array('car_model_id' => 0,'dealership_id' => 0, 'description' => 'Select Car Model','tank_capacity' => 0);
				$matches[] = $item;
			}
			while ($item = $this->BDController->fetch($result)){
				$matches[] = $item;
				//$matches2[] = $item;//array('nombre' => $item['nombre']);
			}
			$this->BDController->desconectar();

			return  Array('success' => true,'totalCount' => $num,'matches' => $matches,'inicio' => $inicio,'fin' => $fin);
		} 			
		catch(Exception $e ) {
			$this->BDController->rollback();
			$this->BDController->desconectar();
			$arrayError = Array('Error No:' => $e->getCode(),'Error Message:' => $e->getMessage(),'Stack Trace:' => nl2br($e->getTraceAsString()));
			//$arrayError = 'Error No: ' . $e->getCode().' Error Message: ' . $e->getMessage().' Stack Trace: ' . nl2br($e->getTraceAsString());
			return  Array('success' => false,'mensaje' => "Error durante la operacion",'error' => $arrayError);
		}
		
	}

	public function getCarModel($request){
		$this->BDController = new BDController();
		try{
			$this->BDController->conectar();
			
			$query = '';
			$inicio = 0;
			$fin = 0;
			$num = 0;

			$query = "SELECT * 
                    FROM car_model l    
                    WHERE  car_model_id = '".$request['car_model_id']."'";
			
			$this->BDController->setQuery($query);
			$result = $this->BDController->ejecutaInstruccion();
			$matches = Array();
			while ($item = $this->BDController->fetch($result)){
				$matches[] = $item;
				//$matches2[] = $item;//array('nombre' => $item['nombre']);
			}
			$this->BDController->desconectar();

			return  Array('success' => true,'totalCount' => $num,'matches' => $matches,'inicio' => $inicio,'fin' => $fin);
		} 			
		catch(Exception $e ) {
			$this->BDController->rollback();
			$this->BDController->desconectar();
			$arrayError = Array('Error No:' => $e->getCode(),'Error Message:' => $e->getMessage(),'Stack Trace:' => nl2br($e->getTraceAsString()));
			//$arrayError = 'Error No: ' . $e->getCode().' Error Message: ' . $e->getMessage().' Stack Trace: ' . nl2br($e->getTraceAsString());
			return  Array('success' => false,'mensaje' => "Error durante la operacion",'error' => $arrayError);
		}
		
	}

	public function saveCarModel($request){
		$this->BDController = new BDController();
		try{
			$this->BDController->conectar();
			$this->BDController->autocommit(FALSE);
			$saveQuery = '';
			$requestOriginal = $request;
			
			if(!isset($request['car_model_id'])){
				$saveQuery = "INSERT INTO car_model (dealership_id,description,tank_capacity) 
								VALUES ('".$_SESSION['dealership_id']."','".$request['description']."','".$request['tank_capacity']."');";

				$this->BDController->setQuery($saveQuery);
				$this->BDController->ejecutaInstruccion();
				$lastId = $this->BDController->lastId();
			}
			else{
				$saveQuery = "update car_model set description = '".$request['description']."', 
												tank_capacity = '".$request['tank_capacity']."'   
								where car_model_id = '".$request['car_model_id']."'";

				$this->BDController->setQuery($saveQuery);
				$this->BDController->ejecutaInstruccion();	
				$lastId = $request['car_model_id'];
			}
				$this->BDController->commit();
			
			
			return  Array('success' => true,"message" => "Success","car_model_id" => $lastId);
		} 
		catch(Exception $e ) {
			$this->BDController->rollback();
			$this->BDController->desconectar();
			$arrayError = Array('Error No:' => $e->getCode(),'Error Message:' => $e->getMessage(),'Stack Trace:' => nl2br($e->getTraceAsString()));
			//$arrayError = 'Error No: ' . $e->getCode().' Error Message: ' . $e->getMessage().' Stack Trace: ' . nl2br($e->getTraceAsString());
			return  Array('success' => true,'message' => $e->getMessage(),'error' => $arrayError);
		}
		
	}

	public function getExceptionVehicleList($request){
		$this->BDController = new BDController();
		try{
			$this->BDController->conectar();
			
			$query = '';
			$andDealership = "";
			
			$query = "SELECT e.*  ,d.description department 
				FROM exception_vehicle e  
				inner join department d on d.department_id = e.department_id  
				 WHERE e.dealership_id = '".$_SESSION['dealership_id']."'  
				 order by description  ";
			$this->BDController->setQuery($query);
			$result = $this->BDController->ejecutaInstruccion();
			$num = $this->BDController->numero_filas($result);
			$matches = Array();
			while ($item = $this->BDController->fetch($result)){
				$matches[] = $item;
			}

			$this->BDController->desconectar();;
			return  Array('success' => true,'message' => 'Success','matches' => $matches,'totalCount' => $num);//
		} 
		catch(Exception $e ) {
			$this->BDController->rollback();
			$this->BDController->desconectar();
			$arrayError = Array('Error No:' => $e->getCode(),'Error Message:' => $e->getMessage(),'Stack Trace:' => nl2br($e->getTraceAsString()));
			//$arrayError = 'Error No: ' . $e->getCode().' Error Message: ' . $e->getMessage().' Stack Trace: ' . nl2br($e->getTraceAsString());
			return  Array('success' => false,'message' => "Error during the operation",'error' => $arrayError);
		}
		
	}

	public function getExceptionVehicle($request){
		$this->BDController = new BDController();
		try{
			$this->BDController->conectar();
			
			$query = '';
			$andDealership = "";
			
			$query = "SELECT e.*  ,d.description department 
				FROM exception_vehicle e  
				inner join department d on d.department_id = e.department_id  
				 WHERE e.exception_vehicle_id = '".$request['exception_vehicle_id']."'  
				 order by description  ";
			$this->BDController->setQuery($query);
			$result = $this->BDController->ejecutaInstruccion();
			$num = $this->BDController->numero_filas($result);
			$matches = Array();
			while ($item = $this->BDController->fetch($result)){
				$matches[] = $item;
			}

			$this->BDController->desconectar();;
			return  Array('success' => true,'message' => 'Success','matches' => $matches,'totalCount' => $num);//
		} 
		catch(Exception $e ) {
			$this->BDController->rollback();
			$this->BDController->desconectar();
			$arrayError = Array('Error No:' => $e->getCode(),'Error Message:' => $e->getMessage(),'Stack Trace:' => nl2br($e->getTraceAsString()));
			//$arrayError = 'Error No: ' . $e->getCode().' Error Message: ' . $e->getMessage().' Stack Trace: ' . nl2br($e->getTraceAsString());
			return  Array('success' => false,'message' => "Error during the operation",'error' => $arrayError);
		}
		
	}

	public function saveExceptionVehicle($request){
		$this->BDController = new BDController();
		try{
			$this->BDController->conectar();
			$this->BDController->autocommit(FALSE);
			$saveQuery = '';
			$requestOriginal = $request;
			
			if(!isset($request['exception_vehicle_id'])){
				$saveQuery = "INSERT INTO exception_vehicle (dealership_id,description,department_id) 
								VALUES ('".$_SESSION['dealership_id']."','".$request['description']."'
								,'".$request['department']['department_id']."');";

				$this->BDController->setQuery($saveQuery);
				$this->BDController->ejecutaInstruccion();
				$lastId = $this->BDController->lastId();
			}
			else{
				$saveQuery = "update exception_vehicle set description = '".$request['description']."', 
												department_id = '".$request['department']['department_id']."'   
								where exception_vehicle_id = '".$request['exception_vehicle_id']."'";

				$this->BDController->setQuery($saveQuery);
				$this->BDController->ejecutaInstruccion();	
				$lastId = $request['exception_vehicle_id'];
			}
				$this->BDController->commit();
			
			
			return  Array('success' => true,"message" => "Success","exception_vehicle_id" => $lastId);
		} 
		catch(Exception $e ) {
			$this->BDController->rollback();
			$this->BDController->desconectar();
			$arrayError = Array('Error No:' => $e->getCode(),'Error Message:' => $e->getMessage(),'Stack Trace:' => nl2br($e->getTraceAsString()));
			//$arrayError = 'Error No: ' . $e->getCode().' Error Message: ' . $e->getMessage().' Stack Trace: ' . nl2br($e->getTraceAsString());
			return  Array('success' => true,'message' => $e->getMessage(),'error' => $arrayError);
		}
		
	}
}

?>
