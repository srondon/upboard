<style type="text/css">

tr.espacio td {padding-top: 5px; padding-bottom:5px}
table {
    border-width: thin;
    border-spacing: 2px;
    border-style: none;
    border-color: black;
}
</style>

<?php

//require_once("../includes/clases/libs/php-barcode-master/barcode.php");
$MAIN_DIR = "../../";
require_once("../clases/controladores/AppController.php");
$request['year'] = substr($_REQUEST['month'], 3, 6);
$request['month'] = (string)((int)$_REQUEST['month']);
$controlador = new AppController($_FILES,$_REQUEST);//,$session
$request['accion'] = "Action/Stock/Department_Breakdown_Report";
$controlador->setRequest('',$request);
$result = $controlador->ejecutar();

//$balance = $result['matches'][0]['balance'];
//$commission = $result['matches'][0]['commission'];
$remainingGas = $result['matches'][0]['remaining_gas'];
$remainingRate = $result['matches'][0]['remaining_rate'];
$remainingAmount = $result['matches'][0]['remaining_amount'];

//$date = date_create($fecha);
$date = new DateTime();//

?>
<page style="font-size:14px">
<table style="width: 100%;" align="right" >
    <tr>
        <td style="font-size:21px;text-align:right">
        <!--img src="../includes/imagenes/icons/hotexpress-logo.png" alt="logo" style="width: 200px;"-->
            <font size='+3'><?php echo $month.' '.$request['year']?></font>
        </td>
    </tr>

</table>

<table cellpadding="0" cellspacing="0" style="width: 100%;" align="left" >
    <tr>
        <td colspan="3">
            <strong>Gas Usage</strong>
        </td>
    </tr>
    <tr style="background-color:#BAD9E3;border-style: solid; border-width: 1px">
        <td style="width: 33%;border-style: solid; border-width: 1px;">
            <strong>Galons</strong>
        </td>
        <td style="width: 33%;border-style: solid; border-width: 1px;">
            <strong>Rate</strong>
        </td>
        <td style="width: 33%;border-style: solid; border-width: 1px;">
            <strong>Total</strong>
        </td>
    </tr>
<?php 
$totalGalons = $totalCost = 0;
$color = "";
$i = 0;
foreach ($result['matchesAmount'] as $key => $detail) {

    $totalCost += $detail['total'];
    $totalGalons += $detail['quantity'];

    if($i % 2 == 0)
        $color = "#FFFFFF";
    else
        $color = "#E9F0F2";
?>
<tr style="background-color:<?php echo $color;?>">
    <td style="width: 33%;border-style: solid; border-width: 1px;;text-align:right;" ><?php echo number_format($detail['quantity'], 2, '.', ',')?></td>
    <td style="width: 33%;border-style: solid; border-width: 1px;;text-align:right;">$ <?php echo number_format($detail['filling_rate'], 6, '.', ',')?></td>
    <td style="width: 33%;border-style: solid; border-width: 1px;;text-align:right;">$ <?php echo number_format($detail['total'], 6, '.', ',')?></td>
</tr>
<?php 
    $i++;
 }?>
<!--tr>
    <td style="border-bottom: 1px  solid black;" colspan="3"><strong></strong></td>
</tr-->
<tr >
    <td colspan="3"><strong>Totals</strong></td>
</tr>
<tr>
    <td style="border-style: solid; border-width: 1px;;text-align:right;"><strong><?php echo number_format($totalGalons, 2, '.', ',')?></strong></td>
    <td><strong></strong></td>
    <td style="border-style: solid; border-width: 1px;;text-align:right;"><strong>$ <?php echo number_format($totalCost, 6, '.', ',')?></strong></td>
</tr>
</table>


<table style="width: 100%;" align="center" >
    <tr>
        <td style="">
        <br>
        </td>
    </tr>

</table>

<table style="width: 100%;" >

<?php 

$color = "";
$i = 0;
$totalRemaining = 0;
foreach ($result['matches'] as $key => $detail) { 
    $totalRemaining += $detail['remaining_gas'];
    ?>
    <tr>
        <td style="width: 35%;font-size:14px">
            <?php if ($i == 0) {
                echo "<strong>Remaining Gas On Hand:</strong>"; 
                $i++;
            }?>
         
        </td>
        <td style="width: 15%;font-size:14px;text-align:right; ">
        <?php echo number_format($detail['remaining_gas'], 2, '.', ',')?> gals
        </td>
        <td style="width: 25%;font-size:14px;text-align:right; ">
            $ <?php echo number_format($detail['remaining_rate'], 6, '.', ',')?>
        </td>
        <td style="width: 25%;font-size:14px;text-align:right; ">
            $ <?php echo number_format($detail['remaining_amount'], 6, '.', ',')?>
        </td>
    </tr>
<?php }?>
    <tr>
        <td style="width: 35%;font-size:14px">
            Total
         
        </td>
        <td style="width: 15%;font-size:14px;border-top: 1px  solid black;;text-align:right; ">
        <?php echo number_format($totalRemaining, 2, '.', ',')?> gals
        </td>
        <td style="width: 25%;font-size:14px ">
        </td>
        <td style="width: 25%;font-size:14px ">
        </td>
    </tr>
</table>

<table style="width: 100%;" align="center" >
    <tr>
        <td style="width: 100% ; border: 1px  solid black;">
        <br>
        </td>
    </tr>
</table>

<br>
<!--  border: 1px  solid black; -->

<table style="width: 100%;" align="center" >
    <tr>
        <td >
        <br>
        </td>
    </tr>
</table>

<table cellpadding="0" cellspacing="0"  style="width: 50%; " align="left" >
    
<?php 
$totalAmount = 0;
$totalQuantity = 0;
$initRate = "";
$color = "#FFFFFF";
$i = 0;
foreach ($result['matchesDepartment'] as $key => $detail) 
{

    
    if($i % 2 == 0)
        $color = "#FFFFFF";
    else
        $color = "#E9F0F2";

    $totalAmount += $detail['total'];
    $totalQuantity += $detail['quantity'];

    if($initRate != $detail['stock_entry_id']){
        if($initRate != ''){
            $totalAmount -= $detail['total'];
            $totalQuantity -= $detail['quantity'];
            ?>
            <tr style="border: 1px  solid black;">
                <td><strong>Total</strong></td>
                <td style="border: 1px  solid black;;text-align:right;"><strong><?php echo number_format($totalQuantity, 2, '.', ',')?></strong></td>
                <td style="border: 1px  solid black;;text-align:right;"><strong>$ <?php echo number_format($totalAmount, 6, '.', ',')?></strong></td>
            </tr>
        <?php 
            $totalAmount = 0;
            $totalQuantity = 0;
            $totalAmount += $detail['total'];
            $totalQuantity += $detail['quantity'];
            $color = "#FFFFFF";
            $i = 0;
        }
        $initRate = $detail['stock_entry_id'];
        
    ?>
        <tr>
            <td colspan="2">
            <strong>Gas Usage By Department</strong>
            </td>
            <td >
            <strong>Rate: <?php echo $detail['filling_rate']?> </strong>
            </td>
        </tr>
        <tr style="background-color:#BAD9E3;border-style: solid; border-width: 1px">
            <td style="width: 50%;border-style: solid; border: 1px  solid black;">
            <strong>Department</strong>
            </td>
            <td style="width: 25%;border: 1px  solid black;">
            <strong>Gallons</strong>
            </td>
            <td style="width: 25%;border: 1px  solid black;">
            <strong>Amount</strong>
            </td>
        </tr>
    <?php
    }
?>
<tr style="background-color:<?php echo $color;?>">
    <td style="border: 1px  solid black;"><?php echo $detail['department']?></td>
    <td style="border: 1px  solid black;;text-align:right;"><?php echo number_format($detail['quantity'], 2, '.', ',')?></td>
    <td style="border: 1px  solid black;;text-align:right;">$ <?php echo number_format($detail['total'], 6, '.', ',')?></td>
</tr>
<?php 
    $i++;
}?>
<tr style="border: 1px  solid black;">
    <td><strong>Total</strong></td>
    <td style="border: 1px  solid black;;text-align:right;"><strong><?php echo number_format($totalQuantity, 2, '.', ',')?></strong></td>
    <td style="border: 1px  solid black;;text-align:right;"><strong>$ <?php echo number_format($totalAmount, 6, '.', ',')?></strong></td>
</tr>
</table>
<br>

<table cellpadding="0" cellspacing="0"  style="width: 50%; " align="left" >
    <tr>
        <td colspan="3">
        <strong>Gas Breakdown By Department. Total Expense</strong>
        </td>
    </tr>
    <tr style="background-color:#BAD9E3;border-style: solid; border-width: 1px">
        <td style="width: 50%;border-style: solid; border: 1px  solid black;">
        <strong>Department</strong>
        </td>
        <td style="width: 25%;border: 1px  solid black;">
        <strong>Gallons</strong>
        </td>
        <td style="width: 25%;border: 1px  solid black;">
        <strong>Amount</strong>
        </td>

    </tr>
<?php 
$totalAmount = 0;
$totalQuantity = 0;
$color = "";
$i = 0;
foreach ($result['matchesDepartmentExpense'] as $key => $detail) 
{

    $totalAmount += $detail['total'];
    $totalQuantity += $detail['quantity'];

    if($i % 2 == 0)
        $color = "#FFFFFF";
    else
        $color = "#E9F0F2";
?>
<tr style="background-color:<?php echo $color;?>">
    <td style="border: 1px  solid black;"><?php echo $detail['department']?></td>
    <td style="border: 1px  solid black;;text-align:right;"><?php echo number_format($detail['quantity'], 2, '.', ',')?></td>
    <td style="border: 1px  solid black;;text-align:right;">$ <?php echo number_format($detail['total'], 6, '.', ',')?></td>
</tr>
<?php 
    $i++;
}?>
<tr style="border: 1px  solid black;">
    <td><strong>Total</strong></td>
    <td style="border: 1px  solid black;;text-align:right;"><strong><?php echo number_format($totalQuantity, 2, '.', ',')?></strong></td>
    <td style="border: 1px  solid black;;text-align:right;"><strong>$ <?php echo number_format($totalAmount, 6, '.', ',')?></strong></td>
</tr>
</table>

    <page_footer>
        <table style="width: 100%; border-bottom: 1px  solid black;" align="left" >
            <tr>
                <td style="width: 50%;">
                    <strong>Printed By</strong>
                </td>
                <td style="width: 25%;">
                    <strong><?php echo $_SESSION['name']?></strong>
                </td>
                <td style="width: 25%;">
                    <strong><?php echo date_format($date, 'm/d/Y')?></strong>
                </td>
            </tr>
        </table>
    </page_footer>
</page>