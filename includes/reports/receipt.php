<?php
$MAIN_DIR = "../../";
require_once("../clases/controladores/AppController.php");
$controlador = new AppController($_FILES,$_REQUEST);//,$session
$request['fuel_filling_id'] = $_GET['receipt'];
$request['accion'] = "Action/Stock/Fill_Receipt_Report";
$controlador->setRequest('',$request);
$result = $controlador->ejecutar();

//$date = date_create($fecha);

?>
<style type="text/css">
<!--
    div.zone { border: 1px;border-color:#000000; border-radius: 6mm; background: #FFFFFF; border-collapse: collapse; padding:3mm; font-size: 2.7mm;}
    h1 { padding: 0; margin: 0; color: #DD0000; font-size: 7mm; }
    h2 { padding: 0; margin: 0; color: #222222; font-size: 5mm; position: relative; }
    b { font-size: 2.5mm;}
-->
</style>
<page format="150x100" orientation="P" backcolor="#FFFFFF" style="font: arial;">
    <div style="rotate: 90; position: absolute; width: 100mm; height: 4mm; left: 195mm; top: 0; font-style: italic; font-weight: normal; text-align: center; font-size: 2.5mm;">
        
    </div>
    <table style="width: 99%;border: none;" cellspacing="4mm" cellpadding="0">
        <tr>
            <td colspan="2" style="width: 100%">
                <div class="zone" style="height: 25mm;position: relative;font-size: 5mm;">
                    <div style="position: absolute; right: 3mm; top: 3mm; text-align: right; font-size: 4mm; ">
                        <b>RECEIPT</b><br>
                        <b class='d'>Department: <?php echo $result['matches'][0]['department']; ?></b><br>
                        <b class='d'>Porter: <?php echo $result['matches'][0]['name']; ?></b><br>
                        <b class='d'>Car Type: <?php echo $result['matches'][0]['car_type']; ?></b><br>
                        <b class='d'>Car: <?php echo $result['matches'][0]['car_stock_number']; ?></b><br>
                        <b class='d'>Fill #: <?php echo $result['matches'][0]['fuel_filling_id']; ?></b><br>
                        <b class='d'>Date: <?php echo $result['matches'][0]['date']; ?></b>
                    </div>
                    <div style="position: absolute; right: 3mm; bottom: 3mm; text-align: right; font-size: 2.5mm; ">
                        
                    </div>
                    <qrcode value="1" style="position: absolute; left: 3mm; top: 3mm;width: 30mm;"></qrcode>
                    <!--img src="./res/logo.gif" alt="logo" style="margin-top: 3mm; margin-left: 20mm"-->
                </div>
            </td>
        </tr>
        <tr>
            <td style="width: 50%;">
             Galons   
            </td>
            <td style="width: 50%">
             Initial/Final Reading   
            </td>
        </tr>
        <?php foreach ($result['matches'] as $key => $detail) {
            
        ?>
        <tr>
            <td style="width: 50%;">
                <?php echo $detail['quantity']; ?>
            </td>
            <td style="width: 50%">
                <?php echo $detail['initial_reading']. ' - '.$detail['final_reading'];?>
            </td>
        </tr>
        <?php }?>
    </table>
</page>