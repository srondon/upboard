<?php //require_once ('../../includes/clases/CONF.php');?>
<section class="content-header">
          <h1>
            Users List
            <!--small>Control panel</small-->
          </h1>
          <!--ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Calendar</li>
          </ol-->
</section>
<section class="content">

  <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <button ng-disabled="disableAdd" type="button" ng-click="newUser()" class="btn btn-info">New User</button>
                  <!--a href="<?php echo HOST;?>/fuelcontrol/index.php/#/newUser/" class="btn btn-info">New User</a-->
                  <!--h3 class="box-title">Hover Data Table</h3-->
                </div><!-- /.box-header -->
                <div class="box-body">
                  <pagination total-items="totalItems" ng-model="currentPage" ng-change="pageChanged()" max-size="maxSize" items-per-page="30" boundary-links="true" style="float:right"></pagination>
                  <table id="example2" class="table table-bordered table-hover">
                    <thead>
                      <tr>
                        <th>Name</th>
                        <th>User Name</th>
                        <th>Profile</th>
                        <th>Dealerships</th>
                        <th width="20%"></th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr ng-repeat="user in users">
                        <td>{{user.name}}</td>
                        <td>{{user.username}}</td>
                        <td>{{user.profile}}</td>
                        <td>{{user.dealerships}}</td>
                        <td>
                            <div class="btn-group">
                              <button type="button" class="btn btn-info">Action</button>
                              <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                                <span class="caret"></span>
                                <span class="sr-only">Toggle Dropdown</span>
                              </button>
                              <ul class="dropdown-menu" role="menu">
                                <li><a href="#/User/View/{{user.user_id}}">View</a></li>
                                <li><a ng-click="delete(user.user_id)">Delete</a></li>
                                <!--li><a href="#">Modify</a></li-->
                                <!--li class="divider"></li>
                                <li><a href="#">Separated link</a></li-->
                              </ul>
                            </div>
                        </td>
                      </tr>
                    </tbody>
                    <!--tfoot>
                      <tr>
                        <th>Rendering engine</th>
                        <th>Browser</th>
                      </tr>
                    </tfoot-->
                  </table>
                  <pagination total-items="totalItems" ng-model="currentPage" ng-change="pageChanged()" max-size="maxSize" items-per-page="30" boundary-links="true" style="float:right"></pagination>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
      </section>