<?php
   require_once("../includes/clases/CONF.php");
   session_start();

    if(!isset($_SESSION['name'])){
      header("Location:../login.php?install");
    }
    if ($now >= $_SESSION['session_timeout']) {
      session_destroy();
      header("Location:../login.php?install");
    }
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo APP_NAME?> | New Dealership</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="../dist/css/skins/_all-skins.min.css">
    <link rel="stylesheet" href="../dist/css/plugins.css">
    <link rel="stylesheet" href="../dist/css/style.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script>
      var ROOT_FOLDER = "<?php echo ROOT_FOLDER;?>";
    </script>

    <script data-require="jquery@*" data-semver="2.1.1" src="../includes/js/jquery.min.js"></script>
    <script data-require="angular.js@1.3.0" data-semver="1.3.0" ng:autobind src="../includes/js/angular.js"></script>
    <script src="../includes/js/angular-sanitize.js"></script>
    <script src="../includes/js/ui-bootstrap-tpls.js"></script>
    <script src="../includes/js/angular-confirm.js"></script>
    <!--script src="includes/js/angular.min.js"></script-->
    <script type="text/javascript" src="../includes/js/angular-resource.min.js"></script>
    <script data-require="ng-route@1.2.0" data-semver="1.2.0" src="../includes/js/angular-route.js"></script>
    <script src="../includes/js/ui-bootstrap-tpls-0.12.0.js"></script>

    <script src="../includes/js/angular-animate.js"></script>
    <script src="../includes/js/angular-file-upload/dist/angular-file-upload.min.js"></script>
    <script src="../includes/js/app/install_app.js"></script>
    <script src="../includes/js/checklist-model/checklist-model.js"></script>
    <script src="../includes/js/app/install_controllers.js"></script>
    <script src="../includes/js/ngDialog.js"></script>
    <script src="../includes/js/angular-multi-check.js"></script>

    <link href="../includes/js/xeditable.css" rel="stylesheet">
    <script src="../includes/js/xeditable.js"></script>
  </head>
  <!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
  <body class="hold-transition skin-blue layout-top-nav" data-ng-app="fuelcontrol-install-app">
    <div class="wrapper" ng-controller="wizard" id="wizard">

      <header class="main-header">
        <nav class="navbar navbar-static-top">
          <div class="container">
            <div class="navbar-header">
              <a href="" class="navbar-brand"><b><?php echo APP_NAME?></a>
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                <i class="fa fa-bars"></i>
              </button>
            </div>

            
            <!-- Navbar Right Menu -->
              <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                  
                </ul>
              </div><!-- /.navbar-custom-menu -->
          </div><!-- /.container-fluid -->
        </nav>
      </header>
      <!-- Full Width Column -->
      <div class="content-wrapper">
        <div class="container">
          <!-- Content Header (Page header) -->
          <?php if($_SESSION['user_id'] == 1){?>
          <section class="content-header">
            <h1>
              Installing a New Dealership
              <small></small>
            </h1>
          </section>

          <!-- Main content -->
          <section class="content">
            <!--div class="alert alert-info alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><i class="icon fa fa-info"></i> Tip!</h4>
                    Info alert preview. This alert is dismissable.
            </div-->
            <div class="box box-default">
              <!--div class="box-header with-border">
                <h3 class="box-title">Blank Box</h3>
              </div-->
              <div class="box-body">
                <div class="form-wizard">
                  <div class="form-body">
                    <ul class="nav nav-pills nav-justified steps">
                          <li id="li1" class="active">
                            <a onclick="changeTabClick(1)"  class="step" data-target="#step1" data-toggle="tab"  id="stepTab1"> <!-- href="#step1"  -->
                              <span class="number">
                                 1
                              </span>
                              <span class="desc">
                                <i class="fa fa-check"></i> Enter License
                              </span>
                            </a>
                          </li>
                          <li id="li2">
                            <a onclick="changeTabClick(2)" class="step" data-target="#step2" data-toggle="tab"  id="stepTab2">
                              <span class="number">
                                 2
                              </span>
                              <span class="desc">
                                <i class="fa fa-check"></i> General Config.
                              </span>
                            </a>
                          </li>
                          <li id="li3">
                            <a onclick="changeTabClick(3)" class="step" data-target="#step3" data-toggle="tab"  id="stepTab3">
                              <span class="number">
                                 3
                              </span>
                              <span class="desc">
                                <i class="fa fa-check"></i> Profiles Setup
                              </span>
                            </a>
                          </li>
                          <li id="li4">
                            <a onclick="changeTabClick(4)" class="step" data-target="#step4" data-toggle="tab"  id="stepTab4">
                              <span class="number">
                                 4
                              </span>
                              <span class="desc">
                                <i class="fa fa-check"></i> Administrator Setup
                              </span>
                            </a>
                          </li>
                          <li id="li5">
                            <a onclick="changeTabClick(5)" class="step" data-target="#step5" data-toggle="tab"  id="stepTab5">
                              <span class="number">
                                 5
                              </span>
                              <span class="desc">
                                <i class="fa fa-check"></i> Setup Confirmation
                              </span>
                            </a>
                          </li>
                        </ul>

                      <div class="progress active">
                        <div id="progress_bar" class="progress-bar progress-bar-success progress-bar-striped " role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%">
                        </div>
                      </div>
                      <div class="tab-content">
                        <div class="tab-pane active" id="step1">
                          <div class='row' >
                            <div class="col-xs-6" style="text-align:center">
                              <b>Please Enter your Dealership Group License:</b>
                              <div id="license_div" class="form-group "> <!-- has-success has-error -->
                                <label id="lincese_success_label" style="visibility:hidden" class="control-label" for="inputSuccess"><i class="fa fa-check"></i> The license is valid. Dealership Group: {{dealership_group}}</label>
                                <label id="lincese_error_label" style="visibility:hidden" class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i> Input with error</label>
                                <input type="text" ng-blur="validateLicense()" ng-model="install.license" class="form-control" placeholder="Enter ...">
                                <img src="../includes/images/spinner.gif" width="40" height="40" style="visibility:hidden"  id="loading_image" />                    
                              </div>
                            </div>
                          </div>
                        </div><!-- /.tab-pane -->
                        <div class="tab-pane" id="step2">
                          <form id="form-step2">
                            <div class="box-body">
                               <div class='row'>
                                <div class="col-xs-4">
                                  <div class="form-group">
                                    <label for="exampleInputEmail1">Company Name</label>
                                    <input required  type="text" class="form-control" ng-model="install.company_name" id="company_name" placeholder="Enter company name">
                                  </div>
                                </div>
                                <div class="col-xs-4">
                                  <div class="form-group">
                                    <label for="exampleInputEmail1">Short Name</label>
                                    <input required  type="text" class="form-control" ng-model="install.short_name" id="short_name" placeholder="Enter short name">
                                  </div>
                                </div>
                                <div class="col-xs-4">
                                  <div class="form-group">
                                    <label for="exampleInputEmail1">Address</label>
                                    <input required  type="text" class="form-control" ng-model="install.address" id="address" placeholder="Enter address">
                                  </div>
                                </div>
                              </div>
                              <div class='row'>
                                <div class="col-xs-4">
                                  <div class="form-group">
                                    <label for="exampleInputEmail1">Time Zone</label>
                                      <select required  class="form-control" ng-model='install.time_zone' >
                                        <option value="Pacific/Midway">(GMT-11:00) Midway Island, Samoa</option>
                                        <option value="America/Adak">(GMT-10:00) Hawaii-Aleutian</option>
                                        <option value="Etc/GMT+10">(GMT-10:00) Hawaii</option>
                                        <option value="Pacific/Marquesas">(GMT-09:30) Marquesas Islands</option>
                                        <option value="Pacific/Gambier">(GMT-09:00) Gambier Islands</option>
                                        <option value="America/Anchorage">(GMT-09:00) Alaska</option>
                                        <option value="America/Ensenada">(GMT-08:00) Tijuana, Baja California</option>
                                        <option value="Etc/GMT+8">(GMT-08:00) Pitcairn Islands</option>
                                        <option value="America/Los_Angeles">(GMT-08:00) Pacific Time (US & Canada)</option>
                                        <option value="America/Denver">(GMT-07:00) Mountain Time (US & Canada)</option>
                                        <option value="America/Chihuahua">(GMT-07:00) Chihuahua, La Paz, Mazatlan</option>
                                        <option value="America/Dawson_Creek">(GMT-07:00) Arizona</option>
                                        <option value="America/Belize">(GMT-06:00) Saskatchewan, Central America</option>
                                        <option value="America/Cancun">(GMT-06:00) Guadalajara, Mexico City, Monterrey</option>
                                        <option value="Chile/EasterIsland">(GMT-06:00) Easter Island</option>
                                        <option value="America/Chicago">(GMT-06:00) Central Time (US & Canada)</option>
                                        <option value="America/New_York">(GMT-05:00) Eastern Time (US & Canada)</option>
                                        <option value="America/Havana">(GMT-05:00) Cuba</option>
                                        <option value="America/Bogota">(GMT-05:00) Bogota, Lima, Quito, Rio Branco</option>
                                        <option value="America/Caracas">(GMT-04:30) Caracas</option>
                                        <option value="America/Santiago">(GMT-04:00) Santiago</option>
                                        <option value="America/La_Paz">(GMT-04:00) La Paz</option>
                                        <option value="Atlantic/Stanley">(GMT-04:00) Faukland Islands</option>
                                        <option value="America/Campo_Grande">(GMT-04:00) Brazil</option>
                                        <option value="America/Goose_Bay">(GMT-04:00) Atlantic Time (Goose Bay)</option>
                                        <option value="America/Glace_Bay">(GMT-04:00) Atlantic Time (Canada)</option>
                                        <option value="America/St_Johns">(GMT-03:30) Newfoundland</option>
                                        <option value="America/Araguaina">(GMT-03:00) UTC-3</option>
                                        <option value="America/Montevideo">(GMT-03:00) Montevideo</option>
                                        <option value="America/Miquelon">(GMT-03:00) Miquelon, St. Pierre</option>
                                        <option value="America/Godthab">(GMT-03:00) Greenland</option>
                                        <option value="America/Argentina/Buenos_Aires">(GMT-03:00) Buenos Aires</option>
                                        <option value="America/Sao_Paulo">(GMT-03:00) Brasilia</option>
                                        <option value="America/Noronha">(GMT-02:00) Mid-Atlantic</option>
                                        <option value="Atlantic/Cape_Verde">(GMT-01:00) Cape Verde Is.</option>
                                        <option value="Atlantic/Azores">(GMT-01:00) Azores</option>
                                        <option value="Europe/Belfast">(GMT) Greenwich Mean Time : Belfast</option>
                                        <option value="Europe/Dublin">(GMT) Greenwich Mean Time : Dublin</option>
                                        <option value="Europe/Lisbon">(GMT) Greenwich Mean Time : Lisbon</option>
                                        <option value="Europe/London">(GMT) Greenwich Mean Time : London</option>
                                        <option value="Africa/Abidjan">(GMT) Monrovia, Reykjavik</option>
                                        <option value="Europe/Amsterdam">(GMT+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna</option>
                                        <option value="Europe/Belgrade">(GMT+01:00) Belgrade, Bratislava, Budapest, Ljubljana, Prague</option>
                                        <option value="Europe/Brussels">(GMT+01:00) Brussels, Copenhagen, Madrid, Paris</option>
                                        <option value="Africa/Algiers">(GMT+01:00) West Central Africa</option>
                                        <option value="Africa/Windhoek">(GMT+01:00) Windhoek</option>
                                        <option value="Asia/Beirut">(GMT+02:00) Beirut</option>
                                        <option value="Africa/Cairo">(GMT+02:00) Cairo</option>
                                        <option value="Asia/Gaza">(GMT+02:00) Gaza</option>
                                        <option value="Africa/Blantyre">(GMT+02:00) Harare, Pretoria</option>
                                        <option value="Asia/Jerusalem">(GMT+02:00) Jerusalem</option>
                                        <option value="Europe/Minsk">(GMT+02:00) Minsk</option>
                                        <option value="Asia/Damascus">(GMT+02:00) Syria</option>
                                        <option value="Europe/Moscow">(GMT+03:00) Moscow, St. Petersburg, Volgograd</option>
                                        <option value="Africa/Addis_Ababa">(GMT+03:00) Nairobi</option>
                                        <option value="Asia/Tehran">(GMT+03:30) Tehran</option>
                                        <option value="Asia/Dubai">(GMT+04:00) Abu Dhabi, Muscat</option>
                                        <option value="Asia/Yerevan">(GMT+04:00) Yerevan</option>
                                        <option value="Asia/Kabul">(GMT+04:30) Kabul</option>
                                        <option value="Asia/Yekaterinburg">(GMT+05:00) Ekaterinburg</option>
                                        <option value="Asia/Tashkent">(GMT+05:00) Tashkent</option>
                                        <option value="Asia/Kolkata">(GMT+05:30) Chennai, Kolkata, Mumbai, New Delhi</option>
                                        <option value="Asia/Katmandu">(GMT+05:45) Kathmandu</option>
                                        <option value="Asia/Dhaka">(GMT+06:00) Astana, Dhaka</option>
                                        <option value="Asia/Novosibirsk">(GMT+06:00) Novosibirsk</option>
                                        <option value="Asia/Rangoon">(GMT+06:30) Yangon (Rangoon)</option>
                                        <option value="Asia/Bangkok">(GMT+07:00) Bangkok, Hanoi, Jakarta</option>
                                        <option value="Asia/Krasnoyarsk">(GMT+07:00) Krasnoyarsk</option>
                                        <option value="Asia/Hong_Kong">(GMT+08:00) Beijing, Chongqing, Hong Kong, Urumqi</option>
                                        <option value="Asia/Irkutsk">(GMT+08:00) Irkutsk, Ulaan Bataar</option>
                                        <option value="Australia/Perth">(GMT+08:00) Perth</option>
                                        <option value="Australia/Eucla">(GMT+08:45) Eucla</option>
                                        <option value="Asia/Tokyo">(GMT+09:00) Osaka, Sapporo, Tokyo</option>
                                        <option value="Asia/Seoul">(GMT+09:00) Seoul</option>
                                        <option value="Asia/Yakutsk">(GMT+09:00) Yakutsk</option>
                                        <option value="Australia/Adelaide">(GMT+09:30) Adelaide</option>
                                        <option value="Australia/Darwin">(GMT+09:30) Darwin</option>
                                        <option value="Australia/Brisbane">(GMT+10:00) Brisbane</option>
                                        <option value="Australia/Hobart">(GMT+10:00) Hobart</option>
                                        <option value="Asia/Vladivostok">(GMT+10:00) Vladivostok</option>
                                        <option value="Australia/Lord_Howe">(GMT+10:30) Lord Howe Island</option>
                                        <option value="Etc/GMT-11">(GMT+11:00) Solomon Is., New Caledonia</option>
                                        <option value="Asia/Magadan">(GMT+11:00) Magadan</option>
                                        <option value="Pacific/Norfolk">(GMT+11:30) Norfolk Island</option>
                                        <option value="Asia/Anadyr">(GMT+12:00) Anadyr, Kamchatka</option>
                                        <option value="Pacific/Auckland">(GMT+12:00) Auckland, Wellington</option>
                                        <option value="Etc/GMT-12">(GMT+12:00) Fiji, Kamchatka, Marshall Is.</option>
                                        <option value="Pacific/Chatham">(GMT+12:45) Chatham Islands</option>
                                        <option value="Pacific/Tongatapu">(GMT+13:00) Nuku'alofa</option>
                                        <option value="Pacific/Kiritimati">(GMT+14:00) Kiritimati</option>
                                      </select>
                                  </div>
                                </div>
                                <div class="col-xs-4">
                                  <div class="form-group">
                                    <label for="exampleInputEmail1">Logo </label>
                                    <input  required  type="text" class="form-control" ng-model="install.logo" id="logo" placeholder="Enter users logo"/>
                                  </div>
                                </div>
                              </div>  
                              
                            </div>
                          </form>
                        </div><!-- /.tab-pane -->
                        
                        <div class="tab-pane" id="step3">
                          <div class='row'>
                            <div class="col-xs-2">
                                <div class="form-group">
                                  <label>Profile Description</label>
                                  <input type="text" class="form-control" ng-model="profile.description" placeholder="Enter description">
                                </div>                                
                            </div>
                            <div class="col-xs-3">
                              <div class="form-group">
                                <label>Menu Options</label>
                                <select multiple size="10" class="form-control" ng-model="profile.menu_options" ng-options="option.menu group by option.parent_menu for option in menuoptions">
                                </select>
                              </div>
                              <!--div class="form-group">
                                <label>Dealerships From Group</label>
                                <select multiple size="10" class="form-control" ng-model="profile.dealer_access" ng-options="dealer.company_name for dealer in profileDealerGroup">
                                </select>
                              </div-->
                            </div>

                            <div class="col-xs-1" style="text-align:center">
                                <div class="form-group " style="text-align:center"> <!-- has-success-->
                                  <button ng-click="addProfile()" class="btn btn-success">Add Profile</button>
                                </div>
                              </div>
                            <div class="col-xs-2">
                              <div class="form-group">
                                <label>Profiles</label>
                                <select ng-change="selectProfile()" size="10" class="form-control" ng-model="profile.profiles" ng-options="profileDefault.description for profileDefault in install.profileDefaults">
                                </select>
                              </div>                              
                              <!--div class="form-group">
                                <label>Dealerships Access From Group</label>
                                <select multiple size="10" class="form-control" ng-model="profile.dealer_access" ng-options="dealer.company_name for dealer in install.profileDealerAccess">
                                </select>
                              </div-->
                            </div>
                            <div class="col-xs-4">
                              <div class="form-group">
                                <label>Menu Options Access</label>                                
                                  <table class="table table-bordered">
                                    <tr ng-repeat="profile_access in profileDetails">
                                      <td style="width: 45%">
                                          {{profile_access.parent_menu}}
                                      </td>
                                      <td style="width: 45%">
                                          {{profile_access.menu}}
                                      </td>
                                      <td style="width: 10%">
                                          <span style="cursor:pointer" ng-click='changeAccess($index)' class="label label-{{profile_access.allow==1?'success':'danger'}}">{{profile_access.allow==1?'Yes':'No'}}</span>
                                      </td>
                                    </tr>                              
                                  </table>
                              </div>
                            </div>
                          </div>
                        </div><!-- /.tab-pane -->
                        <div class="tab-pane" id="step4">
                            
                            <div class="col-xs-4">
                              <div class="form-group">
                                <label for="exampleInputEmail1">Admins on Dealership Group: </label>
                                <select size="5" class="form-control" ng-model="useAdminOnGroup" ng-options="admin.description for admin in install.adminsOnGroup2">
                                </select>
                              </div>
                            </div>
                            <div class="col-xs-1" style="text-align:center">
                                <div class="form-group " style="text-align:center"> <!-- has-success-->
                                  <button style="width:65px" ng-click="useAdmin()" class="btn btn-success">&nbsp;&nbsp;Use&nbsp;&nbsp;</button>
                                  <br>
                                  <button ng-disabled="unUseDisable" style="width:65px" ng-click="unUseAdmin()" class="btn btn-danger">Unuse</button>
                                </div>
                              </div>
                        </div>
                        <div class="tab-pane" id="step5">
                          <div class="col-xs-12" style="width:80%;word-wrap: break-word;">
                            {{install}}
                          </div>
                        </div><!-- /.tab-pane -->
                      </div><!-- /.tab-content -->

                    </div>
                  </div>
              </div><!-- /.box-body  ng-click="next()" ng-click="previous()"  -->
              <div class="box-footer">
                <button ng-disabled='previousDisabled'  onclick="changeTab(2)" class="btn btn-primary">Previous</button>
                <button ng-disabled='nextDisabled' onclick="changeTab(1)" class="btn btn-primary">Next</button>
                <button ng-click="exit()" class="btn btn-danger" style="float:right;margin-left:4px">Exit</button>
                &nbsp;
                <button ng-disabled='finishDisabled' ng-click="finish()" class="btn btn-success" style="float:right">Finish</button>
                &nbsp;
                <img src="../includes/images/spinner.gif" width="40" height="40" style="float:right;visibility:hidden"  id="loading_image2" />
              </div>
            </div><!-- /.box -->
          </section><!-- /.content -->
        </div><!-- /.container -->
      </div><!-- /.content-wrapper -->
      <?php }else{?>
            <section class="content-header">
            <h1>
              Access Denied
              <small></small>
            </h1>
          </section>
      <?php }?>
      <footer class="main-footer">
        <div class="container">
          <div class="pull-right hidden-xs">
            <b>Version</b> 2.3.0
          </div>
          <strong>Copyright &copy; 2014-2015 <a href="http://almsaeedstudio.com">Almsaeed Studio</a>.</strong> All rights reserved.
        </div><!-- /.container -->
      </footer>
    </div><!-- ./wrapper -->

    <!-- jQuery 2.1.4 -->
    <script src="../plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="../bootstrap/js/bootstrap.min.js"></script>
    <!-- SlimScroll -->
    <script src="../plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="../plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="../dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="../dist/js/demo.js"></script>

    <script type="text/javascript" id="runscript">
    function changeTabClick(tab){
          /*var activeTab = tab;
          var scope = angular.element(document.getElementById('wizard')).scope();
          scope.$apply(function(){
              scope.ActiveTab = parseInt(activeTab);
              var percentage = 100/9;
              percentage = percentage*scope.ActiveTab;
              percentage = percentage.toFixed(2);
              document.getElementById('progress_bar').style.width = percentage+"%"
              activeTab = scope.ActiveTab;
          });
          scope.$apply(function(){
              if(op == 1){
                scope.next();
              }
              else{
                scope.previous();
              }
          });*/
    }

    function changeTab(op){
          var activeTab = 1;
          var validTab = false;
          var scope = angular.element(document.getElementById('wizard')).scope();
          
          if(op == 1){
              scope.$apply(function(){
                validTabMessage = scope.validateTab(scope.ActiveTab);
              });

              if(validTabMessage != ''){
                alert(validTabMessage);
                return;
              }
          }
          

          scope.$apply(function(){
              //validTab = scope.isTabValid();
              //if(validTab){
                if(op == 1){
                  scope.ActiveTab = parseInt(scope.ActiveTab);
                  scope.ActiveTab++;
                }
                else{
                  scope.ActiveTab = parseInt(scope.ActiveTab);
                  scope.ActiveTab--;
                }
                var percentage = 100/10;
                percentage = percentage*scope.ActiveTab;
                percentage = percentage.toFixed(2);
                document.getElementById('progress_bar').style.width = percentage+"%"
                activeTab = scope.ActiveTab;
              //}              
          });
          $("#stepTab"+activeTab).trigger("click");
          scope.$apply(function(){
            //if(validTab){
              if(op == 1){
                scope.next();
              }
              else{
                scope.previous();
              }
            //}
          });
    }
    </script>
  </body>
</html>

