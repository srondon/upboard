<?php

require_once $MAIN_DIR.'includes/clases/modelos/ModelSalesBoard.php';
//if(!isset($_SESSION)){session_start();} 

class ActionSalesBoard {
	
	private $session = "";
	
	public function load_upboard_state($request){
		$sessionModelo = new ModelSalesBoard();
		
		$response = $sessionModelo->loadUpboardState($request);
		
		return $response;
	}

	public function change_saleman_status($request){
		$sessionModelo = new ModelSalesBoard();
		
		$response = $sessionModelo->changeSalemanStatus($request);
		
		return $response;
	}	

	public function reset($request){
		$sessionModelo = new ModelSalesBoard();
		
		$response = $sessionModelo->reset($request);
		
		return $response;
	}	

	public function resetAll($request){
		$sessionModelo = new ModelSalesBoard();
		
		$response = $sessionModelo->resetAll($request);
		
		return $response;
	}	

	public function notify($request){
		$sessionModelo = new ModelSalesBoard();
		
		$response = $sessionModelo->notify($request);
		
		return $response;
	}
}
?>
