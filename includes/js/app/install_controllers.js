angular.module('fuelcontrol-install-app')
.controller('wizard', function ($scope,$http,$routeParams,$location,FileUploader,$filter) {
    
    $scope.install = {
        dealership_group_id:0,
        license:'',
        company_name:'',
        short_name:'',
        address:'',
        time_zone:'',
        logo:'',
        profileDefaults:[],
        profileAccess:[],
        profileDealerAccess:[],
        adminsOnGroup:[],
        adminsOnGroup2:[],
        useAdminOnGroup:0
    };

    $scope.passwordDisable = false;
    $scope.previousDisabled = true;
    $scope.nextDisabled = true;
    $scope.finishDisabled = true;
    $scope.install.tanks = [];
    $scope.install.departments = [];
    $scope.install.fuelTypes = [];
    $scope.install.carModels = [];
    $scope.menuoptions = [];
    $scope.profileDefaults = [];
    $scope.profileAccess = [];
    $scope.profileDetails = [];
    $scope.profileDealerGroup = [];
    $scope.counterTank = 0;
    $scope.counterDepartment = 0;
    $scope.counterCarModel = 0;
    $scope.counterFuelType = 0;
    $scope.validLicense = false;

    $scope.newProfileId = 0;
    $scope.ActiveTab = 1;
    $scope.currentTabValid = false;
    $scope.using_admin_message = '';
    $scope.unUseDisable = true;

    $scope.useAdmin = function(){
        $scope.unUseDisable = false;
        $scope.install.adminPassword = '';
        $scope.install.adminEmail = '';
        $scope.passwordDisable = true;
        $scope.emailDisable = true;
        $scope.using_admin_message = 'Using the existing admin user: ' + $scope.useAdminOnGroup.username;
        $scope.install.useAdminOnGroup = $scope.useAdminOnGroup.user_id;
    }

    $scope.unUseAdmin = function(){
        $scope.unUseDisable = true;
        $scope.install.adminPassword = '';
        $scope.install.adminEmail = '';
        $scope.passwordDisable = false;
        $scope.emailDisable = false;
        $scope.using_admin_message = '';
        $scope.install.useAdminOnGroup = 0;
    }

    $scope.isTabValid = function(){

        if($scope.ActiveTab == 1){
            if($scope.validLicense == true)
                $scope.currentTabValid = true;
            else
                $scope.currentTabValid =  false;
        }
        else if($scope.ActiveTab == 2){
            if($scope.install.company_name == '' || typeof $scope.install.company_name == 'undefined' || 
                $scope.install.short_name  == '' || typeof $scope.install.short_name == 'undefined' || 
                $scope.install.address  == '' || typeof $scope.install.address == 'undefined' || 
                $scope.install.time_zone  == '' || typeof $scope.install.time_zone == 'undefined')
                $scope.currentTabValid =  false;
            else
                $scope.currentTabValid =  true;
        }
        alert($scope.currentTabValid)
    }

    $scope.classTabIcon = function(){
        for (var i = 1; i <= 10; i++) {
            if($scope.ActiveTab == i){
                angular.element( document.querySelector( '#li'+i ) ).removeClass("done");
                angular.element( document.querySelector( '#li'+i ) ).addClass("active");
            }
            else if($scope.ActiveTab > i){
                angular.element( document.querySelector( '#li'+i ) ).removeClass("active");
                angular.element( document.querySelector( '#li'+i ) ).addClass("done");
            }
            else if($scope.ActiveTab < i){
                angular.element( document.querySelector( '#li'+i ) ).removeClass("active");
                angular.element( document.querySelector( '#li'+i ) ).removeClass("done");
            }
        }
    }

    $scope.validateTab = function(tab) {

        var validFormMessage = '';

        if (tab == 2) {
            if($scope.install.company_name == '' || $scope.install.short_name == '' || $scope.install.address == '' ||
             $scope.install.time_zone == '')
                validFormMessage = 'You have to fill all required fields';
        }
        else if(tab == 8){

        }
        else if(tab == 9){
            if($scope.install.adminPasswod == '' &&  $scope.install.useAdminOnGroup == 0){
                validFormMessage = "Type the password for the admin user or use an existing admin user on the group";
            }
        }

        return validFormMessage;

    }

    $scope.next = function() {

        if($scope.ActiveTab > 1)
            $scope.previousDisabled = false;
        if($scope.ActiveTab == 10){
            $scope.nextDisabled = true;
            $scope.finishDisabled = false;
        }
        $scope.classTabIcon();
        //$scope.ActiveTab++;
        //angular.element(document.getElementById('step'+$scope.ActiveTab)).trigger('click');
    }
    $scope.previous = function() {
        if($scope.ActiveTab == 1)
            $scope.previousDisabled = true;
        if ($scope.ActiveTab < 10) {
            $scope.nextDisabled = false;
            $scope.finishDisabled = true;
        }
        $scope.classTabIcon();
        //$scope.ActiveTab--;
        //angular.element(document.getElementById('step'+$scope.ActiveTab)).trigger('click');
    }

    $scope.exit = function(){
        location.href = "/"+ROOT_FOLDER+"/login.php";
    }

    $scope.finish = function() {
        if(confirm("Are you sure you want to install this new Dealership?")){
            if($scope.install.low_limit_notify == true)
                $scope.install.low_limit_notify = '1';
            else
                $scope.install.low_limit_notify = '0';
            if($scope.install.new_purchase_notify == true)
                $scope.install.new_purchase_notify = '1';
            else
                $scope.install.new_purchase_notify = '0';
            if($scope.install.new_fill_notify == true)
                $scope.install.new_fill_notify = '1';
            else
                $scope.install.new_fill_notify = '0';



            if($scope.install.purchase_before_date == true)
                $scope.install.purchase_before_date = '1';
            else
                $scope.install.purchase_before_date = '0';
            if($scope.install.mandatory_odo == true)
                $scope.install.mandatory_odo = '1';
            else
                $scope.install.mandatory_odo = '0';

            if($scope.install.fill_car_model_tank_validate == true)
                $scope.install.fill_car_model_tank_validate = '1';
            else
                $scope.install.fill_car_model_tank_validate = '0';

            document.getElementById('loading_image2').style.visibility = 'visible';
            formData = $scope.install;

            $http.post("../includes/clases/controladores/AppController.php?accion=Action/Setting/Install_Dealership&output=json", JSON.stringify(formData))
            .success(function(response){
                alert(response.message);
                if(response.message == "Success"){
                    alert("Installation completed");
                    document.getElementById('loading_image2').style.visibility = 'hidden';
                    if(confirm("Do you want to start other installation?"))
                        location.reload();                    
                    else
                        location.href = "/"+ROOT_FOLDER+"/login.php";
                }
                document.getElementById('loading_image2').style.visibility = 'hidden';
            });
        }
    }

    $scope.dealership_group = "";
    $scope.validateLicense = function(){
        var myEl = angular.element( document.querySelector( '#license_div' ) );
        document.getElementById('loading_image').style.visibility = 'visible';
        $http.get("../includes/clases/controladores/AppController.php",{params:{
            accion:'Action/Setting/Validate_License',
            output:'json',
            license:$scope.install.license
        }})
        .then(function(response) {
                if(response.data.totalCount != 0){
                    angular.copy(response.data.matches, $scope.profileDealerGroup);
                    $scope.dealership_group = response.data.dealershipGroup[0].dealership_group_name;
                    $scope.install.dealership_group_id = response.data.dealershipGroup[0].dealership_group_id;
                    angular.copy(response.data.adminsOnGroup, $scope.install.adminsOnGroup);
                    angular.copy(response.data.adminsOnGroup2, $scope.install.adminsOnGroup2);
                    

                    $scope.nextDisabled = false;
                    $scope.validLicense = true;
                    myEl.removeClass('has-error'); 
                    myEl.addClass('has-success');  
                    document.getElementById('lincese_success_label').style.visibility = 'visible';
                    document.getElementById('lincese_error_label').style.visibility = 'hidden';
                }
                else{
                    $scope.dealership_group = "";
                    $scope.nextDisabled = true;
                    $scope.validLicense = false;
                    document.getElementById('lincese_error_label').style.visibility = 'visible';
                    document.getElementById('lincese_success_label').style.visibility = 'hidden';
                    myEl.removeClass('has-success'); 
                    myEl.addClass('has-error'); 
                }
                document.getElementById('loading_image').style.visibility = 'hidden';
        });
    }

    

    $scope.selectProfile = function(index){
        //console.log($filter('filter')($scope.profileAccess, {profile_id: 1}),$scope.profile.profiles)
        $scope.profileDetails = $filter('filter')($scope.install.profileAccess, {profile_id: $scope.profile.profiles.profile_id});
    }

    $scope.changeAccess = function(index){
        
        var changeLine = $scope.profileDetails[index].profile_menu_access_id;
        var count = $scope.install.profileAccess.length;

        //console.log(index,changeLine,$scope.profileDetails[index])

        $scope.profileDetails[index].allow = ($scope.profileDetails[index].allow == '1')?'0':'1'; 

        for (var i = 0; i < count; i++) {
            //console.log($scope.profileAccess[i].profile_menu_access_id , changeLine)
            if($scope.install.profileAccess[i].profile_menu_access_id == changeLine){
                //console.log($scope.profileAccess[i].allow)
                $scope.install.profileAccess[i].allow = $scope.profileDetails[index].allow;
                //console.log($scope.profileAccess[i].allow)
                break;
            }
        }
        /*
        $scope.auxProfiles = $scope.profileAccess;
        $scope.profileAccess = [];*/
        //angular.copy($scope.profileDetails, $scope.profileAccess);
        //$scope.profileDetails = $filter('filter')($scope.profileAccess, {profile_id: $scope.profile.profiles.profile_id});
    }

    $scope.addProfile = function(){
        //console.log($scope.menuoptions)
        //console.log($scope.profileAccess)
        //console.log($scope.profileDefaults)
        ///console.log($scope.profileDetails)

        $scope.install.profileDefaults.push({description:$scope.profile.description,profile_id:'NP'+$scope.newProfileId});

        for (var i = 0; i < $scope.menuoptions.length; i++) {
            var allowMenu = 0;
            for (var j = 0; j < $scope.profile.menu_options.length; j++) {
                if($scope.menuoptions[i].menu_id == $scope.profile.menu_options[j].menu_id){
                    allowMenu = 1;
                    break;
                }
            }
            $scope.install.profileAccess.push({
                    allow:allowMenu,
                    menu:$scope.menuoptions[i].menu,
                    menu_id:$scope.menuoptions[i].menu_id,
                    parent_menu:$scope.menuoptions[i].parent_menu,
                    parent_menu_id:$scope.menuoptions[i].parent_menu_id,
                    profile_id:'NP'+$scope.newProfileId,
                    profile_menu_access_id:0
            }); 
        }
        /*for (var i = 0; i < $scope.profileDealerGroup.length; i++) {
            var allowDealer = 0;
            for (var j = 0; j < $scope.profile.dealer_access.length; j++) {
                if($scope.profileDealerGroup[i].dealership_id == $scope.profile.dealer_access[j].dealership_id){
                    allowDealer = 1;
                    break;
                }
            }
            $scope.install.profileDealerAccess.push({
                    allow:allowDealer,
                    dealership_id:$scope.profileDealerGroup[i].dealership_id,
                    profile_id:'NP'+$scope.newProfileId,
            }); 
        }*/
        $scope.newProfileId++;        
    }

    $http.get("../includes/clases/controladores/AppController.php",{params:{
            accion:'Action/Setting/Get_Menu_Options',
            output:'json'
    }})
    .then(function(response) {
            angular.copy(response.data.matches, $scope.menuoptions);
    });

    $http.get("../includes/clases/controladores/AppController.php",{params:{
            accion:'Action/Setting/Get_Default_Profiles',
            output:'json'
    }})
    .then(function(response) {
            angular.copy(response.data.matchesProfiles, $scope.install.profileDefaults);
            angular.copy(response.data.matchesProfilesDetails, $scope.install.profileAccess);
    });
    
});