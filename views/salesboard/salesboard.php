        <section class="content-header">
          <div class="text-muted well well-sm no-shadow" style="height: 70px;">
                <input type="text" ng-readonly="true" ng-model="datetoshow" class="pull-right" id="reservationtime" style="font-size: 60px;width:270px;height: 50px; font-weight: bolder;font-family: arial;">
                <img class="pull-right" ng-click="sendMessage('SMS',0)" style="cursor: pointer;" src="includes/images/sms_01.png">
                <img class="pull-right" ng-click="sendMessage('MAIL',0)" style="cursor: pointer;" src="includes/images/mail_new_01.png">
          </div>  
          <div class="text-muted well well-sm no-shadow">
          <p style="margin-top: 5px;">
              <span style="font-size: 4em;font-weight: bolder;font-family: arial;color: white">
                Up-Board
              </span>
              <span style="font-size: 4em;font-weight: bolder;font-family: arial;color: yellow">
                {{next_up_name}}
              </span>    <!-- ng-value="{{ clock | date:'HH:mm:ss'}}" -->
          </p>
          <!--div class="form-group pull-right"-->
              <!--div class="input-group">
                <div class="input-group-addon">
                  <i class="fa fa-clock-o"></i>
                </div-->
                
              <!--/div-->
          <!--/div-->
          </div>  
        </section>

        <!-- Main content -->
        <section class="content">
        <div class="row">
            <div class="col-md-12">
              <div class="box box-primary">
                <div class="box-header with-border" style="font-size:30px;font-weight: bolder;font-family: arial ">
                  <!--  class="form-control select2" -->
                  <select ng-disabled="!isGreeterRO" ng-disabled="disableAddSalesmen && isGreeter " ng-init="upboard.salesman = salesmen[0]" ng-model="upboard.salesman" ng-change="onSelectSalesman()" ng-options="salesman.name for salesman in salesmen">
                  </select>
                  <div class="box-tools pull-right">
                    <div class="has-feedback">
                      <!--input type="text" class="form-control input-sm" placeholder="Search Mail"-->

                    </div>
                  </div><!-- /.box-tools -->
                </div><!-- /.box-header -->
                <div class="box-body no-padding">
                </div><!-- /.box-body -->
                <div class="box-footer no-padding">
                  
                </div>
              </div><!-- /. box -->
            </div><!-- /.col -->
          </div>

          <div class="row">
            <div class="col-md-6">
              <div class="box box-primary ">
                <div class="box-header with-border">
                  <h3 class="box-title"></h3>
                  <div class="box-tools pull-right">
                    <div class="has-feedback">
                      <!--input type="text" class="form-control input-sm" placeholder="Search Mail"-->
                      <button ng-disabled="!isGreeterRO" ng-disabled="disableChangeStatus && !isGreeter" class="btn btn-primary" ng-click="reset()" style="font-size:16px;font-weight: bolder;font-family: arial ">Reset</button>
                      <button ng-disabled="disableChangeStatus && isGreeter" class="btn btn-primary" ng-click="next()" style="font-size:16px;font-weight: bolder;font-family: arial ">Next</button>
                    </div>
                  </div><!-- /.box-tools -->
                </div><!-- /.box-header -->
                <div class="box-body no-padding">
                  <div class="mailbox-controls">
                    <!-- Check all button -->
                    <!--button class="btn btn-default btn-sm checkbox-toggle"><i class="fa fa-square-o"></i></button>
                    <div class="btn-group">
                      <button class="btn btn-default btn-sm"><i class="fa fa-trash-o"></i></button>
                      <button class="btn btn-default btn-sm"><i class="fa fa-reply"></i></button>
                      <button class="btn btn-default btn-sm"><i class="fa fa-share"></i></button>
                    </div--><!-- /.btn-group -->
                    <div class="pull-right">
                      <!--1-50/200 -->
                      <div class="btn-group">
                        
                      </div><!-- /.btn-group -->
                    </div><!-- /.pull-right -->
                  </div>
                  <div class="table-responsive mailbox-messages" style="height: 600px;overflow-y: scroll">
                    <table class="table table-hover table-striped">
                      <tbody>
                        <tr ng-repeat="availableBoardSalesman in availableBoardSalesmen" style="font-size: 30px">
                          <!--td><input type="checkbox"></td-->
                          <!--td class="mailbox-star"><a href="#"><i class="fa fa-star text-yellow"></i></a></td-->
                          <td class="mailbox-name">{{availableBoardSalesman.board_order}}</td>
                          <td class="mailbox-date">{{availableBoardSalesman.name}}</td>
                          <td class="mailbox-date">
                            <select ng-disabled="((disableChangeStatus) || (!isGreeterRO))" ng-init="status=1" ng-model="status" ng-change="changeStatus({{$index}},status,'up');" 
                             ng-click="oldValue = status">
                              <option ng-repeat="status in statusArray" value="{{status.status_id}}">
                                  {{status.name}}
                              </option>
                            </select>
                          </td>
                          <td class="mailbox-attachment">
                            <img  ng-click="sendMessage('MAIL',$index)" style="cursor: pointer;" src="includes/images/mail_new_01.png">
                            <img ng-click="sendMessage('SMS',$index)" style="cursor: pointer;" src="includes/images/sms_01.png">
                          </td>
                        </tr>
                      </tbody>
                    </table><!-- /.table -->
                  </div><!-- /.mail-box-messages -->
                </div><!-- /.box-body -->
                <div id="prossesiing-available-list" style="visibility: hidden;" class="overlay">
                  <i class="fa fa-refresh fa-spin"></i>
                </div>
                <div class="box-footer no-padding">
                  <div class="mailbox-controls">
                    <!-- Check all button -->
                    <!--button class="btn btn-default btn-sm checkbox-toggle"><i class="fa fa-square-o"></i></button>
                    <div class="btn-group">
                      <button class="btn btn-default btn-sm"><i class="fa fa-trash-o"></i></button>
                      <button class="btn btn-default btn-sm"><i class="fa fa-reply"></i></button>
                      <button class="btn btn-default btn-sm"><i class="fa fa-share"></i></button>
                    </div--><!-- /.btn-group -->
                    <div class="pull-right">
                      
                    </div><!-- /.pull-right -->
                  </div>
                </div>
              </div><!-- /. box -->
            </div><!-- /.col -->
              
            <div class="col-md-6">
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title"></h3>
                  <div class="box-tools pull-right">
                    <div class="has-feedback">
                      <!--input type="text" class="form-control input-sm" placeholder="Search Mail"-->
                    </div>
                  </div><!-- /.box-tools -->
                </div><!-- /.box-header -->
                <div class="box-body no-padding">
                  <div class="mailbox-controls">
                    <!-- Check all button -->
                    <!--button class="btn btn-default btn-sm checkbox-toggle"><i class="fa fa-square-o"></i></button>
                    <div class="btn-group">
                      <button class="btn btn-default btn-sm"><i class="fa fa-trash-o"></i></button>
                      <button class="btn btn-default btn-sm"><i class="fa fa-reply"></i></button>
                      <button class="btn btn-default btn-sm"><i class="fa fa-share"></i></button>
                    </div--><!-- /.btn-group -->
                    <div class="pull-right">
                      <!--1-50/200 -->
                      <div class="btn-group">
                        
                      </div><!-- /.btn-group -->
                    </div><!-- /.pull-right -->
                  </div>
                  <div class="table-responsive mailbox-messages"   style="height: 600px;overflow-y: scroll">
                    <table class="table table-hover table-striped">
                      <tbody>
                       <!-- ng-class="{'bg-yellow color-palette':actionBoardSalesman.status=='Off'}"-->
                        <tr ng-repeat="actionBoardSalesman in actionBoardSalesmen" style="font-size: 30px;" >
                          <!--td><input type="checkbox"></td-->
                          <!--td class="mailbox-star"><a href="#"><i class="fa fa-star text-yellow"></i></a></td-->
                          <td ng-if="actionBoardSalesman.status == 'Off'" style="background-color: #f39c12" ></td> <!-- $index+1-->
                          <td ng-if="actionBoardSalesman.status != 'Off'">{{actionBoardSalesman.order_showed}}</td> <!-- $index+1-->
                          <td ng-if="actionBoardSalesman.status == 'Off'" style="background-color: #f39c12">{{actionBoardSalesman.name}}</td>
                          <td ng-if="actionBoardSalesman.status != 'Off'">{{actionBoardSalesman.name}}</td>
                          <td ng-if="actionBoardSalesman.status == 'Off'" style="background-color: #f39c12">{{actionBoardSalesman.status}}</td>
                          <td ng-if="actionBoardSalesman.status != 'Off'">{{actionBoardSalesman.status}}</td>
                          <td ng-if="actionBoardSalesman.status == 'Off'" style="background-color: #f39c12"></td>
                          <td ng-if="actionBoardSalesman.status != 'Off'">{{actionBoardSalesman.datetoshow}}</td>
                          
                          <td ng-if="actionBoardSalesman.status != 'Off' && actionBoardSalesman.status != 'Selling'">
                            <button  ng-disabled="!isGreeterRO"  ng-disabled="disableChangeStatus && !isGreeter" class="btn btn-primary" style="font-size:16px;font-weight: bolder;font-family: arial " ng-click="changeStatus($index,1,'back')">Back</button>
                          </td>
                          <td  ng-if="actionBoardSalesman.status == 'Off'"  style="background-color: #f39c12" >
                              <button  ng-if="isGreeterRO" ng-disabled="disableChangeStatus && !isGreeter" class="btn btn-primary" style="font-size:16px;font-weight: bolder;font-family: arial " ng-click="changeStatus($index,1,'back')">Back</button>
                          </td>
                          <td  ng-if="actionBoardSalesman.status == 'Selling'" >                              
                              <div class="btn-group">
                                <button  ng-disabled="!isGreeterRO" type="button" ng-disabled="disableChangeStatus && !isGreeter" class="btn btn-primary"  style="font-size:16px;font-weight: bolder;font-family: arial ">Actions</button>
                                <button  ng-disabled="!isGreeterRO" type="button" ng-disabled="disableChangeStatus && !isGreeter" class="btn btn-primary dropdown-toggle" data-toggle="dropdown"  style="font-size:16px;font-weight: bolder;font-family: arial ">
                                  <span class="caret"></span>
                                  <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <ul class="dropdown-menu" role="menu">
                                  <li  ng-disabled="!isGreeterRO" style="font-size:16px;font-weight: bolder;font-family: arial "><a ng-click="changeStatus($index,1,'back')" >Back</a></li>
                                  <li class="divider"></li>
                                  <li  ng-disabled="!isGreeterRO" style="font-size:16px;font-weight: bolder;font-family: arial "><a ng-click="changeStatus($index,1,'hot')">Hot!</a></li>
                                </ul>
                            </div>
                          </td>
                        </tr>
                      </tbody>
                    </table><!-- /.table -->
                  </div><!-- /.mail-box-messages -->
                </div><!-- /.box-body -->
                <div id="prossesiing-action-list" style="visibility: hidden;" class="overlay">
                  <i class="fa fa-refresh fa-spin"></i>
                </div>  
                <div class="box-footer no-padding">
                  <div class="mailbox-controls">
                    <!-- Check all button -->
                    <!--button class="btn btn-default btn-sm checkbox-toggle"><i class="fa fa-square-o"></i></button>
                    <div class="btn-group">
                      <button class="btn btn-default btn-sm"><i class="fa fa-trash-o"></i></button>
                      <button class="btn btn-default btn-sm"><i class="fa fa-reply"></i></button>
                      <button class="btn btn-default btn-sm"><i class="fa fa-share"></i></button>
                    </div--><!-- /.btn-group -->
                    <div class="pull-right">
                      
                    </div><!-- /.pull-right -->
                  </div>
                </div>
              </div><!-- /. box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->