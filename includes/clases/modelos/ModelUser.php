<?php
if(!isset($_SESSION)){session_start();} 
require_once $MAIN_DIR.'includes/clases/BDController.php';
require_once $MAIN_DIR.'includes/clases/Notifier.php';

class ModelUser {
	
	private $bd = null;
	
	public function getLogin($request){
		$this->BDController = new BDController();
		try{
			$this->BDController->conectar();
			//$request = json_decode(file_get_contents('php://input'), true);
			$query = '';
			
			{
				//$query = "update user set password='".md5("Salesboard123")."' where user_id not in (1)";
				//		$this->BDController->setQuery($query);
				//		$result = $this->BDController->ejecutaInstruccion();

				$query = "SELECT u.dealership_id,u.user_id,u.name,u.username,u.password   , 
				          coalesce(p.create_new,0) create_new,coalesce(p.modify,0) modify,coalesce(p.remove,0) remove, coalesce(p.description) profile, 
				          d.time_zone ,d.session_timeout,u.photo    
                    FROM user u    
                    left join profile p on p.profile_id = u.profile_id 
                    left join dealership d on d.dealership_id = u.dealership_id 
					WHERE  u.username = '".$request['username']."' and u.password = '".md5($request['password'])."'";
				//echo $query;
				$this->BDController->setQuery($query);
				$result = $this->BDController->ejecutaInstruccion();
				$num = $this->BDController->numero_filas($result);
				$matches = Array();
				while ($item = $this->BDController->fetch($result)){
					$matches[] = $item;
				}
				$log = 0;
				if(isset($matches[0]['user_id'])){//if($matches[0]['user_id'] != ''){
					$_SESSION['dealership_id'] = $matches[0]['dealership_id'];
					$_SESSION['name'] = $matches[0]['name'];
				    $_SESSION['username'] = $matches[0]['username'];
				    $_SESSION['profile'] = $matches[0]['profile'];
				    $_SESSION['photo'] = ($matches[0]['photo']=='')?'no-avatar.jpg':$matches[0]['photo'];
				    $_SESSION['PHP_TIMEZONE'] = $matches[0]['time_zone'];
				    date_default_timezone_set($_SESSION['PHP_TIMEZONE']);
				    $now = new DateTime();
				    $mins = $now->getOffset() / 60;
				    $sgn = ($mins < 0 ? -1 : 1);
				    $mins = abs($mins);
				    $hrs = floor($mins / 60);
				    $mins -= $hrs * 60;
				    $offset = sprintf('%+d:%02d', $hrs*$sgn, $mins);

				    $_SESSION['MYSQL_TIMEZONE'] = $offset;
				    //$_SESSION['user_type_id'] = $matches[0]['user_type_id'];
				    //$_SESSION['dashboard'] = $matches[0]['dashboard'];
				    //$_SESSION['foto'] = "includes/images/avatars/".(($matches[0]['picture']!="")?$matches[0]['picture']:'no-avatar.jpg');
				    //$_SESSION['foto'] = "includes/images/uploads/log_login/login_user_".$matches[0]['user_id']."_log_".$log.".png";
				    $_SESSION['user_id'] = $matches[0]['user_id'];
				    $_SESSION['password_login'] = 1;
				    $_SESSION['firts_load'] = 1;
				    $_SESSION['can_add'] = $matches[0]['create_new'];
				    $_SESSION['can_update'] = $matches[0]['modify'];
				    $_SESSION['can_remove'] = $matches[0]['remove'];
				    $_SESSION['session_timeout'] = time() + $matches[0]['session_timeout'];
				    $_SESSION['session_timeout_limit'] = $matches[0]['session_timeout'];


					//vERIFICA si es el primer login del dia. si lo es, entonces se resetea el salesboard
					$query = "SELECT * 
	                    FROM log_login  g 
	                    inner join user u on u.user_id = g.user_id 
	                    WHERE  u.dealership_id = '".$_SESSION['dealership_id']."' 
	                     and year(log_date)=year(curdate()) and month(log_date)=month(curdate()) and 
	                     day(log_date)=day(curdate())";
				
					$this->BDController->setQuery($query);
					$result = $this->BDController->ejecutaInstruccion();
					$item = $this->BDController->fetch($result);

					if($item['log_date']==''){
						$query = "delete from salesboard where dealership_id = '".$_SESSION['dealership_id']."' ";
						$this->BDController->setQuery($query);
						$result = $this->BDController->ejecutaInstruccion();
					}

					$query = "insert into log_login (user_id,log_date) values ('".$matches[0]['user_id']."',concat(curdate(),' ',curtime()))";
					$this->BDController->setQuery($query);
					$result = $this->BDController->ejecutaInstruccion();
					$log = $this->BDController->lastId();
					$_SESSION['session_log_id'] = $log;
					
				}
				else{
					return  Array('success' => true,'log' => -1);
				}
				
				$this->BDController->commit();
				$this->BDController->desconectar();;

				return  Array('success' => true,'totalCount' => $num,'matches' => $matches,'user_id' => $matches[0]['user_id'],'log' => $log);
			}
			
		} 
		catch(Exception $e ) {
			$this->BDController->rollback();
			$this->BDController->desconectar();
			$arrayError = Array('Error No:' => $e->getCode(),'Error Message:' => $e->getMessage(),'Stack Trace:' => nl2br($e->getTraceAsString()));
			//$arrayError = 'Error No: ' . $e->getCode().' Error Message: ' . $e->getMessage().' Stack Trace: ' . nl2br($e->getTraceAsString());
			return  Array('success' => false,'mensaje' => "Error durante la operacion",'error' => $arrayError);
		}
		
	}


	public function save($request){
		$this->BDController = new BDController();
		try{
			$this->BDController->conectar();
			$this->BDController->autocommit(FALSE);
			//$request = json_decode(file_get_contents('php://input'), true);
			$query = '';
			if(!isset($request['user_id'])){//if(!isset($request['update'])){
						$query = "insert into user (dealership_id,name,username,password,phone,profile_id,notification_carrier_id) values ('".$_SESSION['dealership_id']."','".$request['name']."',
						'".$request['username']."','".md5($request['password'])."','".$request['phone']."','".$request['profile']['profile_id']."','".$request['notification_carrier_id']."')";
						$this->BDController->setQuery($query);
						$result = $this->BDController->ejecutaInstruccion();
						$userId = $this->BDController->lastId();
						$parent_menu_id = "---";
						foreach ($request['profileAccess'] as $key => $profileAccesMenu) {
								if($request['profile']['profile_id'] == $profileAccesMenu['profile_id']){
									if($profileAccesMenu['parent_menu_id'] != $parent_menu_id){
										$parent_menu_id = $profileAccesMenu['parent_menu_id'];

										$saveQuery = "INSERT INTO user_menu_access (user_id,menu_id,allow) 
													VALUES ('".$userId."','".$profileAccesMenu['parent_menu_id']."',1);";
										$this->BDController->setQuery($saveQuery);
										$this->BDController->ejecutaInstruccion();
									}
									$saveQuery = "INSERT INTO user_menu_access (user_id,menu_id,allow) 
													VALUES ('".$userId."','".$profileAccesMenu['menu_id']."','".$profileAccesMenu['allow']."');";
									$this->BDController->setQuery($saveQuery);
									$this->BDController->ejecutaInstruccion();
								}
						}
						//print_r($request['dealerships']);
						$firstDealerId = '';
						$i=0;
						foreach ($request['dealerships'] as $key => $dealershipAccess) {
							if($i==0)
								$firstDealerId = $dealershipAccess['dealership_id'];
							$saveQuery = "INSERT INTO user_dealership_access (user_id,dealership_id,allow) 
													VALUES ('".$userId."','".$dealershipAccess['dealership_id']."','".$dealershipAccess['allow']."');";
							$this->BDController->setQuery($saveQuery);
							$this->BDController->ejecutaInstruccion();
							$i++;
						}	

						if($firstDealerId!=''){
								$query = "update user set dealership_id = '".$firstDealerId."' 
								 where user_id ='".$userId."' ";
							$this->BDController->setQuery($query);
							$result = $this->BDController->ejecutaInstruccion();
						}
			}
			else{
						$query = "update user set dealership_id='".$request['dealership_id']."',profile_id='".$request['profile_id']."',username='".$request['username']."',name='".$request['name']."',username='".$request['username']."',phone='".$request['phone']."',notification_carrier_id='".$request['notification_carrier_id']."'  
						  where user_id = '".$request['user_id']."' ";
						$this->BDController->setQuery($query);
						$result = $this->BDController->ejecutaInstruccion();
						$userId = $request['user_id'];

						foreach ($request['dealerAccessDetails'] as $key => $dealershipAccess) {
							if($dealershipAccess['user_dealership_access_id'] == 0){
								$saveQuery = "INSERT INTO user_dealership_access (user_id,dealership_id,allow) 
													VALUES ('".$userId."','".$dealershipAccess['dealership_id']."','".$dealershipAccess['allow']."');";
								$this->BDController->setQuery($saveQuery);
								$this->BDController->ejecutaInstruccion();
							}
							else{
								$saveQuery = "update user_dealership_access set allow = '".$dealershipAccess['allow']."' 
								 where user_dealership_access_id = '".$dealershipAccess['user_dealership_access_id']."'";
								$this->BDController->setQuery($saveQuery);
								$this->BDController->ejecutaInstruccion();	
							}	
						}

						foreach ($request['profileDetails'] as $key => $menuAccess) {
							$saveQuery = "update user_menu_access set allow = '".$menuAccess['allow']."' 
								 where user_id = '".$request['user_id']."' and 
								 menu_id = '".$menuAccess['menu_id']."'";
							$this->BDController->setQuery($saveQuery);
							$this->BDController->ejecutaInstruccion();	
								
						}
			}
						

				$this->BDController->commit();
				$this->BDController->desconectar();;

				return  Array('success' => true,'message' => 'Success','user_id' => $userId);	
			
			
		} 
		catch(Exception $e ) {
			$this->BDController->rollback();
			$this->BDController->desconectar();
			$arrayError = Array('Error No:' => $e->getCode(),'Error Message:' => $e->getMessage(),'Stack Trace:' => nl2br($e->getTraceAsString()));
			//$arrayError = 'Error No: ' . $e->getCode().' Error Message: ' . $e->getMessage().' Stack Trace: ' . nl2br($e->getTraceAsString());
			return  Array('success' => false,'message' => "Error saving user data",'error' => $arrayError);
		}
		
	}

	public function delete($request){
		$this->BDController = new BDController();
		try{
			$this->BDController->conectar();
			$this->BDController->autocommit(FALSE);
			$saveQuery = '';
			$requestOriginal = $request;
			
			
				$saveQuery = "delete from user where user_id = '".$request['user_id']."'";

			$this->BDController->setQuery($saveQuery);
			$this->BDController->ejecutaInstruccion();	
			
			$this->BDController->commit();
			
			
			return  Array('success' => true,"message" => "Success. User deleted.");
		} 
		catch(Exception $e ) {
			$this->BDController->rollback();
			$this->BDController->desconectar();
			$arrayError = Array('Error No:' => $e->getCode(),'Error Message:' => $e->getMessage(),'Stack Trace:' => nl2br($e->getTraceAsString()));
			//$arrayError = 'Error No: ' . $e->getCode().' Error Message: ' . $e->getMessage().' Stack Trace: ' . nl2br($e->getTraceAsString());
			return  Array('success' => true,'message' => 'Error during the operation. Please check if this Salesman is not in board before delete','error' => $arrayError);//$e->getMessage()
		}
		
	}

	public function saveMyProfile($request,$files){
		$this->BDController = new BDController();
		try{
			$this->BDController->conectar();
			$this->BDController->autocommit(FALSE);
			//$request = json_decode(file_get_contents('php://input'), true);
			$setPhoto = "";
			/*if(isset($files['photo'])){    
			    $errors= array();        
			    $file_name = $files['photo']['name'];
			    $file_size =$files['photo']['size'];
			    $file_tmp =$files['photo']['tmp_name'];
			    $file_type=$files['photo']['type'];   
			    $file_ext = strtolower(pathinfo($file_name, PATHINFO_EXTENSION));
			    $extensions = array("jpeg","jpg","png");        
			    if(in_array($file_ext,$extensions )=== false){
			         $errors[]="image extension not allowed, please choose a JPEG or PNG file.";
			    }
			    if($file_size > 2097152){
			        $errors[]='File size cannot exceed 2 MB';
			    }               
			    if(empty($errors)==true){
			        move_uploaded_file($file_tmp,"includes/images/avatar/".$file_name);
			    }else{
			    	throw new Exception($errors);
			    }
			    $setPhoto = ",photo = '".$file_name."'";
			}*/
			$query = '';
			$pass = '';
			if(isset($request['new_password']) && !empty($request['new_password'])){
				$pass = ", password='".md5($request['new_password'])."' ";
			}
			$query = "update user set username='".$request['username']."',phone='".$request['phone']."' ".$pass." ".$setPhoto."     
						  where user_id = '".$request['user_id']."' ";
						$this->BDController->setQuery($query);
			$result = $this->BDController->ejecutaInstruccion();
			$user_id = $request['user_id'];
			
						

			$this->BDController->commit();
			$this->BDController->desconectar();;

			return  Array('success' => true,'message' => 'Success','user_id' => $user_id);	
			
			
		} 
		catch(Exception $e ) {
			$this->BDController->rollback();
			$this->BDController->desconectar();
			$arrayError = Array('Error No:' => $e->getCode(),'Error Message:' => $e->getMessage(),'Stack Trace:' => nl2br($e->getTraceAsString()));
			//$arrayError = 'Error No: ' . $e->getCode().' Error Message: ' . $e->getMessage().' Stack Trace: ' . nl2br($e->getTraceAsString());
			return  Array('success' => false,'message' => "Error saving user data",'error' => $arrayError);
		}
		
	}

	public function getUserList($request){
		$this->BDController = new BDController();
		try{
			$this->BDController->conectar();
			
			$query = '';
			$inicio = 0;
			$fin = 0;
			$limit = '';
			$num = 0;
			$andUserId = "";
			
			if (isset($request['orden']) && $request['orden'] != "") {
				$orden = $request['orden']." ".$request['dir'];
			}
			else{
				$orden = " name asc";
			}

			if(isset($request['user_id_messages']) && !empty($request['user_id_messages'])){
				$andUserId = " and u.user_id != '".$request['user_id_messages']."'";
			}
			
			$tamanoPagina = 30;
			$num0 =0;
			$andDealership = "";
			if(isset($_SESSION['dealership_id']) && $_SESSION['dealership_id'] != '' && $_SESSION['user_id'] != 1){
				$andDealership = " and (u.dealership_id = '".$_SESSION['dealership_id']."' or 
				u.user_id in (select user_id from user_dealership_access where dealership_id = '".$_SESSION['dealership_id']."'))";
			}

			if (isset($request['pagina'])) {
				$inicio = ($request['pagina'] - 1 )  * $tamanoPagina;
				$query = "SELECT *     
                    FROM user  
                    WHERE  1 ".$andDealership;
				
				$this->BDController->setQuery($query);
				$result = $this->BDController->ejecutaInstruccion();
				$num0 = $this->BDController->numero_filas($result);
				$limit = " limit ".$inicio.",".$tamanoPagina;
			}
			
			

			

			$query = "SELECT u.*,t.description profile,  ds.dealerships
                    FROM user u
                    inner join (SELECT uda.user_id,
								 GROUP_CONCAT(DISTINCT d.company_name 
								 ORDER BY d.company_name ASC SEPARATOR ',') dealerships 
								 FROM user_dealership_access uda 
								 inner join dealership d on d.dealership_id = uda.dealership_id
								   where uda.allow = 1  
								   GROUP BY uda.user_id) ds on ds.user_id = u.user_id    
                    left join profile t on t.profile_id = u.profile_id  
                    WHERE  1  ".$andUserId." ".$andDealership." 
                    ORDER BY ".$orden." ".$limit;
            //echo $query;
			$this->BDController->setQuery($query);
			$result = $this->BDController->ejecutaInstruccion();
			$num2 = $this->BDController->numero_filas($result);
			$matches = Array();
			while ($item = $this->BDController->fetch($result)){
				$matches[] = $item;
			}

			$query = "SELECT coalesce(p.create_new,0) create_new,coalesce(p.modify,0) modify,coalesce(p.remove,0) remove 
                    FROM user u    
                    left join profile p on p.profile_id = u.profile_id 
					WHERE  u.user_id = '".$_SESSION['user_id']."'";
			$this->BDController->setQuery($query);
			$result = $this->BDController->ejecutaInstruccion();
			$num = $this->BDController->numero_filas($result);
			$item = $this->BDController->fetch($result);//'create_new' => $item['create_new'],'modify' => $item['modify'],'remove' => $item['remove']

			$this->BDController->desconectar();

			if(isset($request['typeahead'])){
				return  $matches;
			}

			return  Array('success' => true,'totalCount' => $num,'totalCount2' => $num2,'totalCount3' => $num0,'matches' => $matches,'inicio' => $inicio,'fin' => $fin,
				'create_new' => $item['create_new'],'modify' => $item['modify'],'remove' => $item['remove']);
		} 			
		catch(Exception $e ) {
			$this->BDController->rollback();
			$this->BDController->desconectar();
			$arrayError = Array('Error No:' => $e->getCode(),'Error Message:' => $e->getMessage(),'Stack Trace:' => nl2br($e->getTraceAsString()));
			//$arrayError = 'Error No: ' . $e->getCode().' Error Message: ' . $e->getMessage().' Stack Trace: ' . nl2br($e->getTraceAsString());
			return  Array('success' => false,'mensaje' => "Error durante la operacion",'error' => $arrayError);
		}
		
	}

	public function getUser($request){
		$this->BDController = new BDController();
		try{
			$this->BDController->conectar();
			
			$query = '';
			
			$query = "SELECT u.* 
                    FROM user u    
                    WHERE  u.user_id = '".$request['user_id']."'";
			
			$this->BDController->setQuery($query);
			$result = $this->BDController->ejecutaInstruccion();
			$matches = Array();
			while ($item = $this->BDController->fetch($result)){
				$matches[] = $item;
				//$matches2[] = $item;//array('nombre' => $item['nombre']);
			}

			$query = "SELECT p.* 
                    FROM profile p 
                    inner join user u on u.profile_id = p.profile_id 
                    WHERE  u.user_id = '".$request['user_id']."'";
			
			$this->BDController->setQuery($query);
			$result = $this->BDController->ejecutaInstruccion();
			$matchProfile = Array();
			while ($item = $this->BDController->fetch($result)){
				$matchProfile[] = $item;
				//$matches2[] = $item;//array('nombre' => $item['nombre']);
			}

			/*$query = "SELECT p.* 
                    FROM user_dealership_access p 
                    inner join user u on u.user_id = p.user_id 
                    WHERE  u.user_id = '".$request['user_id']."' and p.allow = 1";
			
			$this->BDController->setQuery($query);
			$result = $this->BDController->ejecutaInstruccion();
			$matchDealerAccess = Array();
			while ($item = $this->BDController->fetch($result)){
				$matchDealerAccess[] = $item;
				//$matches2[] = $item;//array('nombre' => $item['nombre']);
			}*/

			$query = "SELECT p.user_dealership_access_id,p.user_id,p.dealership_id,d.company_name  ,p.allow 
			 		from dealership d 
                    inner join user_dealership_access p on p.dealership_id = d.dealership_id 
                    WHERE  p.user_id = '".$request['user_id']."'";
			
			$this->BDController->setQuery($query);
			$result = $this->BDController->ejecutaInstruccion();
			$matchDealerAccess = Array();
			while ($item = $this->BDController->fetch($result)){
				$matchDealerAccess[] = $item;
				//$matches2[] = $item;//array('nombre' => $item['nombre']);
			}

			$query = "SELECT m.parent_menu_id,m.menu_id,p.menu_order, p.description parent_menu, m.description menu,
			 			uma.allow 
						FROM user_menu_access uma 
						inner join menu m on uma.menu_id = m.menu_id 
						INNER JOIN menu p ON p.menu_id = m.parent_menu_id 
						WHERE uma.user_id = '".$request['user_id']."' 
						ORDER BY p.menu_order,m.menu_order ";
			
			$this->BDController->setQuery($query);
			$result = $this->BDController->ejecutaInstruccion();
			$matchesMenuAccess = Array();
			while ($item = $this->BDController->fetch($result)){
				$matchesMenuAccess[] = $item;
				//$matches2[] = $item;//array('nombre' => $item['nombre']);
			}

			$query = "SELECT coalesce(p.create_new,0) create_new,coalesce(p.modify,0) modify,coalesce(p.remove,0) remove 
                    FROM user u    
                    left join profile p on p.profile_id = u.profile_id 
					WHERE  u.user_id = '".$_SESSION['user_id']."'";
			$this->BDController->setQuery($query);
			$result = $this->BDController->ejecutaInstruccion();
			$num = $this->BDController->numero_filas($result);
			$item = $this->BDController->fetch($result);//'create_new' => $item['create_new'],'modify' => $item['modify'],'remove' => $item['remove']

			$this->BDController->desconectar();

			return  Array('success' => true,'matches' => $matches,'matchProfile' => $matchProfile,'matchesMenuAccess' => $matchesMenuAccess,'matchDealerAccess' => $matchDealerAccess,'create_new' => $item['create_new'],'modify' => $item['modify'],'remove' => $item['remove']);
		} 			
		catch(Exception $e ) {
			$this->BDController->rollback();
			$this->BDController->desconectar();
			$arrayError = Array('Error No:' => $e->getCode(),'Error Message:' => $e->getMessage(),'Stack Trace:' => nl2br($e->getTraceAsString()));
			//$arrayError = 'Error No: ' . $e->getCode().' Error Message: ' . $e->getMessage().' Stack Trace: ' . nl2br($e->getTraceAsString());
			return  Array('success' => false,'mensaje' => "Error durante la operacion",'error' => $arrayError);
		}
		
	}

	public function getUserTypeListBox($request){
		$this->BDController = new BDController();
		try{
			$this->BDController->conectar();
			
			$andDealership = "";
			if(isset($_SESSION['dealership_id']) && $_SESSION['dealership_id'] != ''){
				$andDealership = " and u.dealership_id = '".$_SESSION['dealership_id']."'";
			}

			$query = "SELECT u.* 
                    FROM user_type u   
                    WHERE  1  ".$andDealership."
                    ORDER BY description asc";
			
			$this->BDController->setQuery($query);
			$result = $this->BDController->ejecutaInstruccion();
			$matches = Array();
			while ($item = $this->BDController->fetch($result)){
				$matches[] = $item;
				//$matches2[] = $item;//array('nombre' => $item['nombre']);
			}
			$this->BDController->desconectar();

			return  Array('success' => true,'matches' => $matches);
		} 			
		catch(Exception $e ) {
			$this->BDController->rollback();
			$this->BDController->desconectar();
			$arrayError = Array('Error No:' => $e->getCode(),'Error Message:' => $e->getMessage(),'Stack Trace:' => nl2br($e->getTraceAsString()));
			//$arrayError = 'Error No: ' . $e->getCode().' Error Message: ' . $e->getMessage().' Stack Trace: ' . nl2br($e->getTraceAsString());
			return  Array('success' => false,'mensaje' => "Error durante la operacion",'error' => $arrayError);
		}
		
	}

	public function getUserMessages($request){
		$this->BDController = new BDController();
		try{
			$this->BDController->conectar();
			
			$and = "";
			if(isset($request['message_read']) && $request['message_read'] == 1){
				$and = " and n.message_read = 1 ";
			}
			elseif(isset($request['message_read']) && $request['message_read'] == 0){
				$and = " and n.message_read = 0 ";
			}
			elseif(isset($request['message_read']) && $request['message_read'] == -1){
				$and = "  ";	
			}

			$query = "SELECT uf.name,'' picture,n.*,date_format(n.date,'%m/%d/%Y') date2 ,date_format(n.time,'%h:%i') time2,
			        SUBSTRING(message,1,15) part_message 
                    FROM notification n 
                    inner join user uf on uf.user_id = n.from_user_id   
                    WHERE  n.to_user_id = '".$_SESSION['user_id']."'   ".$and." 
                    ORDER BY date desc,time desc";
			
			$this->BDController->setQuery($query);
			$result = $this->BDController->ejecutaInstruccion();
			$num = $this->BDController->numero_filas($result);
			$matches = Array();
			while ($item = $this->BDController->fetch($result)){
				$matches[] = $item;
				//$matches2[] = $item;//array('nombre' => $item['nombre']);
			}
			$this->BDController->desconectar();

			return  Array('success' => true,'matches' => $matches,'totalCount' => $num);
		} 			
		catch(Exception $e ) {
			$this->BDController->rollback();
			$this->BDController->desconectar();
			$arrayError = Array('Error No:' => $e->getCode(),'Error Message:' => $e->getMessage(),'Stack Trace:' => nl2br($e->getTraceAsString()));
			//$arrayError = 'Error No: ' . $e->getCode().' Error Message: ' . $e->getMessage().' Stack Trace: ' . nl2br($e->getTraceAsString());
			return  Array('success' => false,'mensaje' => "Error durante la operacion",'error' => $arrayError);
		}
		
	}

	public function sendMessage($request){
		$this->BDController = new BDController();
		$Notifier = new Notifier();
		try{
			$this->BDController->conectar();
			$this->BDController->autocommit(FALSE);

			$query = "SELECT u.user_id,u.username email,u2.name from_name,u.phone   
                    FROM user u 
                     inner join user u2 on u2.user_id = '".$_SESSION['user_id']."' 
                    WHERE  u.user_id in (".$request['recipients'].") ";
                    //echo $query;
			$this->BDController->setQuery($query);
			$result = $this->BDController->ejecutaInstruccion();
			while ($item = $this->BDController->fetch($result)){
				$query = "insert into notification (from_user_id,to_user_id,subject,message,date,time) 
				values ('".$_SESSION['user_id']."','".$item['user_id']."',
						'".$request['subject']."','".$request['message']."',curdate(),curtime())";
				$this->BDController->setQuery($query);
				//echo $query;
				$this->BDController->ejecutaInstruccion();

				$resultEmailMessage = '';
				$resultSMSMessage = '';
				if($item['email'] != ""){
					$resultEmailMessage = $Notifier->sendMail($item['from_name'],$item['email'],$request['subject'],$request['message']);
				}
				if($item['phone'] != ""){
					$resultSMSMessage = $Notifier->sendSMSToAllCarrier($item['from_name'],$item['phone'],$request['subject'],$request['message'],$this->BDController);
				}
			}

			$this->BDController->commit();
			$this->BDController->desconectar();

			return  Array('success' => true,'message' => 'Sent','notificationEmail' => $resultEmailMessage,'notificationPhone' => $resultSMSMessage);
		} 			
		catch(Exception $e ) {
			$this->BDController->rollback();
			$this->BDController->desconectar();
			$arrayError = Array('Error No:' => $e->getCode(),'Error Message:' => $e->getMessage(),'Stack Trace:' => nl2br($e->getTraceAsString()));
			return  Array('success' => false,'message' => "Error durante la operacion",'error' => $arrayError);
		}
	}

	public function setMessageAsRead($request){
		$this->BDController = new BDController();
		$Notifier = new Notifier();
		try{
			$this->BDController->conectar();
			$this->BDController->autocommit(FALSE);

			$query = "SELECT n.message,n.subject,u.name from_name  
                    FROM notification n 
                     inner join user u on u.user_id = n.to_user_id 
                    WHERE  n.notification_id = '".$request['notification_id']."' ";
                    //echo $query;
			$this->BDController->setQuery($query);
			$result = $this->BDController->ejecutaInstruccion();
			$item = $this->BDController->fetch($result);

			$query = "update notification set message_read=1 where notification_id = '".$request['notification_id']."'";
			$this->BDController->setQuery($query);
			$this->BDController->ejecutaInstruccion();

			
			$this->BDController->commit();
			$this->BDController->desconectar();

			return  Array('success' => true,'message' => 'Read','from' => $item['from_name'],'subject' => $item['subject'],'messageBody' => $item['message']);
		} 			
		catch(Exception $e ) {
			$this->BDController->rollback();
			$this->BDController->desconectar();
			$arrayError = Array('Error No:' => $e->getCode(),'Error Message:' => $e->getMessage(),'Stack Trace:' => nl2br($e->getTraceAsString()));
			return  Array('success' => false,'message' => "Error durante la operacion",'error' => $arrayError);
		}
	}

	public function getProfileList($request){
		$this->BDController = new BDController();
		try{
			$this->BDController->conectar();
			
			$query = '';
			$inicio = 0;
			$fin = 0;
			$num = 0;

			$andDealership = "";
			if(isset($_SESSION['dealership_id']) && $_SESSION['dealership_id'] != ''){
				$andDealership = " and c.dealership_id = '".$_SESSION['dealership_id']."'";
			}
			$query = "SELECT * 
                    FROM profile c    
                    where 1 ".$andDealership;
			
			$this->BDController->setQuery($query);
			$result = $this->BDController->ejecutaInstruccion();
			$matches = Array();
			while ($item = $this->BDController->fetch($result)){
				$matches[] = $item;
				//$matches2[] = $item;//array('nombre' => $item['nombre']);
			}

			$query = "SELECT a.profile_menu_access_id,a.profile_id,a.allow, p.description parent_menu,m.description menu, 
						m.parent_menu_id,m.menu_id  
						from profile_menu_access_default a 
						inner join menu m on m.menu_id = a.menu_id 
						inner join menu p on p.menu_id = m.parent_menu_id 
						WHERE 1 
						ORDER BY a.profile_id,p.menu_order,m.menu_order  ";
			
			$this->BDController->setQuery($query);
			$result = $this->BDController->ejecutaInstruccion();
			$matchesProfilesDetails = Array();
			while ($item = $this->BDController->fetch($result)){
				$matchesProfilesDetails[] = $item;
				//$matches2[] = $item;//array('nombre' => $item['nombre']);
			}

			$query = "SELECT coalesce(p.create_new,0) create_new,coalesce(p.modify,0) modify,coalesce(p.remove,0) remove 
                    FROM user u    
                    left join profile p on p.profile_id = u.profile_id 
					WHERE  u.user_id = '".$_SESSION['user_id']."'";
			$this->BDController->setQuery($query);
			$result = $this->BDController->ejecutaInstruccion();
			$num = $this->BDController->numero_filas($result);
			$item = $this->BDController->fetch($result);//'create_new' => $item['create_new'],'modify' => $item['modify'],'remove' => $item['remove']
			$this->BDController->desconectar();

			return  Array('success' => true,'totalCount' => $num,'matches' => $matches,'matchesProfilesDetails' => $matchesProfilesDetails,'inicio' => $inicio,'fin' => $fin,
				'create_new' => $item['create_new'],'modify' => $item['modify'],'remove' => $item['remove']);
		} 			
		catch(Exception $e ) {
			$this->BDController->rollback();
			$this->BDController->desconectar();
			$arrayError = Array('Error No:' => $e->getCode(),'Error Message:' => $e->getMessage(),'Stack Trace:' => nl2br($e->getTraceAsString()));
			//$arrayError = 'Error No: ' . $e->getCode().' Error Message: ' . $e->getMessage().' Stack Trace: ' . nl2br($e->getTraceAsString());
			return  Array('success' => false,'mensaje' => "Error durante la operacion",'error' => $arrayError);
		}
		
	}

	public function getProfile($request){
		$this->BDController = new BDController();
		try{
			$this->BDController->conectar();
			
			$query = '';

			$query = "SELECT * 
                    FROM profile l    
                    WHERE  profile_id = '".$request['profile_id']."'";
			
			$this->BDController->setQuery($query);
			$result = $this->BDController->ejecutaInstruccion();
			$matches = Array();
			while ($item = $this->BDController->fetch($result)){
				$matches[] = $item;
				//$matches2[] = $item;//array('nombre' => $item['nombre']);
			}

            $query = "SELECT ma.profile_menu_access_id,m.parent_menu_id,m.menu_id,p.menu_order, p.description parent_menu, m.description menu,
			 			ma.allow 
			 			FROM profile_menu_access ma 
						inner join menu m on m.menu_id = ma.menu_id 
						INNER JOIN menu p ON p.menu_id = m.parent_menu_id
						WHERE ma.profile_id = '".$request['profile_id']."'  
						ORDER BY p.menu_order,m.menu_order  ";
			
			$this->BDController->setQuery($query);
			$result = $this->BDController->ejecutaInstruccion();
			$matchesAccess = Array();
			while ($item = $this->BDController->fetch($result)){
				$matchesAccess[] = $item;
			}

			$query = "SELECT coalesce(p.create_new,0) create_new,coalesce(p.modify,0) modify,coalesce(p.remove,0) remove 
                    FROM user u    
                    left join profile p on p.profile_id = u.profile_id 
					WHERE  u.user_id = '".$_SESSION['user_id']."'";
			$this->BDController->setQuery($query);
			$result = $this->BDController->ejecutaInstruccion();
			$num = $this->BDController->numero_filas($result);
			$item = $this->BDController->fetch($result);//'create_new' => $item['create_new'],'modify' => $item['modify'],'remove' => $item['remove']
			$this->BDController->desconectar();

			return  Array('success' => true,'matches' => $matches,'matchesAccess' => $matchesAccess,
				'create_new' => $item['create_new'],'modify' => $item['modify'],'remove' => $item['remove']);
		} 			
		catch(Exception $e ) {
			$this->BDController->rollback();
			$this->BDController->desconectar();
			$arrayError = Array('Error No:' => $e->getCode(),'Error Message:' => $e->getMessage(),'Stack Trace:' => nl2br($e->getTraceAsString()));
			//$arrayError = 'Error No: ' . $e->getCode().' Error Message: ' . $e->getMessage().' Stack Trace: ' . nl2br($e->getTraceAsString());
			return  Array('success' => false,'mensaje' => "Error durante la operacion",'error' => $arrayError);
		}
		
	}

	public function saveProfile($request){
		$this->BDController = new BDController();
		try{
			$this->BDController->conectar();
			$this->BDController->autocommit(FALSE);
			$saveQuery = '';
			$requestOriginal = $request;
			
			if(!isset($request['profile_id'])){
				$saveQuery = "INSERT INTO profile (dealership_id,description,create_new,modify,remove) 
								VALUES ('".$_SESSION['dealership_id']."','".$request['description']."','".$request['create_new']."','".$request['modify']."'
								,'".$request['remove']."');";

				$this->BDController->setQuery($saveQuery);
				$this->BDController->ejecutaInstruccion();
				$profileID = $this->BDController->lastId();

				$parent_menu_id = -1;
				foreach ($request['profileDetails'] as $key => $profileAccesMenu) {
					if($profileAccesMenu['parent_menu_id'] != $parent_menu_id){
							$parent_menu_id = $profileAccesMenu['parent_menu_id'];

							$saveQuery = "INSERT INTO profile_menu_access (profile_id,menu_id,allow) 
										VALUES ('".$profileID."','".$profileAccesMenu['parent_menu_id']."',1);";
							$this->BDController->setQuery($saveQuery);
							$this->BDController->ejecutaInstruccion();
					}
					$saveQuery = "INSERT INTO profile_menu_access (profile_id,menu_id,allow) 
										VALUES ('".$profileID."','".$profileAccesMenu['menu_id']."','".$profileAccesMenu['allow']."');";
					$this->BDController->setQuery($saveQuery);
					$this->BDController->ejecutaInstruccion();
					
				}
			}
			else{
				$saveQuery = "update profile set description = '".$request['description']."', create_new='".$request['create_new']."', modify='".$request['modify']."'
				, remove='".$request['remove']."'
								where profile_id = '".$request['profile_id']."'";
								//echo $saveQuery;
				$this->BDController->setQuery($saveQuery);
				$this->BDController->ejecutaInstruccion();	
				$profileID = $request['profile_id'];

				$parent_menu_id = -1;
				foreach ($request['profileDetails'] as $key => $profileAccesMenu) {
					$saveQuery = "update profile_menu_access set allow =  '".$profileAccesMenu['allow']."' 
										where profile_menu_access_id = '".$profileAccesMenu['profile_menu_access_id']."' ";
					$this->BDController->setQuery($saveQuery);
					$this->BDController->ejecutaInstruccion();
					
				}
			}
				$this->BDController->commit();
			
			
			return  Array('success' => true,"message" => "Success","profile_id" => $profileID);
		} 
		catch(Exception $e ) {
			$this->BDController->rollback();
			$this->BDController->desconectar();
			$arrayError = Array('Error No:' => $e->getCode(),'Error Message:' => $e->getMessage(),'Stack Trace:' => nl2br($e->getTraceAsString()));
			//$arrayError = 'Error No: ' . $e->getCode().' Error Message: ' . $e->getMessage().' Stack Trace: ' . nl2br($e->getTraceAsString());
			return  Array('success' => true,'message' => $e->getMessage(),'error' => $arrayError);
		}
		
	}

}

?>
