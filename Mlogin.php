<?php


//$MAIN_DIR = dirname(__FILE__);
//require_once("includes/clases/controladores/AppController.php");
$MAIN_DIR = "";
require_once("includes/clases/controladores/AppController.php");
if(isset($_SESSION['username'])){
  session_destroy();
}

$controlador = new AppController($_FILES,$_REQUEST);//,$session
$request['accion'] = "Action/Setting/Get_General_Setting";
$controlador->setRequest('',$request);
$result = $controlador->ejecutar();

//if(!isset($_SESSION['username'])){
  //define('TIMEZONE', $result['matches'][0]['time_zone']);
  $_SESSION['PHP_TIMEZONE'] = $result['matches'][0]['time_zone'];
  date_default_timezone_set($_SESSION['PHP_TIMEZONE']);
  $now = new DateTime();
  $mins = $now->getOffset() / 60;
  $sgn = ($mins < 0 ? -1 : 1);
  $mins = abs($mins);
  $hrs = floor($mins / 60);
  $mins -= $hrs * 60;
  $offset = sprintf('%+d:%02d', $hrs*$sgn, $mins);

  $_SESSION['MYSQL_TIMEZONE'] = $offset;


if(isset($_POST['user'])){
  
  $controlador = new AppController($_FILES,$_REQUEST);//,$session
  $request['username'] = $_POST['user'];
  $request['password'] = $_POST['password'];
  $request['accion'] = "Action/User/Get_Login";
  $controlador->setRequest('',$request);
  $login = $controlador->ejecutar();
  
  $name = $login['matches'][0]['name'];

  if($name!=""){
    
    $user_id = $login['matches'][0]['user_id'];
    $user_name = $login['matches'][0]['username'];
    $foto = $login['matches'][0]['foto'];

    $_SESSION['name'] = $name;
    $_SESSION['username'] = $user_name;
    $_SESSION['foto'] = ($foto!="")?$foto:'no-avatar.jpg';
    $_SESSION['user_id'] = $user_id;
    //echo "asdasdalskdja";exit;
    header("location: /".ROOT_FOLDER."/");
  }
}

?>
<!DOCTYPE html>

<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <link rel="icon" href="includes/images/server/favicon.ico" type="image/x-icon"/>
    <link rel="shortcut icon" href="includes/images/server/favicon.ico"/>
    <link rel="icon" type="image/png" href="includes/images/server/favicon.png"/>

    <!--link rel="apple-touch-icon" sizes="57x57" href="apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="favicon-16x16.png">
    <link rel="manifest" href="manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff"-->
    
    <title><?php echo APP_NAME?> | Login</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="plugins/iCheck/square/blue.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
      video { border: 1px solid #ccc; display: block; margin: 0 0 20px 0; }
      #canvas { margin-top: 20px; border: 1px solid #ccc; display: block; }
    </style>
  </head>
  <body class="hold-transition login-page">
    <div class="login-box">
      <div class="login-logo">
        <a><b><?php echo APP_NAME?></b><br></a>
      </div><!-- /.login-logo -->
      <div class="login-box-body">
        <p class="login-box-msg">Sign in to start your session</p>
        <form id="formlogin" method="post">
          <div class="col-xs-12">
            <div class="form-group has-feedback">
              <input type="text" id="username" name="username" class="form-control" placeholder="User">
              <span class="glyphicon glyphicon-user form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
              <input type="password" id="password" name="password" class="form-control" placeholder="Password">
              <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
              <a id="snap" onclick="loginSuccess()" class="btn btn-primary btn-block btn-flat">Sign In</a>
            </div>
          </div>
          <div class="row">
            <div class="col-xs-8">
              <div class="checkbox icheck">
                
              </div>
            </div><!-- /.col -->
            <div class="col-xs-4">
                  <input type="hidden" name="foto-hidden" id="foto-hidden" >   
              <!--button type="submit" id="snap" class="btn btn-primary btn-block btn-flat">Sign In</button-->
            </div><!-- /.col -->
          </div>
        </form>


        <!--a href="#">I forgot my password</a><br>
        <a href="register.html" class="text-center">Register a new membership</a-->

      </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->

    <!-- jQuery 2.1.4 -->
    <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <!-- iCheck -->
    <script src="plugins/iCheck/icheck.min.js"></script>
    <script>
      $(function () {
        $('input').iCheck({
          checkboxClass: 'icheckbox_square-blue',
          radioClass: 'iradio_square-blue',
          increaseArea: '20%' // optional
        });
      });
    </script>
    <SCRIPT>
        

        function loginSuccess(){
            //$("#name").val(name);
            //$("#user_id").val(user_id);
            var username = $("#username").val();
            var password = $("#password").val();
            var user_id = 0;

            var ajax = new XMLHttpRequest();
            var jsonResponse = null;
            
            ajax.onreadystatechange = function() {
                if (ajax.readyState == 4 && ajax.status == 200) {
                    jsonResponse = JSON.parse(ajax.responseText);
                    if(jsonResponse.log != -1){
                      if(jsonResponse.success)
                        document.location.href= "/<?php echo ROOT_FOLDER;?>/";
                    }
                    else{
                      alert("User name or password invalid");
                    }
                }
            };
            ajax.open("POST",'includes/clases/controladores/AppController.php?accion=Action/User/Get_Login&output=json&username='+username+"&password="+password,false);
            ajax.send();                

                
            //$("#formlogin").submit();
        }
        $('#password').keypress(function (e) {
         var key = e.which;
         if(key == 13)  // the enter key code
          {
            loginSuccess();
          }
        });
        $('#username').keypress(function (e) {
         var key = e.which;
         if(key == 13)  // the enter key code
          {
            $('#password').focus();
          }
        });  
    </SCRIPT>
  </body>
</html>
