<style type="text/css">

tr.espacio td {padding-top: 5px; padding-bottom:5px}
table {
    border-width: thin;
    border-spacing: 2px;
    border-style: none;
    border-color: black;
}
</style>

<?php

//require_once("../includes/clases/libs/php-barcode-master/barcode.php");
$MAIN_DIR = "../../";
require_once("../clases/controladores/AppController.php");
$controlador = new AppController($_FILES,$_REQUEST);//,$session
$request['car'] = $_GET['car'];
$request['accion'] = "Action/Stock/Car_History_Report";
$controlador->setRequest('',$request);
$result = $controlador->ejecutar();

///*$date = date_create($fecha);


?>
<page style="font-size:14px">
<table style="width: 100%;" align="right" >
    <tr>
        <td style="font-size:21px;text-align:right">
        <!--img src="../includes/imagenes/icons/hotexpress-logo.png" alt="logo" style="width: 200px;"-->
            <font size='+3'><?php echo date('m/d/Y');?></font>
        </td>
    </tr>

</table>

<table style="width: 100%;" align="center" >
    <tr>
        <td style="">
        <br>
        <?php echo "History for: ".$_GET['car'];?>
        </td>
    </tr>

</table>

<table cellpadding="0" cellspacing="0" style="width: 100%;" >
    <tr style="background-color:#BAD9E3;border-style: solid; border-width: 1px">
        <td style="width: 15%;font-size:14px;border-style: solid; border-width: 1px;">
        	<strong>Date</strong>
        </td>
        <td style="width: 35%;font-size:14px;border-style: solid; border-width: 1px; ">
            <strong>User</strong>
        </td>
        <td style="width: 15%;font-size:14px;text-align:right;border-style: solid; border-width: 1px; ">
            <strong>Quantity</strong>
        </td>
        <td style="width: 15%;font-size:14px;text-align:right;border-style: solid; border-width: 1px; ">
            <strong>Rate</strong>
        </td>
        <td style="width: 20%;font-size:14px;text-align:right;border-style: solid; border-width: 1px; ">
            <strong>Total</strong>
        </td>
    </tr>
<?php 

$color = "";
$i = 0;
$total = 0;
foreach ($result['matches'] as $key => $detail) {
    $total += $detail['total'];
    if($i % 2 == 0)
        $color = "#FFFFFF";
    else
        $color = "#E9F0F2";
 ?>
    <tr style="background-color:<?php echo $color;?>">
        <td style="width: 15%;font-size:14px;border-style: solid; border-width: 1px;">
            <?php echo $detail['date']?>    
        </td>
        <td style="width: 35%;font-size:14px;border-style: solid; border-width: 1px; ">
        <?php echo $detail['name']?>
        </td>
        <td style="width: 15%;font-size:14px;text-align:right;border-style: solid; border-width: 1px; ">
             <?php echo $detail['quantity']?> galls
        </td>
        <td style="width: 15%;font-size:14px;text-align:right;border-style: solid; border-width: 1px; ">
            $ <?php echo number_format($detail['filling_rate'], 6, '.', ',')?>
        </td>
        <td style="width: 20%;font-size:14px;text-align:right;border-style: solid; border-width: 1px; ">
            $ <?php echo number_format($detail['total'], 6, '.', ',')?>
        </td>
    </tr>
<?php $i++;}?>
    <tr >
        <td style="width: 15%;font-size:14px;">
            Total
        </td>
        <td style="width: 35%;font-size:14px; ">
        
        </td>
        <td style="width: 15%;font-size:14px;text-align:right; ">
            
        </td>
        <td style="width: 15%;font-size:14px;text-align:right; ">
            
        </td>
        <td style="width: 20%;font-size:14px;text-align:right;border-style: solid; border-width: 1px; ">
            $ <?php echo number_format($total, 6, '.', ',')?>
        </td>
    </tr>
</table>

<table style="width: 100%;" align="center" >
    <tr>
        <td style="width: 100% ; border-bottom: 1px  solid black;">
        <br>
        </td>
    </tr>
</table>

<br>

    <page_footer>
        <table style="width: 100%; border-bottom: 1px  solid black;" align="left" >
            <tr>
                <td style="width: 50%;">
                    <strong>Printed By</strong>
                </td>
                <td style="width: 25%;">
                    <strong><?php echo $_SESSION['name']?></strong>
                </td>
                <td style="width: 25%;">
                    <strong><?php echo date('m/d/Y')?></strong>
                </td>
            </tr>
        </table>
    </page_footer>
</page>