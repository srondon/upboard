<?php

require_once $MAIN_DIR.'includes/clases/BDController.php';
//if(!isset($_SESSION)){session_start();} 
class ModelDashboard {
	
	private $bd = null;
	
	public function saveNotification($request){
		$this->BDController = new BDController();
		try{
			$this->BDController->conectar();
			$this->BDController->autocommit(FALSE);
			$saveQuery = '';
			$requestOriginal = $request;
			
			$saveQuery = "INSERT INTO notification (subject, message,to_user_id,date,time) 
								VALUES ('".$request['subject']."','".$request['message']."','".$request['user_id']."', curdate(),curtime());";

			$this->BDController->setQuery($saveQuery);
			$this->BDController->ejecutaInstruccion();
			
			$saveQuery = "update fuel_filling_temporal set error_notified = 1
							where fuel_filling_id = '".$request['fuel_filling_id']."' ";

			$this->BDController->setQuery($saveQuery);
			$this->BDController->ejecutaInstruccion();

			$this->BDController->commit();
			
			
			return  Array('success' => true,"message" => "Success");
		} 
		catch(Exception $e ) {
			$this->BDController->rollback();
			$this->BDController->desconectar();
			$arrayError = Array('Error No:' => $e->getCode(),'Error Message:' => $e->getMessage(),'Stack Trace:' => nl2br($e->getTraceAsString()));
			//$arrayError = 'Error No: ' . $e->getCode().' Error Message: ' . $e->getMessage().' Stack Trace: ' . nl2br($e->getTraceAsString());
			return  Array('success' => true,'message' => $e->getMessage(),'error' => $arrayError);
		}
		
	}

	public function getData($request){
		$this->BDController = new BDController();
		try{
			$this->BDController->conectar();
			
			$query = '';
			$andDealership = "";
			$andDealership2 = "";
			if(isset($_SESSION['dealership_id']) && $_SESSION['dealership_id'] != ''){
				$andDealership = " and p.dealership_id = '".$_SESSION['dealership_id']."'";
				$andDealership2 = " and s.dealership_id = '".$_SESSION['dealership_id']."'";
			}

			$query = "SELECT s.name,st.name status,p.datetoshow   
				FROM salesboard p 
				 inner join saleman s on s.saleman_id = p.saleman_id 
				 inner join status st on st.status_id = p.status_id 
				 WHERE p.status_id in (3,5,9) ".$andDealership." 
				 order by datetoshow asc" ;

			$this->BDController->setQuery($query);
			$result = $this->BDController->ejecutaInstruccion();
			$matchesSelling = Array();
			while ($item = $this->BDController->fetch($result)){
				$matchesSelling[] = $item;
			}

			$query = "SELECT s.name,st.name status,p.datetoshow   
				FROM salesboard p 
				 inner join saleman s on s.saleman_id = p.saleman_id 
				 inner join status st on st.status_id = p.status_id 
				 WHERE p.status_id in (10) ".$andDealership;

			$this->BDController->setQuery($query);
			$result = $this->BDController->ejecutaInstruccion();
			$matchesOffs = Array();
			while ($item = $this->BDController->fetch($result)){
				$matchesOffs[] = $item;
			}

			$query = "SELECT s.name,(case when p.board_order=1 then 'Next' else st.name end) status,p.board_order, 
			 (case when p.board_order=1 then 'green' else 'blue' end ) color    
				FROM salesboard p 
				 inner join saleman s on s.saleman_id = p.saleman_id 
				 inner join status st on st.status_id = p.status_id 
				 WHERE p.status_id = 1 ".$andDealership. " 
				  order by p.board_order asc ";

			$this->BDController->setQuery($query);
			$result = $this->BDController->ejecutaInstruccion();
			$matchesBoard = Array();
			$i =0;
			$next = '';
			while ($item = $this->BDController->fetch($result)){
				if($i==0){
					$next = $item['name'];
				}
				$matchesBoard[] = $item;
				$i++;
			}	

			
			$this->BDController->desconectar();;
			return  Array('success' => true,'matchesSelling' => $matchesSelling,'matchesOffs' => $matchesOffs,'matchesBoard' => $matchesBoard,'next' => $next);//
		} 
		catch(Exception $e ) {
			$this->BDController->rollback();
			$this->BDController->desconectar();
			$arrayError = Array('Error No:' => $e->getCode(),'Error Message:' => $e->getMessage(),'Stack Trace:' => nl2br($e->getTraceAsString()));
			//$arrayError = 'Error No: ' . $e->getCode().' Error Message: ' . $e->getMessage().' Stack Trace: ' . nl2br($e->getTraceAsString());
			return  Array('success' => false,'mensaje' => "Error durante la operacion",'error' => $arrayError);
		}
		
	}
}

?>
