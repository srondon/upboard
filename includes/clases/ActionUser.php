<?php

require_once $MAIN_DIR.'includes/clases/modelos/ModelUser.php';
//if(!isset($_SESSION)){session_start();} 

class ActionUser {
	
	private $session = "";
	
	public function get_login($request){
		$sessionModel = new ModelUser();
		
		$response = $sessionModel->getLogin($request);
		
		return $response;
	}

	public function login_finger($request){
		$sessionModel = new ModelUser();
		
		$response = $sessionModel->getLoginFinger($request);
		
		return $response;
	}

	public function get_user_list($request){
		$sessionModel = new ModelUser();
		
		$response = $sessionModel->getUserList($request);
		
		return $response;
	}

	public function get_user($request){
		$sessionModel = new ModelUser();
		
		$response = $sessionModel->getUser($request);
		
		return $response;
	}	

	public function save($request){
		$sessionModel = new ModelUser();
		
		$response = $sessionModel->save($request);
		
		return $response;
	}	

	public function delete($request){
		$sessionModel = new ModelUser();
		
		$response = $sessionModel->delete($request);
		
		return $response;
	}	

	public function save_my_profile($request,$files){
		$sessionModel = new ModelUser();
		
		$response = $sessionModel->saveMyProfile($request,$files);
		
		return $response;
	}	

	public function get_user_messages($request){
		$sessionModel = new ModelUser();
		
		$response = $sessionModel->getUserMessages($request);
		
		return $response;
	}	

	public function send_message($request){
		$sessionModel = new ModelUser();
		
		$response = $sessionModel->sendMessage($request);
		
		return $response;
	}

	public function set_message_as_read($request){
		$sessionModel = new ModelUser();
		
		$response = $sessionModel->setMessageAsRead($request);
		
		return $response;
	}

	public function get_profile_list($request){
		$sessionModel = new ModelUser();
		
		$response = $sessionModel->getProfileList($request);
		
		return $response;
	}

	public function get_profile($request){
		$sessionModel = new ModelUser();
		
		$response = $sessionModel->getProfile($request);
		
		return $response;
	}

	public function save_profile($request){
		$sessionModel = new ModelUser();
		
		$response = $sessionModel->saveProfile($request);
		
		return $response;
	}
}

?>