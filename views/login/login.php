<?php

//$MAIN_DIR = dirname(__FILE__);
//require_once("includes/clases/controladores/ControladorApp.php");
if(isset($_SESSION['name'])){
  session_destroy();
}

if(isset($_POST[user]))
{
  $text = "";
  //$controlador = new ControladorApp($_FILES,$_REQUEST);//,$session
  $request['user'] = $_POST[user];
  $request['password'] = $_POST[password];
  $request['accion'] = "Accion/Login/Get_Login";
  //$controlador->setRequest('',$request);
  //$login = $controlador->ejecutar();
  
  //$name = $login['matches'][0][name];
  //$user_id = $login['matches'][0][user_id];
  //$foto = $login['matches'][0][foto];

  if($name!="")
  {
    $_SESSION['name'] = $name;
    $_SESSION['foto'] = ($foto!="")?$foto:'no-avatar.jpg';
    $_SESSION['user_id'] = $user_id;
    
    header("location: index.php");
    exit;
  }
  else
  {
    if(isset($_POST['loginweb'])){
      header("location: http://hot.protec.com.pa/index.php?error=1");
      exit; 
    }
    $text = "user o password incorrecto";
  }
}

?>
<!DOCTYPE html>

<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>AdminLTE 2 | Log in</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="../../bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="../../plugins/iCheck/square/blue.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
      video { border: 1px solid #ccc; display: block; margin: 0 0 20px 0; }
      #canvas { margin-top: 20px; border: 1px solid #ccc; display: block; }
    </style>
  </head>
  <body class="hold-transition login-page">
    <div class="login-box">
      <div class="login-logo">
        <a href="../../index2.html"><b>Fuel Control</b><br>Toyota of South Florida</a>
      </div><!-- /.login-logo -->
      <div class="login-box-body">
        <p class="login-box-msg">Sign in to start your session</p>
        <form action="../../index2.html" method="post">
          <div class="col-xs-8">
            <video id="video" width="300" height="300" autoplay></video>
            <canvas style="display:none" id="canvas" width="300" height="300"></canvas>
          </div>
          <div class="col-xs-4">
            <div class="form-group has-feedback">
              <input type="text" class="form-control" placeholder="User">
              <span class="glyphicon glyphicon-user form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
              <input type="password" class="form-control" placeholder="Password">
              <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
              <button type="submit" id="snap" class="btn btn-primary btn-block btn-flat">Sign In</button>
            </div>
          </div>
          <div class="row">
            <div class="col-xs-8">
              <div class="checkbox icheck">
                
              </div>
            </div><!-- /.col -->
            <div class="col-xs-4">
              <!--button type="submit" id="snap" class="btn btn-primary btn-block btn-flat">Sign In</button-->
            </div><!-- /.col -->
          </div>
        </form>


        <!--a href="#">I forgot my password</a><br>
        <a href="register.html" class="text-center">Register a new membership</a-->

      </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->

    <!-- jQuery 2.1.4 -->
    <script src="../../plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="../../bootstrap/js/bootstrap.min.js"></script>
    <!-- iCheck -->
    <script src="../../plugins/iCheck/icheck.min.js"></script>
    <script>
      $(function () {
        $('input').iCheck({
          checkboxClass: 'icheckbox_square-blue',
          radioClass: 'iradio_square-blue',
          increaseArea: '20%' // optional
        });
      });
    </script>
    <script>

    // Put event listeners into place
    window.addEventListener("DOMContentLoaded", function() {
      // Grab elements, create settings, etc.
      var canvas = document.getElementById("canvas"),
        context = canvas.getContext("2d"),
        video = document.getElementById("video"),
        videoObj = { "video": true },
        errBack = function(error) {
          console.log("Video capture error: ", error.code); 
        };

      // Put video listeners into place
      if(navigator.getUserMedia) { // Standard
        navigator.getUserMedia(videoObj, function(stream) {
          video.src = stream;
          video.play();
        }, errBack);
      } else if(navigator.webkitGetUserMedia) { // WebKit-prefixed
        navigator.webkitGetUserMedia(videoObj, function(stream){
          video.src = window.webkitURL.createObjectURL(stream);
          video.play();
        }, errBack);
      } else if(navigator.mozGetUserMedia) { // WebKit-prefixed
        navigator.mozGetUserMedia(videoObj, function(stream){
          video.src = window.URL.createObjectURL(stream);
          video.play();
        }, errBack);
      }

      // Trigger photo take
      document.getElementById("snap").addEventListener("click", function() {
        context.drawImage(video, 0, 0, 640, 480);
        
      });
    }, false);

  </script>
  </body>
</html>
