<section class="content-header">
          <h1>
            New Status
            <!--small>Control panel</small-->
          </h1>
          <!--ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Calendar</li>
          </ol-->
</section>
<section class="content">

  <div class="row">
    <div class="col-md-8">
    <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title"></h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form role="form">
                    <div class="box-body">
                    <div class="col-xs-4">
                      <div class="form-group">
                        <label for="exampleInputEmail1">Name</label>
                        <input type="text" class="form-control" ng-model="status.name" id="name" placeholder="Enter name">
                      </div>

                      
                    </div>  
                    <div class="col-xs-4">
                      <div class="form-group">
                        <label for="exampleInputEmail1">Short Name</label>
                        <input type="text" class="form-control" ng-model="status.short_name" id="short_name" placeholder="Enter short name">
                      </div>

                      
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                          <label for="exampleInputEmail1">Description</label>
                          <input type="text" class="form-control" ng-model="status.description" id="description" placeholder="Enter description">
                        </div>
                    </div>
                    
                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button ng-click="submitForm(0)" class="btn btn-primary">Submit and Continue</button>
                    <button ng-click="submitForm(1)" class="btn btn-primary">Submit and Exit</button>
                    <button ng-click="exit()" style="float:right" class="btn btn-danger">Exit</button>
                  </div>
                </form>
              </div>        
    
  </div>
  </div>
</section>