<?php

require_once $MAIN_DIR.'includes/clases/BDController.php';
//require_once $MAIN_DIR.'includes/clases/AccionAuditoria.php';
require_once $MAIN_DIR.'includes/clases/Notifier.php';
//if(!isset($_SESSION)){session_start();} 
class ModelSalesman {
	
	private $bd = null;
	
	public function getSalesmanList($request){
		$this->BDController = new BDController();
		try{
			$this->BDController->conectar();
			
			$query = '';
			$inicio = 0;
			$fin = 0;
			$num = 0;
			$limit = "";

			if (isset($request['orden']) && $request['orden'] != "") {
				$orden = $request['orden']." ".$request['dir'];
			}
			else{
				$orden = " name asc";
			}

			$andDealership = "";
			if(isset($_SESSION['dealership_id']) && $_SESSION['dealership_id'] != ''){
				$andDealership = " and f.dealership_id = '".$_SESSION['dealership_id']."'";
			}
			
			$tamanoPagina = 30;
			if (isset($request['pagina'])) {
				$inicio = ($request['pagina'] - 1 )  * $tamanoPagina;
				$query = "SELECT *     
                    FROM saleman f 
                    WHERE  1 ".$andDealership;
				
				$this->BDController->setQuery($query);
				$result = $this->BDController->ejecutaInstruccion();
				$num = $this->BDController->numero_filas($result);
				$limit = " limit ".$inicio.",".$tamanoPagina;
			}
			
			$showActives = "";
			if((isset($request['typeahead']) || isset($request['isBox']))){
				$showActives = " and f.active = 1 ";
			}
			$query = "SELECT *,d.company_name dealership  
                    FROM saleman  f  
                    inner join dealership d on d.dealership_id = f.dealership_id 
                    WHERE  1  ".$andDealership." ".$showActives." 
                    ORDER BY ".$orden." ".$limit;
			
			$this->BDController->setQuery($query);
			$result = $this->BDController->ejecutaInstruccion();
			$matches = Array();
			$itemBox['name'] = "----Put In----";
			$itemBox['dpid'] = "";
			$itemBox['carrier_id'] = "";
			$itemBox['dealership_id'] = "";
			$itemBox['email'] = "";
			$itemBox['phone'] = "";
			$itemBox['saleman_id'] = 0;
			$i=0;
			while ($item = $this->BDController->fetch($result)){
				if((isset($request['typeahead']) || isset($request['isBox'])) && $i == 0){
					$matches[] = $itemBox;					
					$i++;
				}
				$matches[] = $item;
				//$matches2[] = $item;//array('nombre' => $item['nombre']);
			}

			$query = "SELECT coalesce(p.create_new,0) create_new,coalesce(p.modify,0) modify,coalesce(p.remove,0) remove 
                    FROM user u    
                    left join profile p on p.profile_id = u.profile_id 
					WHERE  u.user_id = '".$_SESSION['user_id']."'";
			$this->BDController->setQuery($query);
			$result = $this->BDController->ejecutaInstruccion();
			$num = $this->BDController->numero_filas($result);
			$item = $this->BDController->fetch($result);//'create_new' => $item['create_new'],'modify' => $item['modify'],'remove' => $item['remove']
			$this->BDController->desconectar();

			return  Array('success' => true,'totalCount' => $num,'matches' => $matches,'inicio' => $inicio,'fin' => $fin,
				'create_new' => $item['create_new'],'modify' => $item['modify'],'remove' => $item['remove']);
		} 			
		catch(Exception $e ) {
			$this->BDController->rollback();
			$this->BDController->desconectar();
			$arrayError = Array('Error No:' => $e->getCode(),'Error Message:' => $e->getMessage(),'Stack Trace:' => nl2br($e->getTraceAsString()));
			//$arrayError = 'Error No: ' . $e->getCode().' Error Message: ' . $e->getMessage().' Stack Trace: ' . nl2br($e->getTraceAsString());
			return  Array('success' => false,'mensaje' => "Error durante la operacion",'error' => $arrayError);
		}
		
	}

	

	public function getSalesman($request){
		$this->BDController = new BDController();
		try{
			$this->BDController->conectar();
			
			$query = '';
			$inicio = 0;
			$fin = 0;
			$num = 0;

			$query = "SELECT * 
                    FROM saleman l    
                    WHERE  saleman_id = '".$request['saleman_id']."'";
			
			$this->BDController->setQuery($query);
			$result = $this->BDController->ejecutaInstruccion();
			$matches = Array();
			while ($item = $this->BDController->fetch($result)){
				$matches[] = $item;
				//$matches2[] = $item;//array('nombre' => $item['nombre']);
			}
			$query = "SELECT coalesce(p.create_new,0) create_new,coalesce(p.modify,0) modify,coalesce(p.remove,0) remove 
                    FROM user u    
                    left join profile p on p.profile_id = u.profile_id 
					WHERE  u.user_id = '".$_SESSION['user_id']."'";
			$this->BDController->setQuery($query);
			$result = $this->BDController->ejecutaInstruccion();
			$num = $this->BDController->numero_filas($result);
			$item = $this->BDController->fetch($result);//'create_new' => $item['create_new'],'modify' => $item['modify'],'remove' => $item['remove']
			$this->BDController->desconectar();

			return  Array('success' => true,'totalCount' => $num,'matches' => $matches,'inicio' => $inicio,'fin' => $fin,
				'create_new' => $item['create_new'],'modify' => $item['modify'],'remove' => $item['remove']);
		} 			
		catch(Exception $e ) {
			$this->BDController->rollback();
			$this->BDController->desconectar();
			$arrayError = Array('Error No:' => $e->getCode(),'Error Message:' => $e->getMessage(),'Stack Trace:' => nl2br($e->getTraceAsString()));
			//$arrayError = 'Error No: ' . $e->getCode().' Error Message: ' . $e->getMessage().' Stack Trace: ' . nl2br($e->getTraceAsString());
			return  Array('success' => false,'mensaje' => "Error durante la operacion",'error' => $arrayError);
		}
		
	}

	public function saveSalesman($request){
		$this->BDController = new BDController();
		try{
			$this->BDController->conectar();
			$this->BDController->autocommit(FALSE);
			$saveQuery = '';
			$requestOriginal = $request;
			
			if(!isset($request['saleman_id'])){
				$saveQuery = "INSERT INTO saleman (dealership_id,name,adpid,email,phone,notification_carrier_id) 
								VALUES ('".$_SESSION['dealership_id']."','".$request['name']."','".((isset($request['adpid']))?$request['adpid']:'')."','".$request['email']."','".$request['phone']."','".$request['notification_carrier_id']."');";

				$this->BDController->setQuery($saveQuery);
				$this->BDController->ejecutaInstruccion();
				$lastId = $this->BDController->lastId();
			}
			else{
				$saveQuery = "update saleman set name = '".$request['name']."',adpid = '".((isset($request['adpid']))?$request['adpid']:'')."',
								email = '".$request['email']."',phone = '".$request['phone']."',notification_carrier_id = '".$request['notification_carrier_id']."',
								active = '".$request['active']."'    
								where saleman_id = '".$request['saleman_id']."'";

				$this->BDController->setQuery($saveQuery);
				$this->BDController->ejecutaInstruccion();	
				$lastId = $request['saleman_id'];
			}
				$this->BDController->commit();
			
			
			return  Array('success' => true,"message" => "Success","saleman_id" => $lastId);
		} 
		catch(Exception $e ) {
			$this->BDController->rollback();
			$this->BDController->desconectar();
			$arrayError = Array('Error No:' => $e->getCode(),'Error Message:' => $e->getMessage(),'Stack Trace:' => nl2br($e->getTraceAsString()));
			//$arrayError = 'Error No: ' . $e->getCode().' Error Message: ' . $e->getMessage().' Stack Trace: ' . nl2br($e->getTraceAsString());
			return  Array('success' => true,'message' => $e->getMessage(),'error' => $arrayError);
		}
		
	}

	public function delete($request){
		$this->BDController = new BDController();
		try{
			$this->BDController->conectar();
			$this->BDController->autocommit(FALSE);
			$saveQuery = '';
			$requestOriginal = $request;
			
			
				$saveQuery = "delete from saleman where saleman_id = '".$request['saleman_id']."'";

			$this->BDController->setQuery($saveQuery);
			$this->BDController->ejecutaInstruccion();	
			
			$this->BDController->commit();
			
			
			return  Array('success' => true,"message" => "Success");
		} 
		catch(Exception $e ) {
			$this->BDController->rollback();
			$this->BDController->desconectar();
			$arrayError = Array('Error No:' => $e->getCode(),'Error Message:' => $e->getMessage(),'Stack Trace:' => nl2br($e->getTraceAsString()));
			//$arrayError = 'Error No: ' . $e->getCode().' Error Message: ' . $e->getMessage().' Stack Trace: ' . nl2br($e->getTraceAsString());
			return  Array('success' => true,'message' => 'Error during the operation. Please check if this Salesman is not in board before delete','error' => $arrayError);//$e->getMessage()
		}
		
	}
}

?>